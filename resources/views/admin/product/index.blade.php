@extends('adminlte::page')

@section('title', 'Product Master')
@section('css')
<style>
.mt-2 {
    margin-top: 1rem!important;
}
</style>
@endsection
@section('content')

<section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Product Listing</h3>
            </div>
            <!-- The Modal -->
            <div class="modal" id="showmodal">
                <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                    <h4 class="modal-title">Show Product</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="col-lg-9">
                                    <div class="image_display"></div>
                                </div>
                                <div class="col-lg-3 breadcrumb">
                                    <div class="col-lg-12 text-center">
                                        <label for="name">Product Name</label>
                                        <div class="text-capitalize" id="prduct_name"></div>
                                    </div>
                                    <div class="col-lg-12 text-center">
                                        <label for="name">Category Name</label>
                                        <div class="text-capitalize" id="category_name"></div>
                                    </div>
                                    <div class="col-lg-12 text-center">
                                      <label for="name">Brand Name</label>
                                      <div class="text-capitalize" id="brand_name"></div>
                                  </div>
                                    {{-- <div class="col-lg-12 text-cente">
                                        <label for="name">Sub-Category Name</label>
                                        <div class="text-capitalize" id="subcategory_name"></div>
                                    </div> --}}
                                    <div class="col-lg-12 text-center">
                                        <label for="name">Price | Quantity</label>
                                        <div class="text-capitalize" id="price_show"></div>
                                    </div>
                                    <div class="col-lg-12 text-center">
                                        <label for="name">Active</label>
                                        <div class="text-capitalize" id="is_active_show"></div>
                                    </div>
                                    <div class="col-lg-12 text-center">
                                        <label for="name">Description</label>
                                        <div class="text-capitalize" id="description_show"></div>
                                    </div>
                                    <div class="col-lg-6 text-center">
                                        <label for="name">Is Hot Product</label>
                                        <div class="text-capitalize" id="is_hot_product_show"></div>
                                    </div>
                                    <div class="col-lg-6 text-center">
                                        <label for="name">Is special Product</label>
                                        <div class="text-capitalize" id="is_special_product_show"></div>
                                    </div>
                                    <div class="col-lg-12 text-center">
                                        <label for="name">Is Daily Need Product</label>
                                        <div class="text-capitalize" id="is_dailyneed_product_show"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                     <div id="prodd_id"></div>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                </div>
                </div>
            </div>


            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="{{route('product.create')}}">
                            <button class="btn btn-success pull-right">
                                    Create <span class="badge badge-primary">new</span>
                            </button>
                        </a>
                            <button class="btn btn-success pull-left">
                                    Total Product <span class="badge badge-primary">{{count($products)}}</span>
                            </button>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  
                  <th>Category Name</th>
                  <th>Brand</th>
                  {{-- <th>SubCategory Name</th> --}}
                  <th>Price | Quantity</th>
                  <th>Discounted Price</th>
                  <th>Description</th>
                  <th>Image</th>
                  <th>Status</th>
                  <th>Create Info</th>
                  <th>Update Info</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                    @php $imagearray = explode(',',$product->image); @endphp
                    <tr class="appenddata">
                    <td class="text-capitalize prod_name" data-name="{{$product->name??""}}">{{$product->name??""}}</td>
                    <td class="text-capitalize prod_cat" data-categoryname="{{$product->category->name??""}}">{{$product->category->name??""}}</td>
                    <td class="text-capitalize prod_brand" data-brandname="{{$product->brand->name??""}}">{{$product->brand->name??""}}</td>
                    {{-- <td class="text-capitalize prod_subcat" data-subcategoryname="{{$product->subcategory->name??""}}">{{$product->subcategory->name??""}}</td> --}}
                    <td class="prod_price" data-price="{{$product->price}}" data-quantity="{{$product->quantity??"0"}}"><b>{{$product->price??""}} | {{$product->quantity??""}}</b></td>
                    <td class="discounted_price" data-price="{{$product->discounted_price}}" ><b>{{$product->discounted_price??"0"}} </b></td>
                    <td class ="prod_desc" data-description="{{$product->description??""}}">{{substr($product->description, 0, 100)}}</td>
                    <td class="image prod_image" data-image=" {{$product->image}}">
                    @foreach($imagearray as $key=>$prodimage)
                    <div class="col-sm-2">
                    <div class="user-block">
                         <img alt="{{$product->name}}" src="{{URL::asset('storage/uploads/product/thumbnails/'.$prodimage)}}" class="img-circle img-bordered-sm" >
                    </div>
                    </div>

                    @endforeach
                    </td>
                <td class ="prod_is_active" data-is_active="{{$product->is_active}}" data-is_hot_product="{{$product->is_hot_product}}" data-is_special_product="{{$product->is_special_product}}" data-is_dailyneed_product="{{$product->is_dailyneed_product}}"><span class="label  @if($product->is_active ==true)label-success @else label-danger @endif ">@if($product->is_active ==true)Active @else Inactive @endif</span></td>
                    <td data-createdinfo="{{$product->create_info?$product->create_info:'' }}">{{$product->create_info?$product->create_info:'' }}</td>
                    <td data-updateinfo="{{$product->update_info?$product->update_info:'' }}">{{$product->update_info?$product->update_info:'' }}</td>
                    <td class="prod_id" data-id="{{$product->id}}">
                        <div class="btn-group btn-group-sm">
                            <button type="button" class="btn btn-success showproduct">
                                <i class="fa fa-eye"></i>
                              </button>
                              {{-- <button id="showproduct">das</button> --}}
                            {{-- <a href="{{ route('product.show',$product->id) }}" id="myModal" class="edit-model btn btn-success btn-sm " ><i class="fa fa-eye"></i></a> --}}
                            <a href="{{ route('product.edit',$product->id) }}" class="edit-model btn btn-warning btn-sm " ><i class="fa fa-edit"></i></a>
                                <button class="delete-model btn btn-danger btn-sm " type="button" onclick="deleteProduct({{ $product->id }})">
                                    <i class="fa fa-trash"></i>
                                </button>
                                <form id="delete-form-{{ $product->id }}" action="{{ route('product.destroy',$product->id) }}" method="POST" style="display: none;">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </form>
                        </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>Name</th>
                    
                    <th>Category Name</th>
                    <th>Brand</th>
                    {{-- <th>SubCategory Name</th> --}}
                   
                    <th>Price | Quantity</th>
                    <th>Discounted Price</th>
                    <th>Description</th>
                    <th>Image</th>
                    <th>Status</th>
                    <th>Create Info</th>
                    <th>Update Info</th>
                    <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</section>
@endsection
@push('js')
<script>
    $(function () {
      $('#example1').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false,
        "scrollX": true
      })
    })
  </script>

  <script type="text/javascript">
  function deleteProduct(id) {
   const swalWithBootstrapButtons = Swal.mixin({
  customClass: {
    confirmButton: 'btn btn-success',
    cancelButton: 'btn btn-danger'
  },
  buttonsStyling: false
})

swalWithBootstrapButtons.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, cancel!',
  reverseButtons: true
}).then((result) => {
  if (result.value) {
    event.preventDefault();
      document.getElementById('delete-form-'+id).submit();
  } else if (
    /* Read more about handling dismissals below */
    result.dismiss === Swal.DismissReason.cancel
  ) {
    swalWithBootstrapButtons.fire(
      'Cancelled',
      'Your imaginary file is safe :)',
      'error'
    )
  }
})
  }
  $('.showproduct').click(function() {
    $('.image_display').text('');
    $('#prodd_id').text('');
    var parenttr = $(this).parents('tr');
    console.log(parenttr);
    var product_name =  $(parenttr).find("td.prod_name").data('name');
    var category_name = $(parenttr).find("td.prod_cat").data('categoryname');
    var brand_name = $(parenttr).find("td.prod_brand").data('brandname');
    // var subcategory_name = $(parenttr).find("td.prod_subcat").data('subcategoryname');
    var description = $(parenttr).find("td.prod_desc").data('description');
    var price = $(parenttr).find("td.prod_price").data('price');
    var quantity = $(parenttr).find("td.prod_price").data('quantity');
    var is_active = $(parenttr).find("td.prod_is_active").data('is_active');
    var active_status = '';
    var active_status_color = '';
    if(is_active == true)
    {
        active_status = "Active";
        active_status_color = "label-success"
    } else {
        active_status = "InActive";
        active_status_color = "label-danger";
    }
    var is_hot_product = $(parenttr).find("td.prod_is_active").data('is_hot_product');
    var is_hot_product_status = '';
    if(is_hot_product ==true)
    {
        is_hot_product_status = "<i class='text-success fa fa-check'></i>";
    } else {
        is_hot_product_status = "<i class='text-danger fa fa-close'></i>";
    }
    var is_special_product = $(parenttr).find("td.prod_is_active").data('is_special_product');
    var is_special_product_status = '';
    if(is_special_product ==true)
    {
        is_special_product_status = "<i class='text-success fa fa-check'></i>";
    } else {
        is_special_product_status = "<i class='text-danger fa'>&#xf00d;</i>";
    }
    var is_dailyneed_product = $(parenttr).find("td.prod_is_active").data('is_dailyneed_product');
    var is_dailyneed_product_status = '';
    if(is_dailyneed_product ==true)
    {
        is_dailyneed_product_status = "<i class='text-success fa fa-check'></i>";
    } else {
        is_dailyneed_product_status = "<i class='text-danger fa'>&#xf00d;</i>";
    }
    var image = $(parenttr).find("td.prod_image").data('image');
    $('#prduct_name').text(product_name);
    $('#category_name').text(category_name);
    $('#brand_name').text(brand_name);
    // $('#subcategory_name').text(subcategory_name);
    $('#description_show').text(description);
    $('#price_show').text(price+'|'+quantity);
    $('#quantity_show').text(quantity);
    $('#is_dailyneed_product').text(is_dailyneed_product);
    $('#is_active_show').html("<span class='label "+active_status_color+"'>"+active_status+"</span>");
    $('#is_hot_product_show').html("<span>"+is_hot_product_status+"</span>");
    $('#is_special_product_show').html("<span>"+is_special_product_status+"</span>");
    $('#is_dailyneed_product_show').html("<span>"+is_dailyneed_product_status+"</span>");
    var imagesplit = image.split(",");
    var image_path = '{{ URL::asset('/storage') }}';
    $.each(imagesplit,function(i){
        $('.image_display').append("<div class='col-lg-4  img-responsive  mt-2'><img src='"+image_path+"/uploads/product/thumbnails/"+imagesplit[i].trim()+"' width='100%'></div>");
    });

    $('#showmodal').modal('show');
  });
  </script>
@endpush
@yield('js')
