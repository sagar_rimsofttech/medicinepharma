@extends('adminlte::page')

@section('title', 'ProductReview Master')
@section('css')
<style>
.mt-2 {
    margin-top: 1rem!important;
}
</style>
@endsection
@section('content')

<section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">ProductReview Listing</h3>
            </div>
            <!-- The Modal -->
           


            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="{{route('productreview.create')}}">
                            <button class="btn btn-success pull-right">
                                    Create <span class="badge badge-primary">new</span>
                            </button>
                        </a>
                            <button class="btn btn-success pull-left">
                                    Total ProductReview <span class="badge badge-primary">{{count($productreviews)}}</span>
                            </button>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Product Name</th>
                  <th>Comment</th>
                  <th>Image</th>
                  <th>Rating</th>
                  <th>Create Info</th>
                  <th>Update Info</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($productreviews as $productreview)
                    @php $imagearray = explode(',',$productreview->image); @endphp
                    <tr class="appenddata">
                      <td class="text-capitalize prod_brand" data-brandname="{{$productreview->product->name??""}}">{{$productreview->product->name??""}}</td>
                      <td class="text-capitalize prod_name" data-name="{{$productreview->name??""}}">{{$productreview->name??""}}</td>
                      <td class="image prod_image" data-image=" {{$productreview->image}}">
                        @foreach($imagearray as $key=>$prodimage)
                        <div class="col-sm-2">
                        <div class="user-block">
                             <img alt="{{$productreview->name}}" src="{{URL::asset('storage/uploads/productreview/thumbnails/'.$prodimage)}}" class="img-circle img-bordered-sm" >
                        </div>
                        </div>
    
                        @endforeach
                        </td>
                        <td class="text-capitalize prod_name" >
                          <div class="rating">
                            @for($i = 0; $i < 5; $i++)
                                <span><i class="fa fa-star{{ $productreview->rating <= $i ? '-o' : '' }}"></i></span>
                            @endfor
                        </div>
                        </td>
                        <td data-createdinfo="{{$productreview->create_info?$productreview->create_info:'' }}">{{$productreview->create_info?$productreview->create_info:'' }}</td>
                    <td data-updateinfo="{{$productreview->update_info?$productreview->update_info:'' }}">{{$productreview->update_info?$productreview->update_info:'' }}</td>
                        <td class="prod_id" data-id="{{$productreview->id}}">
                          <div class="btn-group btn-group-sm">
                            
                                
                              <a href="{{ route('productreview.edit',$productreview->id) }}" class="edit-model btn btn-warning btn-sm " ><i class="fa fa-edit"></i></a>
                                  <button class="delete-model btn btn-danger btn-sm " type="button" onclick="deleteProductReview({{ $productreview->id }})">
                                      <i class="fa fa-trash"></i>
                                  </button>
                                  <form id="delete-form-{{ $productreview->id }}" action="{{ route('productreview.destroy',$productreview->id) }}" method="POST" style="display: none;">
                                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  </form>
                          </div>
                          </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>Product Name</th>
                  <th>Comment</th>
                  <th>Image</th>
                  <th>Rating</th>
                  <th>Create Info</th>
                  <th>Update Info</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</section>
@endsection
@push('js')
<script>
    $(function () {
      $('#example1').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false,
        "scrollX": true
      })
    })
  </script>

  <script type="text/javascript">
  function deleteProductReview(id) {
   const swalWithBootstrapButtons = Swal.mixin({
  customClass: {
    confirmButton: 'btn btn-success',
    cancelButton: 'btn btn-danger'
  },
  buttonsStyling: false
})

swalWithBootstrapButtons.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, cancel!',
  reverseButtons: true
}).then((result) => {
  if (result.value) {
    event.preventDefault();
      document.getElementById('delete-form-'+id).submit();
  } else if (
    /* Read more about handling dismissals below */
    result.dismiss === Swal.DismissReason.cancel
  ) {
    swalWithBootstrapButtons.fire(
      'Cancelled',
      'Your imaginary file is safe :)',
      'error'
    )
  }
})
  }
  
  </script>
@endpush
@yield('js')
