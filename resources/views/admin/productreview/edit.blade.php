@extends('adminlte::page')

@section('title', 'Edit ProductReview Details')

@section('content')
<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit ProductReview</div>
                    <div class="panel-body">
                        <a href="{{ route('productreview.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @include('admin.alertMessage')
                        {!! Form::open(['route' => ['productreview.update', $productreview->id], 'class' => 'form-horizontal','method'=>'POST','id'=>'productreviewUpdationForm','enctype'=>'multipart/form-data']) !!}

                        <div class="form-group {{ $errors->has('product_id') ? 'has-error' : ''}}">
                            <label for="product" class = 'col-md-4 control-label'>Product</label>
                            <div class="col-md-6">
                            <select id="product_id" class="selectpicker form-control product-select2"
                            data-show-subtext="true" name="product_id" data-live-search="true" title="">
                            </select>

                            </div>
                            <small id="helpId" class="text-muted">Help text</small>
                     </div>
                     <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                        <label for="name" class = 'col-md-4 control-label'>Comment</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="name" id="name" value="{{ $productreview->name}}">
                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                        <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                            <label for="image" class = 'col-md-4 control-label'>Product Image</label>
                            <div class="col-md-6">
                                <input type="file" class="form-control" name="image[]" id="image"multiple>
                                {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                                <br>
                                @foreach(explode(",",$productreview->image) as $key=>$prodimage)
                                <div class="col-sm-2">
                                <div class="user-block">
                                    <img alt="{{$productreview->name}}" src="{{URL::asset('storage/uploads/productreview/thumbnails/'.$prodimage)}}" class="img-circle " >
                                </div>
                                </div>

                                @endforeach
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('rating') ? 'has-error' : ''}}">
                            <label for="rating" class = 'col-md-4 control-label'>Rating</label>
                            <div class="col-md-6">
                            <input id="rating" name="rating" type="radio" class="" value="1" {{ $productreview->rating==1 ? 'checked' : '' }}/>1
                            <input id="rating" name="rating" type="radio" class="" value="2" {{ $productreview->rating==2 ? 'checked' : '' }}/>2
                            <input id="rating" name="rating" type="radio" class="" value="3" {{ $productreview->rating==3 ? 'checked' : '' }}/>3
                            <input id="rating" name="rating" type="radio" class="" value="4" {{ $productreview->rating==4 ? 'checked' : '' }}/>4
                            <input id="rating" name="rating" type="radio" class="" value="5" {{ $productreview->rating==5 ? 'checked' : '' }}/>5
                            </div>
                        </div>
                        <input type="hidden" name="hidden_id" value="{{$productreview->id}}">


                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-success">Update</button>
                        </div>


                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')

    {{--{!! Html::script('vendor/select2/dist/js/select2.min.js') !!}--}}
    {!! Html::script('js/bootstrap3-wysihtml5.all.min.js') !!}
    {!! Html::style('vendor/formvalidation/css/formValidation.min.css') !!}
    {!! Html::script('vendor/formvalidation/js/formValidation.min.js') !!}
    {!! Html::script('vendor/formvalidation/js/framework/bootstrap.min.js') !!}

    <script>
    $(document).ready(function () {
        var product = "{{$productreview->product_id}}";
        var product_name = "{{$productreview->product_name}}";
      
        $('.product-select2').append("<option value="+product+" selected>"+product_name+"</option>").trigger('change');


        $(".product-select2").select2({
        ajax: {
            url: function (params) {
                return "{{route('search.product').'/'}}" + params.term;
            },
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {q: params.term, };
            },
            processResults: function (data, params) {
                return {results: data};
            },
            cache: true
        },
        minimumInputLength: 3,
    });
   

        
    })
    
            $("#productreviewUpdationForm").submit(function(e){
               
                document.getElementById('productreviewUpdationForm').submit();
               
               
            });
    </script>
@endpush
@yield('js')
