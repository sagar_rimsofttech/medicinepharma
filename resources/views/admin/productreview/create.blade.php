@extends('adminlte::page')

@section('title', 'Create ProductReview')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Create ProductReview</div>
                    <div class="panel-body">
                        <a href="{{ route('productreview.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        {{-- @include('admin.alertMessage') --}}

                        {!! Form::open(['route' => 'productreview.store', 'class' => 'form-horizontal','method'=>'POST','id'=>'productReviewRegistrationForm','enctype'=>"multipart/form-data"]) !!}

                       
                     
                        <div class="form-group {{ $errors->has('product_id') ? 'has-error' : ''}}">
                                <label for="product" class = 'col-md-4 control-label'>Product</label>
                                <div class="col-md-6">
                                <select id="product_id" class="selectpicker form-control product-select2"
                                data-show-subtext="true" name="product_id" data-live-search="true" title="">
                                </select>

                                </div>
                                <small id="helpId" class="text-muted">Help text</small>
                         </div>
                         <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                            <label for="name" class = 'col-md-4 control-label'>Comment</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" id="name" value="{{ old('name')}}">
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                       

                        <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                            <label for="image" class = 'col-md-4 control-label'>Product Image</label>
                            <div class="col-md-6">
                                <input type="file" class="form-control" name="image[]" id="image" multiple>
                                {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="col-lg-12">

                        </div>
                        <div class="form-group {{ $errors->has('rating') ? 'has-error' : ''}}">
                            <label for="rating" class = 'col-md-4 control-label'>Rating</label>
                            <div class="col-md-6">
                            <input id="rating" name="rating" type="radio" class="" value="1" {{ old('is_active') ? 'checked' : '' }}/>1
                            <input id="rating" name="rating" type="radio" class="" value="2" {{ old('is_active') ? 'checked' : '' }}/>2
                            <input id="rating" name="rating" type="radio" class="" value="3" {{ old('is_active') ? 'checked' : '' }}/>3
                            <input id="rating" name="rating" type="radio" class="" value="4" {{ old('is_active') ? 'checked' : '' }}/>4
                            <input id="rating" name="rating" type="radio" class="" value="5" {{ old('is_active') ? 'checked' : '' }}/>5
                            </div>
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
         $(document).ready(function () {

            $(".product-select2").select2({
                ajax: {
                    url: function (params) {
                        return "{{route('search.product').'/'}}" + params.term;
                    },
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {q: params.term, };
                    },
                    processResults: function (data, params) {
                        return {results: data};
                    },
                    cache: true
                },
                minimumInputLength: 3,
            });

      
        })
          </script>
@endpush
@yield('js')
