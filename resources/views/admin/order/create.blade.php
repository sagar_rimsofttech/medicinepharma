@extends('adminlte::page')

@section('title', 'Create Product')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Create Product</div>
                    <div class="panel-body">
                        <a href="{{ route('product.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        {{-- @include('admin.alertMessage') --}}

                        {!! Form::open(['route' => 'product.store', 'class' => 'form-horizontal','method'=>'POST','id'=>'productRegistrationForm','enctype'=>"multipart/form-data"]) !!}

                        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                            <label for="name" class = 'col-md-4 control-label'>Product Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" id="name" value="{{ old('name')}}">
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                            <label for="description" class = 'col-md-4 control-label'>Product description</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="description" id="description" value="{{ old('description')}}">
                                {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
                                <label for="category" class = 'col-md-4 control-label'>Category</label>
                                <div class="col-md-6">
                                <select id="category_id" class="selectpicker form-control category-select2"
                                data-show-subtext="true" name="category_id" data-live-search="true" title="">
                                </select>

                                </div>
                                <small id="helpId" class="text-muted">Help text</small>
                         </div>
                         <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
                            <label for="brand" class = 'col-md-4 control-label'>Brand</label>
                            <div class="col-md-6">
                            <select id="brand_id" class="selectpicker form-control brand-select2"
                            data-show-subtext="true" name="brand_id" data-live-search="true" title="">
                            </select>

                            </div>
                            <small id="helpId" class="text-muted">Help text</small>
                     </div> 
                         {{-- <div class="form-group {{ $errors->has('subcategory_id') ? 'has-error' : ''}}">
                            <label for="subcategory" class = 'col-md-4 control-label'>SubCategory</label>
                            <div class="col-md-6">
                            <select id="subcategory_id" class="selectpicker form-control subcategory-select2"
                            data-show-subtext="true" name="subcategory_id" data-live-search="true" title="">
                            </select>

                            </div>
                            <small id="helpId" class="text-muted">Help text</small>
                        </div> --}}
                        <div class="form-group {{ $errors->has('quantity') ? 'has-error' : ''}}">
                            <label for="quantity" class = 'col-md-4 control-label'>Product Quantity</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control" name="quantity" id="quantity" value="{{ old('quantity')}}">
                                {!! $errors->first('quantity', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
                            <label for="price" class = 'col-md-4 control-label'>Product Price</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control" name="price" id="price" value="{{ old('price')}}">
                                {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
                            <label for="price" class = 'col-md-4 control-label'>Discounted Price</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control" name="discountedprice" id="discountedprice" value="{{ old('discountedprice')}}">
                                {!! $errors->first('discountedprice', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                            <label for="image" class = 'col-md-4 control-label'>Product Image</label>
                            <div class="col-md-6">
                                <input type="file" class="form-control" name="image[]" id="image" multiple>
                                {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="col-lg-12">

                        </div>
                        <div class="form-group {{ $errors->has('is_hot_product') ? 'has-error' : ''}}">
                            <label for="is_hot_product" class = 'col-md-4 control-label'>Is Hot Product</label>
                            <div class="col-md-6">
                            <input id="is_hot_product" name="is_hot_product" type="checkbox" class="" value="1" {{ old('is_hot_product') ? 'checked' : '' }}/>
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('is_special_product') ? 'has-error' : ''}}">
                            <label for="is_special_product" class = 'col-md-4 control-label'>Is Special Product</label>
                            <div class="col-md-6">
                            <input id="is_special_product" name="is_special_product" type="checkbox" class="" value="1" {{ old('is_special_product') ? 'checked' : '' }}/>
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('is_dailyneed_product') ? 'has-error' : ''}}">
                            <label for="is_dailyneed_product" class = 'col-md-4 control-label'>Is Dailyneed Product</label>
                            <div class="col-md-6">
                            <input id="is_dailyneed_product" name="is_dailyneed_product" type="checkbox" class="" value="1" {{ old('is_dailyneed_product') ? 'checked' : '' }}/>
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('is_active') ? 'has-error' : ''}}">
                            <label for="is_active" class = 'col-md-4 control-label'>Active</label>
                            <div class="col-md-6">
                            <input id="is_active" name="is_active" type="checkbox" class="" value="1" {{ old('is_active') ? 'checked' : '' }}/>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
         $(document).ready(function () {

            $(".category-select2").select2({
                ajax: {
                    url: function (params) {
                        return "{{route('search.category').'/'}}" + params.term;
                    },
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {q: params.term, };
                    },
                    processResults: function (data, params) {
                        return {results: data};
                    },
                    cache: true
                },
                minimumInputLength: 3,
            });

            $(".brand-select2").select2({
                ajax: {
                    url: function (params) {
                        return "{{route('search.brand').'/'}}" + params.term;
                    },
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {q: params.term, };
                    },
                    processResults: function (data, params) {
                        return {results: data};
                    },
                    cache: true
                },
                minimumInputLength: 3,
            });
        //     $( ".category-select2" ) .change(function () {
        //     $("#subcategory_id").val('');
        //     let param_new = $(this).val();
        //     var param_category = "?category_id="+param_new;
        //     $(".subcategory-select2").select2({
        //     ajax: {
        //         url: function (param) {
        //             return "{{route('search.subcategory').'/'}}" + param.term + param_category;
        //         },
        //         dataType: 'json',
        //         delay: 250,
        //         data: function (param) {
        //             return {q: param.term, };
        //         },
        //         processResults: function (data, param) {
        //             return {results: data};
        //         },
        //         cache: true
        //     },
        //     // minimumInputLength: 3,
        // });
        // });
        })

            $("#productRegistrationFormg").submit(function(e){
                e.preventDefault();
                var name = $("#name").val();
                $.ajax({
                    url:"/admin/checkfordeletedproduct/"+name,
                    dataType:"json",
                    success: function (data) {
                        console.log("adasd"+data);
                        if(name === data.name)
                        {
                            const swalWithBootstrapButtons = Swal.mixin({
                            customClass: {
                                confirmButton: 'btn btn-success',
                                cancelButton: 'btn btn-danger'
                            },
                            buttonsStyling: false
                            })

                            swalWithBootstrapButtons.fire({
                            title: 'Given Product Name is already Exist (Deleted)!',
                            text: "Would Like to restored?",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Yes, restore it!',
                            cancelButtonText: 'No, cancel!',
                            reverseButtons: true
                            }).then((result) => {
                            if (result.value) {
                                $.ajax({
                                url:"/admin/restoredeletedproduct/"+name,
                                dataType:"json",
                                success: function (data) {
                                    // toastr.error("Category Restored Sucessfully.");
                                    toastr.success("<br /><br /><button type='button' id='confirmationRevertYes' class='btn btn-danger'>Ok</button>",'Product Restored Sucessfully!',
                                        {
                                            closeButton: false,
                                            allowHtml: true,
                                            positionClass:'toast-top-center',
                                            onShown: function (toast) {
                                                $("#confirmationRevertYes").click(function(){
                                                    window.location.href = "{{ route('product.index')}}";
                                                });
                                                }
                                        });
                                        setTimeout(function(){
                                            window.location.href = "{{ route('product.index')}}";
                                        }, 2000);

                                }
                                });
                            } else if (
                                /* Read more about handling dismissals below */
                                result.dismiss === Swal.DismissReason.cancel
                            ) {
                                swalWithBootstrapButtons.fire(
                                'Cancelled',
                                'Your cancled :)',
                                'error'
                                )
                            }
                            })
                        } else {
                            e.preventDefault();
                            document.getElementById('productyRegistrationForm').submit();
                        }
                    }
                });
            });
          </script>
@endpush
@yield('js')
