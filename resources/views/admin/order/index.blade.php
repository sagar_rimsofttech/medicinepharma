@extends('adminlte::page')

@section('title', 'Order Master')
@section('css')
<style>
.mt-2 {
    margin-top: 1rem!important;
}
.modal-full {
    min-width: 90%;
}

.modal-full .modal-content {
    min-height: 90vh;
}
    th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: auto;
        margin: 0 auto;
    }
</style>
@endsection
@section('content')

<section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Order Listing</h3>
            </div>
            {{-- {{dd($orders)}} --}}
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">
                            <button class="btn btn-success pull-left">
                                    Total Order <span class="badge badge-primary">{{count($orders)}}</span>
                            </button>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
              <table id="example1" class="table table-bordered table-striped wrap">
                <thead>
                <tr>
                  <th>Order Code</th>
                  <th>Order Date</th>
                  <th>Delivery Date</th>
                  <th>Amount</th>
                  <th>Payment Method</th>
                  <th>Payment Status</th>
                  <th>Transaction ID</th>
                  <th>Order Status</th>
                  <th>User ID</th>
                  <th>Delivery Address</th>
                  <th>Delivery Notes</th>
                  <th>Order From</th>
                  {{-- <th>Prescription Details</th> --}}
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($orders as $value)
                    {{-- {{!empty($value->user_delivery_address)?dd($value->useraddress):''}} --}}
                    <tr class="appenddataa">
                    <td>{{$value->order_code??""}}</td>
                    <td>{{$value->order_date??""}}</td>
                    <td>{{$value->delivery_date??""}}</td>
                    <td>{{$value->amount?$value->amount:'' }}</td>
                    <td>{{!empty($value->payment_method)?config("constants.ORDER.PAYMENT_TYPE")[$value->payment_method]:'' }}</td>
                    <td>{{!is_null($value->payment_status)?config("constants.ORDER.PAYMENT_STATUS")[$value->payment_status]:'' }}</td>
                    <td>{{$value->tarnsaction_id?$value->tarnsaction_id:'' }}</td>
                    <td>{{!empty($value->order_status)?config("constants.ORDER.ORDER_STATUS")[$value->order_status]:'' }}</td>
                    <td>{{!empty($value->user)?$value->user->name:'' }}</td>
                    <td class="wrap">{!! !empty($value->user_delivery_address)?'<p>'.$value->address.'</p>':'' !!}</td>
                    <td>{{$value->delivery_note?$value->delivery_note:'' }}</td>
                    <td>{{ !empty($value->order_from)?config("constants.ORDER.ORDER_FROM")[$value->order_from]:'' }}</td>
                    {{-- <td>{{$value->prescription_id?$value->prescription_id:'' }}</td> --}}
                    <?php $cart = unserialize($value->cart);
                    $cart_items = $cart->items;
                    $cart_totalprice = $cart->totalPrice;
                    $cart_totalquantity = $cart->totalQty;
                    ?>

                    <td class="showorder" data-id="{{$value->id}}"    data-total_price="{{$cart_totalprice}}" data-total_quantity="{{$cart_totalquantity}}">
                        <div class="btn-group btn-group-sm">
                            <button type="button" class="btn btn-success ">
                                <i class="fa fa-eye"></i>
                              </button>
                              {{-- <button id="showproduct">das</button> --}}
                            {{-- <a href="{{ route('product.show',$product->id) }}" id="myModal" class="edit-model btn btn-success btn-sm " ><i class="fa fa-eye"></i></a> --}}
                            {{-- <a href="{{ route('product.edit',$value->id) }}" class="edit-model btn btn-warning btn-sm " ><i class="fa fa-edit"></i></a>
                                <button class="delete-model btn btn-danger btn-sm " type="button" onclick="deleteProduct({{ $value->id }})">
                                    <i class="fa fa-trash"></i>
                                </button>
                                <form id="delete-form-{{ $value->id }}" action="{{ route('product.destroy',$value->id) }}" method="POST" style="display: none;">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </form> --}}
                        </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>Order Code</th>
                    <th>Order Date</th>
                    <th>Delivery Date</th>
                    <th>Amount</th>
                    <th>Payment Method</th>
                    <th>Payment Status</th>
                    <th>Transaction ID</th>
                    <th>Order Status</th>
                    <th>User ID</th>
                    <th>Delivery Address</th>
                    <th>Delivery Notes</th>
                    <th>Order From</th>
                    {{-- <th>Prescription Details</th> --}}
                    <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- The Modal -->
      <div class="modal" id="showmodal">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
            <h4 class="modal-title">Show Orders</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody class="showorderproduct">

                            </tbody>

                        </table>
                        <div class="showordertotal"></div>
                    </div>


                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
             <div id="prodd_id"></div>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
        </div>
    </div>
    </section>
    <!-- /.content -->
</section>
@endsection
@push('js')
<script>
    $(function () {
      $('#example1').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : true,
        'scrollCollapse': true,
        "scrollX"     : "200px",

      })
    })
    $('.showorder').click(function() {
        $('.showorderproduct').html('');
        var id = $(this).data('id');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }

        });
        $.ajax({
            type:'GET',
            url:"/admin/get/order",
            data:{id:id},
            dataType:"json",
            success: function (data) {
                console.log(data[0]);
                var image_path = '{{ URL::asset('/storage') }}';
                $.each(data[0].items, function( index, value ) {
                    console.log( value.item.brand_name );
                    var imagesplit = value.item.image.split(",");

                    $('.showorderproduct').append("<tr><td>"+value.item.name+"</td><td><img src='"+image_path+"/uploads/product/thumbnails/"+imagesplit[0].trim()+"' class='img-fluid img-circle img-md' width='100%'></td><td>"+value.item.discounted_price+"</td><td>"+value.qty+"</td><td>"+value.price+"</td></tr>");

                });
                $('.showordertotal').append("<ul style='list-style-type:none;' class='text-right'><li>Total Quantity : "+data[0].totalQty+"</li><li>Total :"+data[0].totalPrice+"</li></ul>");
                $('#showmodal').modal('show');

                // $('.appendaddressdata').prepend("<div class='card mt-3'><div class='card-body'>"+data.data.name+" - <span class='ml-4'> "+data.data.number+" </span> <p>"+data.data.address+" ,"+data.data.locality+","+data.data.landmark+", "+data.data.city+","+data.data.state+","+data.data.pincode+", Alternate number: "+data.data.alternatenumber+"</p></div></div>");
                // showorderproduct
                // $('.adddrresss').hide();
                // $('#addressmodel').modal('hide');

            }
        });
  });
  </script>

@endpush
@yield('js')
