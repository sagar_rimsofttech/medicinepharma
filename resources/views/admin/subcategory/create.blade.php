@extends('adminlte::page')

@section('title', 'Create SubCategory')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Create SubCategory</div>
                    <div class="panel-body">
                        <a href="{{ route('subcategory.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        {{-- @include('admin.alertMessage') --}}

                        {!! Form::open(['route' => 'subcategory.store', 'class' => 'form-horizontal','method'=>'POST','id'=>'subcategoryRegistrationForm','enctype'=>"multipart/form-data"]) !!}

                        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                            <label for="name" class = 'col-md-4 control-label'>SubCategory Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" id="name">
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                            <label for="description" class = 'col-md-4 control-label'>SubCategory description</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="description" id="description">
                                {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('categories') ? 'has-error' : ''}}">
                            {!! Html::decode(Form::label('categories','Categories <span class="text-danger">*</span>', ['class' => 'col-md-4 control-label'])) !!}
                            <div class="col-md-6">
                               {{Form::select('categories',$categories,null,array('name'=>'categories', 'class' => 'select2', 'style' => 'width: 100%'))}}
                               {!! $errors->first('categories', '
                               <p class="help-block">:message</p>
                               ') !!}
                            </div>
                         </div>
                        <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                            <label for="image" class = 'col-md-4 control-label'>SubCategory Image</label>
                            <div class="col-md-6">
                                <input type="file" class="form-control" name="image" id="image">
                                {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>



                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>


                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>



    <script>
            $(function () {
                //Initialize Select2 Elements
              $('.select2').select2()
              //Datemask dd/mm/yyyy
              $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
              //Datemask2 mm/dd/yyyy
              $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
              //Money Euro
              $('[data-mask]').inputmask()

              //Date range picker
              $('#reservation').daterangepicker()
              //Date range picker with time picker
              $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, locale: { format: 'MM/DD/YYYY hh:mm A' }})
              //Date range as a button
              $('#daterange-btn').daterangepicker(
                {
                  ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                  },
                  startDate: moment().subtract(29, 'days'),
                  endDate  : moment()
                },
                function (start, end) {
                  $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
              )

              //Date picker
              $('#datepicker').datepicker({
                autoclose: true
              })

              //iCheck for checkbox and radio inputs
              $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
              })
              //Red color scheme for iCheck
              $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
              })
              //Flat red color scheme for iCheck
              $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
              })

              //Colorpicker
              $('.my-colorpicker1').colorpicker()
              //color picker with addon
              $('.my-colorpicker2').colorpicker()

              //Timepicker
              $('.timepicker').timepicker({
                showInputs: false
              })
            })

            $("#subcategoryRegistrationForm").submit(function(e){
                e.preventDefault();
                var name = $("#name").val();
                $.ajax({
                    url:"/admin/checkfordeletedsubcategory/"+name,
                    dataType:"json",
                    success: function (data) {
                        console.log("adasd"+data);
                        if(name === data.name)
                        {
                            const swalWithBootstrapButtons = Swal.mixin({
                            customClass: {
                                confirmButton: 'btn btn-success',
                                cancelButton: 'btn btn-danger'
                            },
                            buttonsStyling: false
                            })

                            swalWithBootstrapButtons.fire({
                            title: 'Given SubCategory Name is already Exist (Deleted)!',
                            text: "Would Like to restored?",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Yes, restore it!',
                            cancelButtonText: 'No, cancel!',
                            reverseButtons: true
                            }).then((result) => {
                            if (result.value) {
                                $.ajax({
                                url:"/admin/restoredeletedsubcategory/"+name,
                                dataType:"json",
                                success: function (data) {
                                    // toastr.error("Category Restored Sucessfully.");
                                    toastr.success("<br /><br /><button type='button' id='confirmationRevertYes' class='btn btn-danger'>Ok</button>",'SubCategory Restored Sucessfully!',
                                        {
                                            closeButton: false,
                                            allowHtml: true,
                                            positionClass:'toast-top-center',
                                            onShown: function (toast) {
                                                $("#confirmationRevertYes").click(function(){
                                                    window.location.href = "{{ route('subcategory.index')}}";
                                                });
                                                }
                                        });
                                        setTimeout(function(){
                                            window.location.href = "{{ route('subcategory.index')}}";
                                        }, 2000);

                                }
                                });
                            } else if (
                                /* Read more about handling dismissals below */
                                result.dismiss === Swal.DismissReason.cancel
                            ) {
                                swalWithBootstrapButtons.fire(
                                'Cancelled',
                                'Your cancled :)',
                                'error'
                                )
                            }
                            })
                        } else {
                            e.preventDefault();
                            document.getElementById('subcategoryRegistrationForm').submit();
                        }
                    }
                });
            });
          </script>
@endpush
@yield('js')
