@extends('adminlte::page')

@section('title', 'SubCategory Master')

@section('content')

<section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">SubCategory Listing</h3>
            </div>


            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="{{route('subcategory.create')}}">
                            <button class="btn btn-success pull-right">
                                    Create <span class="badge badge-primary">new</span>
                            </button>
                        </a>
                            <button class="btn btn-success pull-left">
                                    Total SubCategory <span class="badge badge-primary">{{count($subcategories)}}</span>
                            </button>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Category Name</th>
                  <th>Description</th>
                  <th>Image</th>
                  <th>Create Info</th>
                  <th>Update Info</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($subcategories as $subcategory)
                    <tr>
                    <td class="text-capitalize">{{$subcategory->name??""}}</td>
                    <td class="text-capitalize">{{$subcategory->category->name??""}}</td>
                    <td>{{$subcategory->description??""}}</td>
                    <td class=" image"><img alt="{{$subcategory->name}}" src="{{URL::asset('storage/uploads/subcategory/thumbnails/'.$subcategory->image)}}" class="profile-user-img img-responsive img-circle" ></td>
                    <td>{{$subcategory->create_info?$subcategory->create_info:'' }}</td>
                    <td>{{$subcategory->update_info?$subcategory->update_info:'' }}</td>
                    <td>
                        <div class="btn-group btn-group-sm">
                            <a href="{{ route('subcategory.edit',$subcategory->id) }}" class="edit-model btn btn-warning btn-sm " ><i class="fa fa-edit"></i></a>
                                <button class="delete-model btn btn-danger btn-sm " type="button" onclick="deleteSubCategory({{ $subcategory->id }})">
                                    <i class="fa fa-trash"></i>
                                </button>
                                <form id="delete-form-{{ $subcategory->id }}" action="{{ route('subcategory.destroy',$subcategory->id) }}" method="POST" style="display: none;">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </form>
                        </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Category Name</th>
                    <th>Description</th>
                    <th>Image</th>
                    <th>Create Info</th>
                    <th>Update Info</th>
                    <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</section>
@endsection
@push('js')
<script>
    $(function () {
      $('#example1').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false,
        "scrollX": true
      })
    })
  </script>

  <script type="text/javascript">
  function deleteSubCategory(id) {
   const swalWithBootstrapButtons = Swal.mixin({
  customClass: {
    confirmButton: 'btn btn-success',
    cancelButton: 'btn btn-danger'
  },
  buttonsStyling: false
})

swalWithBootstrapButtons.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, cancel!',
  reverseButtons: true
}).then((result) => {
  if (result.value) {
    event.preventDefault();
      document.getElementById('delete-form-'+id).submit();
  } else if (
    /* Read more about handling dismissals below */
    result.dismiss === Swal.DismissReason.cancel
  ) {
    swalWithBootstrapButtons.fire(
      'Cancelled',
      'Your imaginary file is safe :)',
      'error'
    )
  }
})
  }</script>
@endpush
@yield('js')
