@extends('adminlte::page')

@section('title', 'Edit Category Details')

@section('content')
<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Category</div>
                    <div class="panel-body">
                        <a href="{{ route('category.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @include('admin.alertMessage')
                        {!! Form::open(['route' => ['category.update', $category->id], 'class' => 'form-horizontal','method'=>'POST','id'=>'categoryUpdationForm','enctype'=>'multipart/form-data']) !!}

                        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                            <label for="name" class = 'col-md-4 control-label'>Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" id="name" value="{{$category->name}}">
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                            <label for="description" class = 'col-md-4 control-label'>Description</label>
                            <div class="col-md-6">
                            <input type="text" class="form-control" name="description" id="description" value="{{$category->description}}">
                                {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                            <label for="image" class = 'col-md-4 control-label'>Category Image</label>
                            <div class="col-md-6">
                                <input type="file" class="form-control" name="image" id="image">
                                {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                                <img src="{{URL::asset('storage/uploads/category/thumbnails/'.$category->image)}}" class="img-circle thumbnail" alt="" width="20%">
                            </div>
                        </div>
                        <input type="hidden" name="hidden_id" value="{{$category->id}}">


                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-success">Update</button>
                        </div>


                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')

    {{--{!! Html::script('vendor/select2/dist/js/select2.min.js') !!}--}}
    {!! Html::script('js/bootstrap3-wysihtml5.all.min.js') !!}
    {!! Html::style('vendor/formvalidation/css/formValidation.min.css') !!}
    {!! Html::script('vendor/formvalidation/js/formValidation.min.js') !!}
    {!! Html::script('vendor/formvalidation/js/framework/bootstrap.min.js') !!}

    <script>
            $("#categoryUpdationForm").submit(function(e){
                e.preventDefault();
                var name = $("#name").val();
                $.ajax({
                    url:"/admin/checkfordeleted/"+name,
                    dataType:"json",
                    success: function (data) {
                        console.log("adasd"+data);
                        if(name === data.name)
                        {
                            const swalWithBootstrapButtons = Swal.mixin({
                            customClass: {
                                confirmButton: 'btn btn-success',
                                cancelButton: 'btn btn-danger'
                            },
                            buttonsStyling: false
                            })

                            swalWithBootstrapButtons.fire({
                            title: 'Given Category Name is already Exist (Deleted)!',
                            text: "Would Like to restored?",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Yes, restore it!',
                            cancelButtonText: 'No, cancel!',
                            reverseButtons: true
                            }).then((result) => {
                            if (result.value) {
                                $.ajax({
                                url:"/admin/restoredeleted/"+name,
                                dataType:"json",
                                success: function (data) {
                                    // toastr.error("Category Restored Sucessfully.");
                                    toastr.success("<br /><br /><button type='button' id='confirmationRevertYes' class='btn btn-danger'>Ok</button>",'Category Restored Sucessfully!',
                                        {
                                            closeButton: false,
                                            allowHtml: true,
                                            positionClass:'toast-top-center',
                                            onShown: function (toast) {
                                                $("#confirmationRevertYes").click(function(){
                                                    window.location.href = "{{ route('category.index')}}";
                                                });
                                                }
                                        });
                                        setTimeout(function(){
                                            window.location.href = "{{ route('category.index')}}";
                                        }, 2000);

                                }
                                });
                            } else if (
                                /* Read more about handling dismissals below */
                                result.dismiss === Swal.DismissReason.cancel
                            ) {
                                swalWithBootstrapButtons.fire(
                                'Cancelled',
                                'Your cancled :)',
                                'error'
                                )
                            }
                            })
                        } else {
                            e.preventDefault();
                            document.getElementById('categoryUpdationForm').submit();
                        }
                    }
                });
            });
    </script>
@endpush
@yield('js')
