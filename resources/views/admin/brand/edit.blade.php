@extends('adminlte::page')

@section('title', 'Edit Brand Details')

@section('content')
<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Brand</div>
                    <div class="panel-body">
                        <a href="{{ route('brand.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @include('admin.alertMessage')
                        {!! Form::open(['route' => ['brand.update', $brand->id], 'class' => 'form-horizontal','method'=>'POST','id'=>'brandUpdationForm','enctype'=>'multipart/form-data']) !!}

                        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                            <label for="name" class = 'col-md-4 control-label'>Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" id="name" value="{{$brand->name}}">
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                            <label for="description" class = 'col-md-4 control-label'>Description</label>
                            <div class="col-md-6">
                            <input type="text" class="form-control" name="description" id="description" value="{{$brand->description}}">
                                {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                            <label for="active" class = 'col-md-4 control-label'>Active</label>
                            <div class="col-md-6">
                                <input type="radio"  name="is_active" value="1" {{ $brand->is_active == 1 ? 'checked' : '' }}>Yes
                                <input type="radio"  name="is_active" value="0" {{ $brand->is_active == 0 ? 'checked' : '' }} >No
                                {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                       
                       
                        <input type="hidden" name="hidden_id" value="{{$brand->id}}">


                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-success">Update</button>
                        </div>


                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')

    {{--{!! Html::script('vendor/select2/dist/js/select2.min.js') !!}--}}
    {!! Html::script('js/bootstrap3-wysihtml5.all.min.js') !!}
    {!! Html::style('vendor/formvalidation/css/formValidation.min.css') !!}
    {!! Html::script('vendor/formvalidation/js/formValidation.min.js') !!}
    {!! Html::script('vendor/formvalidation/js/framework/bootstrap.min.js') !!}

    <script>
            $("#brandUpdationForm").submit(function(e){
                e.preventDefault();
                var name = $("#name").val();
                $.ajax({
                    url:"/admin/checkfordeletedbrand/"+name,
                    dataType:"json",
                    success: function (data) {
                        console.log("adasd"+data);
                        if(name === data.name)
                        {
                            const swalWithBootstrapButtons = Swal.mixin({
                            customClass: {
                                confirmButton: 'btn btn-success',
                                cancelButton: 'btn btn-danger'
                            },
                            buttonsStyling: false
                            })

                            swalWithBootstrapButtons.fire({
                            title: 'Given Brand Name is already Exist (Deleted)!',
                            text: "Would Like to restored?",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Yes, restore it!',
                            cancelButtonText: 'No, cancel!',
                            reverseButtons: true
                            }).then((result) => {
                            if (result.value) {
                                $.ajax({
                                url:"/admin/restoredeletedbrand/"+name,
                                dataType:"json",
                                success: function (data) {
                                    // toastr.error("Category Restored Sucessfully.");
                                    toastr.success("<br /><br /><button type='button' id='confirmationRevertYes' class='btn btn-danger'>Ok</button>",'Brand Restored Sucessfully!',
                                        {
                                            closeButton: false,
                                            allowHtml: true,
                                            positionClass:'toast-top-center',
                                            onShown: function (toast) {
                                                $("#confirmationRevertYes").click(function(){
                                                    window.location.href = "{{ route('brand.index')}}";
                                                });
                                                }
                                        });
                                        setTimeout(function(){
                                            window.location.href = "{{ route('brand.index')}}";
                                        }, 2000);

                                }
                                });
                            } else if (
                                /* Read more about handling dismissals below */
                                result.dismiss === Swal.DismissReason.cancel
                            ) {
                                swalWithBootstrapButtons.fire(
                                'Cancelled',
                                'Your cancled :)',
                                'error'
                                )
                            }
                            })
                        } else {
                            e.preventDefault();
                            document.getElementById('brandUpdationForm').submit();
                        }
                    }
                });
            });
    </script>
@endpush
@yield('js')
