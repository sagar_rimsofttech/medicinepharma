@extends('adminlte::page')

@section('title', 'Edit Product Details')

@section('content')
<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Product</div>
                    <div class="panel-body">
                        <a href="{{ route('product.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @include('admin.alertMessage')
                        {!! Form::open(['route' => ['product.update', $product->id], 'class' => 'form-horizontal','method'=>'POST','id'=>'productUpdationForm','enctype'=>'multipart/form-data']) !!}

                        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                            <label for="name" class = 'col-md-4 control-label'>Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" id="name" value="{{$product->name}}">
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                            <label for="description" class = 'col-md-4 control-label'>Product Description</label>
                            <div class="col-md-6">
                            <input type="text" class="form-control" name="description" id="description" value="{{$product->description}}">
                                {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
                            <label for="category" class = 'col-md-4 control-label'>Category</label>
                            <div class="col-md-6">
                            <select id="category_id" class="selectpicker form-control category-select2"
                            data-show-subtext="true" name="category_id" data-live-search="true" title="">
                            </select>

                            </div>
                            <small id="helpId" class="text-muted">Help text</small>
                        </div>
                        <div class="form-group {{ $errors->has('brand_id') ? 'has-error' : ''}}">
                            <label for="brand" class = 'col-md-4 control-label'>Brand</label>
                            <div class="col-md-6">
                            <select id="brand_id" class="selectpicker form-control brand-select2"
                            data-show-subtext="true" name="brand_id" data-live-search="true" title="">
                            </select>

                            </div>
                            <small id="helpId" class="text-muted">Help text</small>
                        </div>
                        <div class="form-group {{ $errors->has('quantity') ? 'has-error' : ''}}">
                            <label for="quantity" class = 'col-md-4 control-label'>Product Quantity</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control" name="quantity" id="quantity" value="{{$product->quantity}}">
                                {!! $errors->first('quantity', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
                            <label for="price" class = 'col-md-4 control-label'>Product Price</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control" name="price" id="price" value="{{$product->price}}">
                                {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
                            <label for="price" class = 'col-md-4 control-label'>Discounted Price</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control" name="discountedprice" id="discountedprice" value="{{$product->discounted_price}}">
                                {!! $errors->first('discountedprice', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                            <label for="active" class = 'col-md-4 control-label'>Active</label>
                            <div class="col-md-6">
                                <input type="radio"  name="is_active" value="1" {{ $product->is_active == 1 ? 'checked' : '' }}>Yes
                                <input type="radio"  name="is_active" value="0" {{ $product->is_active == 0 ? 'checked' : '' }} >No
                                {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('is_hot_product') ? 'has-error' : ''}}">
                            <label for="is_hot_product" class = 'col-md-4 control-label'>Is Hot Product</label>
                            <div class="col-md-6">
                            <input id="is_hot_product" name="is_hot_product" type="checkbox" class="" value="1" {{ $product->is_hot_product == 1 ? 'checked' : '' }}/>
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('is_special_product') ? 'has-error' : ''}}">
                            <label for="is_special_product" class = 'col-md-4 control-label'>Is Special Product</label>
                            <div class="col-md-6">
                            <input id="is_special_product" name="is_special_product" type="checkbox" class="" value="1" {{ $product->is_special_product == 1 ? 'checked' : '' }}/>
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('is_dailyneed_product') ? 'has-error' : ''}}">
                            <label for="is_dailyneed_product" class = 'col-md-4 control-label'>Is Dailyneed Product</label>
                            <div class="col-md-6">
                            <input id="is_dailyneed_product" name="is_dailyneed_product" type="checkbox" class="" value="1" {{ $product->is_dailyneed_product == 1 ? 'checked' : '' }}/>
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                            <label for="image" class = 'col-md-4 control-label'>Product Image</label>
                            <div class="col-md-6">
                                <input type="file" class="form-control" name="image[]" id="image"multiple>
                                {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                                <br>
                                @foreach(explode(",",$product->image) as $key=>$prodimage)
                                <div class="col-sm-2">
                                <div class="user-block">
                                    <img alt="{{$product->name}}" src="{{URL::asset('storage/uploads/product/thumbnails/'.$prodimage)}}" class="img-circle " >
                                </div>
                                </div>

                                @endforeach
                            </div>
                        </div>
                        <input type="hidden" name="hidden_id" value="{{$product->id}}">


                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-success">Update</button>
                        </div>


                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')

    {{--{!! Html::script('vendor/select2/dist/js/select2.min.js') !!}--}}
    {!! Html::script('js/bootstrap3-wysihtml5.all.min.js') !!}
    {!! Html::style('vendor/formvalidation/css/formValidation.min.css') !!}
    {!! Html::script('vendor/formvalidation/js/formValidation.min.js') !!}
    {!! Html::script('vendor/formvalidation/js/framework/bootstrap.min.js') !!}

    <script>
    $(document).ready(function () {
        var category = "{{$product->category_id}}";
        var category_name = "{{$product->category_name}}";
        var subcategory = "{{$product->subcategory_id}}";
        var subcategory_name = "{{$product->subcategory_name}}";
        var brand = "{{$product->brand_id}}";
        var brand_name = "{{$product->brand_name}}";
        $('.category-select2').append("<option value="+category+" selected>"+category_name+"</option>").trigger('change');
        $('.brand-select2').append("<option value="+brand+" selected>"+brand_name+"</option>").trigger('change');


        $(".category-select2").select2({
        ajax: {
            url: function (params) {
                return "{{route('search.category').'/'}}" + params.term;
            },
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {q: params.term, };
            },
            processResults: function (data, params) {
                return {results: data};
            },
            cache: true
        },
        minimumInputLength: 3,
    });
    $( ".category-select2" ) .change(function () {
    $("#subcategory_id").val('');
        let param_new = $(this).val();
        var param_category = "?category_id="+param_new;
        $(".subcategory-select2").select2({
        ajax: {
            url: function (param) {
                return "{{route('search.subcategory').'/'}}" + param.term + param_category;
            },
            dataType: 'json',
            delay: 250,
            data: function (param) {
                return {q: param.term, };
            },
            processResults: function (data, param) {
                return {results: data};
            },
            cache: true
        },
        // minimumInputLength: 3,
        });
        });

        $(".brand-select2").select2({
        ajax: {
            url: function (params) {
                return "{{route('search.brand').'/'}}" + params.term;
            },
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {q: params.term, };
            },
            processResults: function (data, params) {
                return {results: data};
            },
            cache: true
        },
        minimumInputLength: 3,
    });

        
    })
    
            $("#subcategoryUpdationForm").submit(function(e){
                e.preventDefault();
                var name = $("#name").val();
                $.ajax({
                    url:"/admin/checkfordeletedsubcategory/"+name,
                    dataType:"json",
                    success: function (data) {
                        console.log("adasd"+data);
                        if(name === data.name)
                        {
                            const swalWithBootstrapButtons = Swal.mixin({
                            customClass: {
                                confirmButton: 'btn btn-success',
                                cancelButton: 'btn btn-danger'
                            },
                            buttonsStyling: false
                            })

                            swalWithBootstrapButtons.fire({
                            title: 'Given SubCategory Name is already Exist (Deleted)!',
                            text: "Would Like to restored?",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Yes, restore it!',
                            cancelButtonText: 'No, cancel!',
                            reverseButtons: true
                            }).then((result) => {
                            if (result.value) {
                                $.ajax({
                                url:"/admin/restoredeletedsubcategory/"+name,
                                dataType:"json",
                                success: function (data) {
                                    // toastr.error("Category Restored Sucessfully.");
                                    toastr.success("<br /><br /><button type='button' id='confirmationRevertYes' class='btn btn-danger'>Ok</button>",'SubCategory Restored Sucessfully!',
                                        {
                                            closeButton: false,
                                            allowHtml: true,
                                            positionClass:'toast-top-center',
                                            onShown: function (toast) {
                                                $("#confirmationRevertYes").click(function(){
                                                    window.location.href = "{{ route('subcategory.index')}}";
                                                });
                                                }
                                        });
                                        setTimeout(function(){
                                            window.location.href = "{{ route('subcategory.index')}}";
                                        }, 2000);

                                }
                                });
                            } else if (
                                /* Read more about handling dismissals below */
                                result.dismiss === Swal.DismissReason.cancel
                            ) {
                                swalWithBootstrapButtons.fire(
                                'Cancelled',
                                'Your cancled :)',
                                'error'
                                )
                            }
                            })
                        } else {
                            e.preventDefault();
                            document.getElementById('subcategoryUpdationForm').submit();
                        }
                    }
                });
            });
    </script>
@endpush
@yield('js')
