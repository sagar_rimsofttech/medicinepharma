@extends('adminlte::page')

@section('title', 'Prescription Master')
@section('css')
<style>
.mt-2 {
    margin-top: 1rem!important;
}
.modal-full {
    min-width: 90%;
}

.modal-full .modal-content {
    min-height: 90vh;
}
.rotated {
  transform: rotate(90deg);
  -ms-transform: rotate(90deg);
  -moz-transform: rotate(90deg);
  -webkit-transform: rotate(90deg);
  -o-transform: rotate(90deg);
}
.prduct_name{transition: all 0.5s ease;}
</style>
@endsection
@section('content')

<section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Prescription Listing</h3>
            </div>
            <!-- The Modal -->
            <div class="modal" id="showmodal">
                <div class="modal-dialog modal-lg modal-full">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                    <h4 class="modal-title">Show Prescription</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="col-lg-6">
                                    <div class="prduct_name" id="prduct_name"></div>
                                </div>
                                <div class="col-lg-6 breadcrumb">
                                <form action="{{route('prescription.order')}}" method="post">
                                    {{csrf_field()}}
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <table class="table table-striped table order-list">
                                                    <thead>
                                                        <tr>
                                                        <th>Product</th>
                                                        <th>Price</th>
                                                        <th>Qunatity</th>
                                                        <th>Amount</th>
                                                        <th><button  id="addrow" class="btn btn-primary"><i class="fas fa-plus"></i></button> </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr class="appenddata">
                                                            <td>
                                                                <div class="form-group">
                                                                    <select class="form-control select2-multiple product-select2 product" data-toggle="select2" data-placeholder="Choose Product..." name="product_name[]" id="country_name">
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <input type="text" name="price[]" id="price" class="form-control price" onkeypress="return isNumber(event)" readonly>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <input type="text" name="quantity[]" id="quantity" class="form-control onlynumbers quantity" >
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <input type="number" name="amount[]" id="amount" class="form-control amount" readonly>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                    <input type="hidden" name="hidden_id" class="hidden_id" value="">
                                                    <input type="hidden" name="hidden_user_id" class="hidden_user_id" value="">
                                                    <input type="hidden" name="hidden_address_id" class="hidden_address_id" value="">
                                                    <input type="hidden" name="hidden_order_notes" class="hidden_order_notes" value="">
                                                </table>


                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="text-right mr-4">Total Price : Rs <span class="totalprice"></span></div>
                                            </div>
                                        </div>
                                          <!-- /.col -->
                                        <div class="col-lg-12 mb-6">
                                            <div class="text-right">
                                            <button type="submit" class="btn btn-primary ">
                                               Submit
                                            </button>
                                        </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                     <div id="prodd_id"></div>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                </div>
                </div>
            </div>


            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="{{route('prescription.create')}}">
                            <button class="btn btn-success pull-right">
                                    Create <span class="badge badge-primary">new</span>
                            </button>
                        </a>
                            <button class="btn btn-success pull-left">
                                    Total Prescription <span class="badge badge-primary">{{count($prescriptions)}}</span>
                            </button>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Prescription Code</th>
                  <th>Prescription</th>
                  <th>User</th>
                  <th>Prescription Date</th>
                  <th>Order Address</th>
                  <th>Order Notes</th>
                  <th>Create Info</th>
                  <th>Update Info</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($prescriptions as $prescription)
                    {{-- {{dd( \File::extension($prescription->file_name))}} --}}
                    {{-- <embed src="{{URL::asset('storage/uploads/prescription/'.$prescription->file_name)}}"
                    frameborder="0" width="100%" height="400px"> --}}
                    <tr class="appenddataa">
                    <td class="text-capitalize prod_name" data-name="{{$prescription->prescription_code??""}}">{{$prescription->prescription_code??""}}</td>
                    <td class="text-capitalize prescription_filename" data-filename="{{$prescription->file_name??""}}">{{$prescription->file_name??""}}</td>
                    <td class="text-capitalize user_id" data-user_id="{{$prescription->user_id??""}}">{{$prescription->user_id??""}} {{$prescription->user->name??""}}| {{$prescription->user->mobile_no??""}}</td>
                    <td class="prod_price" data-price="{{$prescription->prescription_date}}"><b>{{$prescription->prescription_date??""}}</b></td>
                    <td  class = "address_id" data-address_id="{{$prescription->address_id?$prescription->address_id:'' }}">{{$prescription->address_id?$prescription->address_id:'' }}</td>
                    <td  class = "order_notes" data-address_id="{{$prescription->order_notes?$prescription->order_notes:'' }}">{{$prescription->order_notes?$prescription->order_notes:'' }}</td>
                    <td data-createdinfo="{{$prescription->create_info?$prescription->create_info:'' }}">{{$prescription->create_info?$prescription->create_info:'' }}</td>
                    <td data-updateinfo="{{$prescription->update_info?$prescription->update_info:'' }}">{{$prescription->update_info?$prescription->update_info:'' }}</td>

                    <td class="prescription_id" data-id="{{$prescription->id}}">
                        <div class="btn-group btn-group-sm">
                            <button type="button" class="btn btn-success showprescription">
                                <i class="fa fa-eye"></i>
                              </button>
                              {{-- <button id="showproduct">das</button> --}}
                            {{-- <a href="{{ route('product.show',$product->id) }}" id="myModal" class="edit-model btn btn-success btn-sm " ><i class="fa fa-eye"></i></a> --}}
                            <a href="{{ route('product.edit',$prescription->id) }}" class="edit-model btn btn-warning btn-sm " ><i class="fa fa-edit"></i></a>
                                <button class="delete-model btn btn-danger btn-sm " type="button" onclick="deleteProduct({{ $prescription->id }})">
                                    <i class="fa fa-trash"></i>
                                </button>
                                <form id="delete-form-{{ $prescription->id }}" action="{{ route('product.destroy',$prescription->id) }}" method="POST" style="display: none;">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </form>
                        </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>Prescription Code</th>
                    <th>Prescription</th>
                    <th>User</th>
                    <th>Prescription Date</th>
                    <th>Order Address</th>
                    <th>Order Notes</th>
                    <th>Create Info</th>
                    <th>Update Info</th>
                    <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</section>
@endsection
@push('js')
<script>
    $(function () {
      $('#example1').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false,
        "scrollX": true
      })
    })
    $(document).ready(function () {
      $(".product-select2").select2({
        ajax: {
            url: function (params) {
                return "{{route('prescription.product').'/'}}" + params.term;
            },
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {q: params.term, };
            },
            processResults: function (data, params) {
                return {results: data};
            },
            cache: true
        },
        minimumInputLength: 3,
    });

    $('.product').change(function() {
        let product_id = $(this).val();
        var parenttr=$(this).parents('.appenddata');
        $.ajax({
            url:"/admin/getproduct/"+product_id,
            dataType:"json",
            success:function(html){
                console.log(html);

               parenttr.find('.price').val(html.price);

            }
            })

    })
    $('.quantity').on('keyup change',function(){
        var price = $(this).closest('tr').find("td input[name^='price']").val();
        var quantity = $(this).closest('tr').find("td input[name^='quantity']").val();
        var tot = price * quantity;
        $(this).closest('tr').find("td input[name^='amount']").val(tot);
        var tt =0;
        $('.amount').each(function(i, o) {

            if($.isNumeric($(o).val())){
            tt += parseInt($(o).val());
            }
        });
       $('.totalprice').html(tt);
    });

    });
  </script>

  <script type="text/javascript">
  function deleteProduct(id) {
   const swalWithBootstrapButtons = Swal.mixin({
  customClass: {
    confirmButton: 'btn btn-success',
    cancelButton: 'btn btn-danger'
  },
  buttonsStyling: false
})

swalWithBootstrapButtons.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, cancel!',
  reverseButtons: true
}).then((result) => {
  if (result.value) {
    event.preventDefault();
      document.getElementById('delete-form-'+id).submit();
  } else if (
    /* Read more about handling dismissals below */
    result.dismiss === Swal.DismissReason.cancel
  ) {
    swalWithBootstrapButtons.fire(
      'Cancelled',
      'Your imaginary file is safe :)',
      'error'
    )
  }
})
  }
  $('.showprescription').click(function() {
    $('.image_display').text('');
    $('.price').val('');
    $('.quantity').val('');
    $('.amount').val('');
    $('.product').text('');
    $('#prodd_id').text('');
    $('.totalprice').html('');
    var parenttr = $(this).parents('tr');
    console.log($(parenttr).find("td.prescription_id").data('id'));
    var prescription_filename =  $(parenttr).find("td.prescription_filename").data('filename');
    var prescription_id = $(parenttr).find("td.prescription_id").data('id');
    var user_id = $(parenttr).find("td.user_id").data('user_id');
    var address_id = $(parenttr).find("td.address_id").data('address_id');
    var order_notes = $(parenttr).find("td.order_notes").data('order_notes');
    var file_path = '{{ URL::asset('/storage') }}';
    $('#prduct_name').html("<embed src='"+file_path+"/uploads/prescription/"+prescription_filename.trim()+"' frameborder='0' width='100%' height='700px' class='rotate'>");
    $('.hidden_id').val(prescription_id);
    $('.hidden_user_id').val(user_id);
    $('.hidden_address_id').val(address_id);
    $('.hidden_order_notes').val(order_notes);


    $('#showmodal').modal('show');
  });
  var counter = 0;
  $("#addrow").on("click", function (e) {
        e.preventDefault()

        var newRow = $("<tr class='appenddata'>");
        var cols = "";
        var col =[];

        cols += '<td><div class="form-group"><div class="input-group"><select  class="form-control product-select2 product" id="product" name="product_name[]" style="width: 127px;"></select></div></div></td>';
        cols += '<td><div class="form-group"><div class="input-group"><input type="text" class="form-control price" name="price[]" readonly/></div></div></td>';
        cols += '<td><div class="form-group"><div class="input-group quantity"><input type="number" class="form-control quantity" name="quantity[]"/></div></div></td>';
        cols += '<td><div class="form-group"><div class="input-group amount"><input type="number" class="form-control amt" name="amount[]"readonly/></div></div></td>';

        cols += '<td><button  class="ibtnDel btn btn-primary"><i class="fas fa-minus"></i></button></td>';
        newRow.append(cols);
        newRow.find('.amt').addClass('amount').removeClass('amt');
        newRow.find(".product-select2").select2({
        ajax: {
            url: function (params) {
                return "{{route('prescription.product').'/'}}" + params.term;
            },
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {q: params.term, };
            },
            processResults: function (data, params) {
                return {results: data};
            },
            cache: true
        },
        minimumInputLength: 3,
    });
    newRow.find('.product').change(function() {
        let product_id = $(this).val();
        var parenttr=$(this).parents('.appenddata');
        $.ajax({
            url:"/admin/getproduct/"+product_id,
            dataType:"json",
            success:function(html){
                var price = html.price;
                parenttr.find('.price').val(html.price);
            }
            })

    })
    newRow.find('.quantity').on('keyup change',function(){
        var price = $(this).closest('tr').find("td input[name^='price']").val();
        var quantity = $(this).closest('tr').find("td input[name^='quantity']").val();
        var tot = price * quantity;
        $(this).closest('tr').find("td input[name^='amount']").val(tot);
        console.log($('.amount').val());
        var tt =0;
        $('.amount').each(function(i, o) {

            if($.isNumeric($(o).val())){
            tt += parseInt($(o).val());
            }
        });
       $('.totalprice').html(tt);
    });
    newRow.on('change', '.product', function (e) {
            var val = $(this).val();
            var selects = $('.product').not(this);
            for (var index = 0; index < selects.length; index++) {
                if (val === $(selects[index]).val()) {
                    toastr.error("This Product is already added.");
                    $(this).val("").trigger('change');
                    break;
                }
            }
        });

        $("table.order-list").append(newRow);
    //     $("table.order-list").find(".flat-red").iCheck({
    //   checkboxClass: 'icheckbox_flat-green',
    //   radioClass   : 'iradio_flat-green'
    // });
        counter++;

    });



    $("table.order-list").on("click", ".ibtnDel", function (event) {
        event.preventDefault()
        $(this).closest("tr").remove();
        var tt =0;
        $('.amount').each(function(i, o) {

            if($.isNumeric($(o).val())){
            tt += parseInt($(o).val());
            }
        });
       $('.totalprice').html(tt);
        counter -= 1
    });

    var degrees = 0;
  $('.prduct_name').click(function rotateMe(e) {

    degrees += 90;

    //$('.img').addClass('rotated'); // for one time rotation

    $('.prduct_name').css({

      'transform': 'rotate(' + degrees + 'deg)',
      '-ms-transform': 'rotate(' + degrees + 'deg)',
      '-moz-transform': 'rotate(' + degrees + 'deg)',
      '-webkit-transform': 'rotate(' + degrees + 'deg)',
      '-o-transform': 'rotate(' + degrees + 'deg)'
    });
  });
  </script>
@endpush
@yield('js')
