<?php
$link_limit = 7; // maximum number of links (a little bit inaccurate, but will be ok for now)
?>
<style>
    .page-link {
    position: relative;
    display: block;
    padding: .5rem .75rem;
    margin-left: -1px;
    line-height: 1.25;
    color: #7fad39;
    background-color: #fff;
    border: 1px solid #dee2e6;
}
.page-item.active .page-link {
    z-index: 3;
    color: #fff;
    background-color: #7fad39;
    border-color: #a6ff00;
}
    </style>
@if ($paginator->lastPage() > 1)
<section class="pagination-section mt-3 mb-3">
    <div class="row">
        <div id="news_paginate" class="dataTables_paginate paging_simple_numbers col-md-12">
            <ul class="pagination justify-content-center">
                <li id="news_previous" class="paginate_button page-item previous {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
                    <a class="page-link" tabindex="0" href="{{ $paginator->url( (integer) request()->get('page') - 1) }}">Previous</a>
                </li>
                @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                    <?php
                        $half_total_links = floor($link_limit / 2);
                        $from = $paginator->currentPage() - $half_total_links;
                        $to = $paginator->currentPage() + $half_total_links;
                        if ($paginator->currentPage() < $half_total_links) {
                            $to += $half_total_links - $paginator->currentPage();
                        }
                        if ($paginator->lastPage() - $paginator->currentPage() < $half_total_links) {
                            $from -= $half_total_links - ($paginator->lastPage() - $paginator->currentPage()) - 1;
                        }
                    ?>
                    @if ($from < $i && $i < $to)
                        <li class="paginate_button page-item success{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                            <a class="page-link success" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                        </li>
                    @endif
                @endfor
                <li id="news_next" class="paginate_button page-item {{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
                    @if($paginator->currentPage() == $paginator->lastPage())
                        <a class="page-link success" tabindex="0" href="{{ $paginator->url($paginator->currentPage()) }}" >End</a>
                    @else
                        <a class="page-link success" tabindex="0" href="{{ $paginator->url($paginator->currentPage()+1) }}" >Next</a>
                    @endif
                </li>
            </ul>
        </div>
    </div>
</section>
@endif
