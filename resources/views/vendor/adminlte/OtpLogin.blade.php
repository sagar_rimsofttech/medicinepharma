@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/login/style.css') }}">
    @yield('css')
@stop

@section('body_class', 'login-page')

@section('body')
    {{-- <div class="login-box">
        <div class="login-logo">
            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">{{ trans('adminlte::adminlte.login_message') }}</p>
            <form action="{{ url(config('adminlte.login_url', 'login')) }}" method="post">
                {{ csrf_field() }}

                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input type="email" name="email" class="form-control" value="{{ old('email') }}"
                           placeholder="{{ trans('adminlte::adminlte.email') }}">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <input type="password" name="password" class="form-control"
                           placeholder="{{ trans('adminlte::adminlte.password') }}">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="icheck-primary">
                            <input type="checkbox" name="remember" id="remember">
                            <label for="remember">{{ trans('adminlte::adminlte.remember_me') }}</label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">
                            {{ trans('adminlte::adminlte.sign_in') }}
                        </button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <br>
            <p>
                <a href="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}" class="text-center">
                    {{ trans('adminlte::adminlte.i_forgot_my_password') }}
                </a>
            </p>
            {{-- @if (config('adminlte.register_url', 'register'))
                <p>
                    <a href="{{ url(config('adminlte.register_url', 'register')) }}" class="text-center">
                        {{ trans('adminlte::adminlte.register_a_new_membership') }}
                    </a>
                </p>
            @endif --}}
        {{-- </div> --}}
        <!-- /.login-box-body -->
    {{-- </div> --}}
    <!-- /.login-box -->


    <div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">

				<?php
                $imagesDir = 'frontend/img/loginpage/';
                $images = glob($imagesDir . '*.{jpg,jpeg,png,gif}', GLOB_BRACE);
                $random_img = basename($images[array_rand($images)]); ?>

				<div class="login100-pic js-tilt" data-tilt>
					<img src="frontend/img/loginpage/{{$random_img}}"  alt="IMG">
                </div>

                {{-- <form action="{{ url(config('adminlte.login_url', 'login')) }}" method="post">
                    {{ csrf_field() }} --}}
				<form class="login100-form validate-form" action="{{ route('loginWithOtp') }}" method="post">
                    @csrf

                    <span class="login100-form-title">
						Member Login
                    </span>
                    {{-- @if(Session::has('message'))
                    <div class="alert alert-success">
                        {{Session::get('message')}}</div>
                        @endif --}}


					<div class="wrap-input100 validate-input {{ $errors->has('mobile') ? 'has-error' : '' }}" data-validate = "Valid Mobile number is required: ex@abc.xyz">
						<input class="input100 onlynumbers" id="mobile" type="text" name="mobile" placeholder="Mobile Number">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>

                    </div>
                    @if ($errors->has('mobile'))
                        <span class="help-block">
                            <strong>{{ $errors->first('mobile') }}</strong>
                        </span>
                    @endif
                    {{-- <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                        <input type="email" name="email" class="form-control" value="{{ old('email') }}"
                               placeholder="{{ trans('adminlte::adminlte.email') }}">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div> --}}
					<div class="wrap-input100 otp validate-input {{ $errors->has('otp') ? 'has-error' : '' }}" data-validate = "OTP is required">
						<input class="input100 " type="otp" name="otp" placeholder="Otp">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
                        </span>

                    </div>
                    @if ($errors->has('otp'))
                        <span class="help-block">
                            <strong>{{ $errors->first('otp') }}</strong>
                        </span>
                    @endif
                    {{-- <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                        <input type="password" name="password" class="form-control"
                               placeholder="{{ trans('adminlte::adminlte.password') }}">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div> --}}

					<div class="container-login100-form-btn">
						{{-- <button class="login100-form-btn">
							Login
                        </button> --}}
                        <button type="submit" class="login100-form-btn otp">
                            {{ trans('adminlte::adminlte.sign_in') }}
                        </button>
                    </div>

                    {{-- <div class="container-login100-form-btn send-otp">
                        <div class="col-md-8 offset-md-4">
                            <button   class="login100-form-btn" id="sendOtp">
                                Send OTP
                            </button>
                        </div>
                    </div> --}}
					{{-- <div class="text-center p-t-12">
                        <a href="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}" class="text-center">
                            {{ trans('adminlte::adminlte.i_forgot_my_password') }}
                        </a>
                    </div> --}}
                    <div class="text-center p-t-12">
                        <a href="{{route('login.google',['provide'=>'Google'])}}" class="loginBtn loginBtn--google">Log With Google <i class="fa fa-google" aria-hidden="true"></i></a>
                        @if (config('adminlte.register_url', 'register'))
                        <div class="text-center p-t-3">
                            <a class="" href="{{ url(config('adminlte.register_url', 'register')) }}">
                                Create your Account
                                <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                            </a>
                        </div>
                        @endif
                    </div>
                    <div class="container-login100-form-btn send-otp">
                        <div class="col-md-8 offset-md-4">
                            <button   class="login100-form-btn" id="sendOtp">
                                Send OTP
                            </button>
                        </div>
                    </div>
                {{-- @if (config('adminlte.register_url', 'register'))
					<div class="text-center p-t-136">
						<a class="txt2" href="{{ url(config('adminlte.register_url', 'register')) }}">
							Create your Account
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
                    </div>
                    @endif --}}
                </form>

			</div>
		</div>
	</div>

@stop

@section('adminlte_js')
    @yield('js')
    <script>


            $(function () {
            $('.otp').hide();
            $('#sendOtp').on('click',function (e){
                $('#mobile').attr('readonly',true);

                e.preventDefault();
                $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }

                });
                $.ajax({
                    type:'POST',
                    url:'sendOtp',
                    data: {'mobile': $('#mobile').val(), "_token": "{{ csrf_token() }}"},
                    dataType:"json",
                success:function(data) {
                    // alert(data);
                    if(data.Message != ''){
                        toastr.error(data.Message);
                        $('.otp').show();
                        $('.send-otp').hide();
                    }else{
                        $('#mobile').attr('readonly',false);

                    }

                },
                error:function (jqXHR, exception) {
                    $('#mobile').attr('readonly',false);
                    toastr.error(jqXHR.responseJSON.Message);
                }
            });
        });
            });

    </script>
@stop
