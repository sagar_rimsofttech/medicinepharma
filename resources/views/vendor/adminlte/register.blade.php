@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet" href="{{ asset('frontend/css/login/style.css') }}">
    @yield('css')
@stop

@section('body_class', 'register-page')

@section('body')
    {{-- <div class="register-box">
        <div class="register-logo">
            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</a>
        </div>

        <div class="register-box-body">
            <p class="login-box-msg">{{ trans('adminlte::adminlte.register_message') }}</p>
            <form action="{{ url(config('adminlte.register_url', 'register')) }}" method="post">
                {{ csrf_field() }}

                <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
                    <input type="text" name="name" class="form-control" value="{{ old('name') }}"
                           placeholder="{{ trans('adminlte::adminlte.full_name') }}">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input type="email" name="email" class="form-control" value="{{ old('email') }}"
                           placeholder="{{ trans('adminlte::adminlte.email') }}">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <input type="password" name="password" class="form-control"
                           placeholder="{{ trans('adminlte::adminlte.password') }}">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <input type="password" name="password_confirmation" class="form-control"
                           placeholder="{{ trans('adminlte::adminlte.retype_password') }}">
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary btn-block btn-flat">
                    {{ trans('adminlte::adminlte.register') }}
                </button>
            </form>
            <br>
            <p>
                <a href="{{ url(config('adminlte.login_url', 'login')) }}" class="text-center">
                    {{ trans('adminlte::adminlte.i_already_have_a_membership') }}
                </a>
            </p>
        </div>
        <!-- /.form-box -->
    </div><!-- /.register-box --> --}}
    <div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">

				<?php
                $imagesDir = 'frontend/img/loginpage/';
                $images = glob($imagesDir . '*.{jpg,jpeg,png,gif}', GLOB_BRACE);
                $random_img = basename($images[array_rand($images)]); ?>

				<div class="login100-pic js-tilt" data-tilt>
					<img src="frontend/img/loginpage/{{$random_img}}"  alt="IMG">
                </div>

                {{-- <form action="{{ url(config('adminlte.login_url', 'login')) }}" method="post">
                    {{ csrf_field() }} --}}
				<form class="login100-form validate-form" action="{{ url(config('adminlte.register_url', 'register')) }}" method="post">
                    {{ csrf_field() }}

                    <span class="login100-form-title">
						Member Register
					</span>

					<div class="wrap-input100 validate-input {{ $errors->has('name') ? 'has-error' : '' }}" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="text" name="name" placeholder="{{ trans('adminlte::adminlte.full_name') }}" value="{{ old('name') }}">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>

                    </div>
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif

                    <div class="wrap-input100 validate-input {{ $errors->has('email') ? 'has-error' : '' }}" >
						<input class="input100" type="email" name="email" placeholder="{{ trans('adminlte::adminlte.email') }}" value="{{ old('email') }}" autocomplete="off">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>

                    </div>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    <div class="wrap-input100 validate-input {{ $errors->has('mobile_no') ? 'has-error' : '' }}" >
						<input class="input100" type="number" name="mobile_no" placeholder="Enter Mobile Number" value="{{ old('mobile_no') }}" autocomplete="off">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-phone" aria-hidden="true"></i>
                        </span>

                    </div>
                    @if ($errors->has('mobile_no'))
                        <span class="help-block">
                            <strong>{{ $errors->first('mobile_no') }}</strong>
                        </span>
                    @endif

                    <div class="wrap-input100 validate-input {{ $errors->has('password') ? 'has-error' : '' }}" >
						<input class="input100" type="password" name="password" placeholder="{{ trans('adminlte::adminlte.password') }}" >
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
                        </span>

                    </div>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif

                    <div class="wrap-input100 validate-input {{ $errors->has('password_confirmation') ? 'has-error' : '' }}" >
						<input class="input100" type="password" name="password_confirmation" placeholder="{{ trans('adminlte::adminlte.retype_password') }}" >
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
                        </span>

                    </div>
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif




					<div class="container-login100-form-btn">

                        <button type="submit" class="login100-form-btn">
                            {{ trans('adminlte::adminlte.register') }}
                        </button>
					</div>



                @if (config('adminlte.register_url', 'register'))
					<div class="text-center p-t-136">
						<a class="txt2" href="{{ url(config('adminlte.login_url', 'login')) }}">
                            {{ trans('adminlte::adminlte.i_already_have_a_membership') }}
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                        </a>

                    </div>
                    @endif
				</form>
			</div>
		</div>
	</div>
@stop

@section('adminlte_js')
    @yield('js')
@stop
