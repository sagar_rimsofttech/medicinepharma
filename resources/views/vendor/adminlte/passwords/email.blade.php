@extends('adminlte::master')

@section('adminlte_css')
<link rel="stylesheet" href="{{ asset('frontend/css/login/style.css') }}">
    @yield('css')
@stop

@section('body_class', 'login-page')

@section('body')
    {{-- <div class="login-box">
        <div class="login-logo">
            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">{{ trans('adminlte::adminlte.password_reset_message') }}</p>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <form action="{{ url(config('adminlte.password_email_url', 'password/email')) }}" method="post">
                {{ csrf_field() }}

                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input type="email" name="email" class="form-control" value="{{ isset($email) ? $email : old('email') }}"
                           placeholder="{{ trans('adminlte::adminlte.email') }}">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary btn-block btn-flat">
                    {{ trans('adminlte::adminlte.send_password_reset_link') }}
                </button>
            </form>
        </div>

    </div>
 --}}

    <div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">

				<div class="login100-pic js-tilt" data-tilt>
					<img src="https://colorlib.com/etc/lf/Login_v1/images/img-01.png" alt="IMG">
                </div>

                {{-- <form action="{{ url(config('adminlte.login_url', 'login')) }}" method="post">
                    {{ csrf_field() }} --}}
				<form class="login100-form validate-form" action="{{ url(config('adminlte.password_email_url', 'password/email')) }}" method="post">
                    {{ csrf_field() }}

                    <span class="login100-form-title">
						Reset Password
					</span>
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
					<div class="wrap-input100 validate-input {{ $errors->has('email') ? 'has-error' : '' }}">
						<input class="input100" type="text" name="email" placeholder="{{ trans('adminlte::adminlte.email') }}" value="{{ isset($email) ? $email : old('email') }}">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>

                    </div>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif



					<div class="container-login100-form-btn">

                        <button type="submit" class="login100-form-btn">
                            {{ trans('adminlte::adminlte.send_password_reset_link') }}
                        </button>
					</div>
				</form>
			</div>
		</div>
	</div>
@stop

@section('adminlte_js')
    @yield('js')
@stop
