@foreach($categories as $category)
@if(count($category->products)>0)
<?php $rand = 1;
if(count($category->products) <=4){
    $rand = count($category->products);
} else{
    $rand = 4;
}
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <a href="{{route('category.product',$category->id)}}" class="pull-right">
                   <h5 class="text-color"> <u>View All</u> </h5>
            </a>
            <a href="#" class="h3 pull-left">
                    <h3 class="h3 text-color">{{$category->name}}</h3>
            </a>
        </div>
    </div>
    <div class="row">
        @foreach($category->products->random($rand) as $key=>$product)
        @if($key <=3)
        <div class="col-md-3 col-sm-6 getdata" >
            <div class="product-grid4">
                <div class="product-image4">
                    @php $imagearray = explode(',',$product->image); @endphp

                    <a href="{{route('product.details',$product->slug)}}">
                        @foreach($imagearray as $key=>$prodimage)
                        @if($key <=1)
                        <img class="pic-{{$key+1}}" src="{{URL::asset('storage/uploads/product/thumbnails/'.$prodimage)}}">
                        @endif
                        @endforeach
                    </a>
                    {{-- <div class="carousel-item @if($key == 0) active @else '' @endif">
                        <img src="{{URL::asset('storage/uploads/product/thumbnails/'.$prodimage)}}" class="d-block w-100" alt="...">
                    </div> --}}

                    {{-- <ul class="social">
                        <li><a href="#" data-tip="Quick View"><i class="fa fa-eye"></i></a></li>
                        <li><a href="#" data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></a></li>
                        <li><a href="#" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                    </ul> --}}
                    <?php
                        // will check for latest or old product
                        $pastday = \Carbon\Carbon::now()->subDays(2)->format('Y-m-d');
                        $checkdate = $product->created_at->format('Y-m-d');
                        //
                        $dis_per = ceil((($product->price - $product->discounted_price) / $product->price) * 100);
                        ?>
                    @if($checkdate >= $pastday )<span class="product-new-label">New</span>@endif
                    <span class="product-discount-label">-{{$dis_per}}%</span>
                </div>
                <div class="product-content">
                    <h3 class="title"><a href="{{route('product.details',$product->slug)}}">{{$product->name}}</a></h3>
                    <div class="price">
                        Rs {{$product->discounted_price}}
                        <span>Rs {{$product->price}}</span>
                    </div>
                    <input type="hidden" id="product_id" value="{{$product->id}}">
                    <a class="add-to-cart add-cart" data-product_id="{{$product->id}}"href="">ADD TO CART</a>
                </div>
            </div>
        </div>
        @endif
        @endforeach
    </div>
</div>
<hr>
@endif
@endforeach

