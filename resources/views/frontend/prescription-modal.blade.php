<!-- Button trigger modal -->
<style>

.file-upload input[type='file'] {
  display: none;
}

.rounded-lg {
  border-radius: 1rem;
}

.custom-file-label.rounded-pill {
  border-radius: 50rem;
}

.custom-file-label.rounded-pill::after {
  border-radius: 0 50rem 50rem 0;
}
.green {
    outline: 100px solid rgba(11, 235, 3, 0.842) !important;
    outline-offset: -100px;
    position: relative;
}
  </style>

<!-- Modal -->
<div class="modal fade" id="prescriptionupload" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Upload Prescription</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="row">
                <div class="col-lg-6">
              <div class="col-lg-12 mx-auto">
                <div class="p-5 bg-white shadow rounded-lg "><img src="{{asset('frontend/icons/green_upload1.png')}}" alt="" width="200" class="d-block mx-auto mb-4 rounded-pill">

                  <!-- Default bootstrap file upload-->

                  <h6 class="text-center mb-4 text-muted">
                    Note:
                  </h6>
                  <div class="text-left mb-4 text-muted" >
                  <ul>
                      <li>Don’t crop out any part of the image</li>
                      <li>  Avoid blurred image</li>
                      <li>  Include details of doctor and patient + clinic visit date</li>
                      <li> Medicines will be dispensed as per prescription</li>
                      <li> Supported files type: jpeg , jpg , png , pdf</li>
                      <li> Maximum allowed file size: 5MB</li>
                  </ul>
                </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
                <form action="{{ route('store.prescription') }}" method="post" id="prescriptionForm" enctype="multipart/form-data">
                    {{csrf_field()}}
                <div class="col-lg-12 mx-auto">
                    <div class="form-row">
                        @if(!Auth::user())
                        <div class="form-label-group input-group-md mb-3 col-md-12">
                            <input type="text" class="form-control onlynumbers" placeholder="Mobile Number" name="number" id="number">
                            <label for="number">Mobile Number</label>
                        </div>
                        @endif
                        {{-- @if(Auth::user())
                        <p class="lead">
                          Choose your Address:
                        </p>
                        @foreach(Auth::user()->address as $key=>$useraddress)
                        <label>
                            <input type="radio" name="address_id" class="card-input-element d-none" id="address_id" value="{{$useraddress->id??''}} " @if($key == 0) checked @endif>
                            <div class="card card-body bg-light d-flex flex-row justify-content-between align-items-center">
                                  <div class="col-lg-4">
                                  {{$useraddress->name??''}}   -  <span > {{$useraddress->number??''}} </span>
                                  </div>
                              <p>{{$useraddress->address??''}}, {{$useraddress->locality??''}}, {{$useraddress->landmark??''}} , {{$useraddress->city??''}} , {{$useraddress->state??''}} ,{{$useraddress->pincode??''}} , {{$useraddress->alternatenumber?"Alternate Number :" .$useraddress->alternatenumber:''}}</p>
                            </div>
                            </label>
                        @endforeach
                        @else
                        <div class="form-label-group input-group-md mb-3 col-md-6">
                            <input type="text" id="name" class="form-control" placeholder="Name" name="name" autofocus>
                            <label for="name">Name</label>
                            <span class="help-inline">Required To fill</span>
                        </div>
                        <div class="form-label-group input-group-md mb-3 col-md-6">
                            <input type="text" class="form-control" placeholder="Number" name="number" id="number">
                            <label for="number">Number</label>
                        </div>
                        <div class="form-label-group input-group-md mb-3 col-md-6">
                            <input type="number" class="form-control" placeholder="Pincode" name="pincode" id="pincode">
                            <label for="pincode">Pincode</label>
                        </div>
                        <div class="form-label-group input-group-md mb-3 col-md-6">
                            <input type="text" class="form-control" placeholder="Locality" name="locality" id="locality">
                            <label for="locality">Locality</label>
                        </div>
                        <div class="form-label-group input-group-md mb-3 col-md-12">
                            <input type="text" class="form-control" placeholder="Email" name="email" id="email">
                            <label for="email">Email</label>
                        </div>
                        <div class="form-group  input-group-md mb-3 col-md-12">
                            <label for="address">Address</label>
                            <textarea  class="form-control" id="address" name="address"  ></textarea>
                        </div>
                        <div class="form-label-group input-group-md mb-3 col-md-6">
                            <input type="text" class="form-control" placeholder="City/District/Town" name="city" id="city">
                            <label for="city">City/District/Town</label>
                        </div>
                        <div class="mb-3 col-md-6">
                            <select  id="state" name="state" class="selectpicker" data-live-search="true">
                                <option value="58">Utter Pradesh</option>

                            </select>

                        </div>
                        <div class="form-label-group  input-group-md mb-3 col-md-6">
                            <input type="text"  class="form-control" id="landmark" name="landmark" placeholder="landmark(optional)">
                            <label for="landmark">Landmark(optional)</label>
                        </div>
                        <div class="form-label-group input-group-md mb-3 col-md-6">
                            <input type="text" class="form-control" name="alternatenumber" placeholder="Alternate Number" id="alternatenumber">
                            <label for="alternatenumber">Alternate Number</label>
                        </div>
                        <div class="checkout__input__checkbox">
                            <label for="acc">
                                Create an account?
                                <input type="checkbox"  checked readonly name="create_account" id="create_account" value="1">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <p>Create an account by entering the information below. If you are a returning customer
                            please login at the top of the page</p>
                        <div class="form-label-group  input-group-md mb-3 col-md-6">
                            <input type="text"  class="form-control" id="password" name="password" placeholder="Password">
                            <label for="password">Password</label>
                        </div>
                        <div class="form-label-group input-group-md mb-3 col-md-6">
                            <input type="text" class="form-control" name="confirmpassword" placeholder="Confirm Password" id="confirmpassword">
                            <label for="confirmpassword">Confirm Password</label>
                        </div>
                        @endif --}}
                        <div class="checkout__input col-md-12">
                            <p>Order notes<span></span></p>
                            <input type="text"
                                placeholder="Notes about your order, e.g. special notes for delivery." name="delivery_note">
                        </div>
                        <label for="fileUpload" class="file-upload btn btn-success btn-block rounded-pill shadow"><i class="fa fa-upload mr-2"></i>Browse for file ...
                            <input id="fileUpload" type="file" name="prescription">

                        </label>
                        <div class="preview" id="preview"></div>
                        <button class="btn btn-outline-success btn-block rounded-pill shadow" type="submit" data-toggle="modal">

                              <span>UPLOAD</span>
                              <span>PRESCRIPTION <i class="fa fa-upload"></i></span>
                          </button>

                    </div>
                </div>
                </form>
            </div>
            </div>
      </div>
      {{-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> --}}
    </div>
  </div>
</div>

