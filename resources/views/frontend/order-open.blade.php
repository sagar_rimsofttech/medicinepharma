
@extends('layouts.frontend.app')
@section('content')
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>
    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section  tinted-image" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Order Summary</h2>
                        <div class="breadcrumb__option">
                            {{-- <a href="./index.html">Home</a> --}}
                            {{-- <span>Shopping Cart</span> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Shoping Cart Section Begin -->
    <section class="shoping-cart spad">
        @if(!empty($order))
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shoping__cart__table">
                        <p>Payment Failed please try agian!</p>
                        <p>Any money money debited will be refunded back to you in 7 days</p>
                        <p>Contact Us For more details</p>
                    </div>
                </div>
            </div>
            {{-- <div class="row">
                <div class="col-lg-12">
                    <div class="shoping__cart__btns">
                        <a href="#" class="primary-btn cart-btn">CONTINUE SHOPPING</a>
                        <a href="#" class="primary-btn cart-btn cart-btn-right"><span class="icon_loading"></span>
                            Upadate Cart</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="shoping__continue">
                        <div class="shoping__discount">
                            <h5>Discount Codes</h5>
                            <form action="#">
                                <input type="text" placeholder="Enter your coupon code">
                                <button type="submit" class="site-btn">APPLY COUPON</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="shoping__checkout">
                        <h5>Cart Total</h5>
                        <ul>
                            <li>Subtotal <span> Rs. {{Session::has('cart')?Session::get('cart')->totalPrice:'0'}}</span></li>
                            <li>Total <span>Rs. {{Session::has('cart')?Session::get('cart')->totalPrice:'0'}}</span></li>
                        </ul>
                        <a href="{{ route('order.store') }}" class="primary-btn">PROCEED TO CHECKOUT</a>
                    </div>
                </div>
            </div> --}}
        </div>
        @else
        <div class="col-lg-12 text-center">
            <h2>Shopping Cart is Empty</h2>
            <a href="{{route('showIndex')}}" class="primary-btn">Continue Shopping</a>
            </div>
        </div>
        @endif
    </section>
    <!-- Shoping Cart Section End -->

    @endsection

    @push('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $('.checkout_order').click(function() {
            alert(1);

        });
    </script>
    @endpush
