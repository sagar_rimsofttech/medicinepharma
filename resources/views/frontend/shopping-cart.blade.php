
@extends('layouts.frontend.app')
@push('css')
<style>

</style>
@endpush
@section('content')
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>
    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section  tinted-image" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Shopping Cart</h2>
                        <div class="breadcrumb__option">
                            <a href="./index.html">Home</a>
                            <span>Shopping Cart</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Shoping Cart Section Begin -->
    <section class="shoping-cart spad">
        @if(Session::has('cart'))
        <div class="container cartdata">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shoping__cart__table">
                        <table>
                            <thead>
                                <tr>
                                    <th class="shoping__product">Products</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $product)
                                @php $imagearray = explode(',',$product['item']['image']); @endphp
                                <tr>
                                    <td class="shoping__cart__item">
                                        <img src="{{URL::asset('storage/app/public/uploads/product/thumbnails/'.$imagearray[0])}}" width="100" alt="">
                                        <h5 class="text-color"><a class="text-color" href="{{route('product.details',$product['item']['slug'])}}">{{$product['item']['name']}}</a></h5>
                                    </td>
                                    <td class="shoping__cart__price">
                                        Rs {{$product['item']['discounted_price']}}
                                    </td>
                                    <td class="shoping__cart__quantity" data-id="{{$product['item']['id']}}">
                                        <div class="quantity">
                                            <div class="pro-qty">
                                            <input type="text"  value="{{$product['qty']}}">
                                            </div>
                                        </div>
                                    </td>
                                    <td class="shoping__cart__total">
                                        Rs {{$product['price']}}
                                    </td>
                                    <td class="shoping__cart__item__close">
                                        <span class="icon_close" data-product_id="{{$product['item']['id']}}"></span>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                {{-- <div class="col-lg-12">
                    <div class="shoping__cart__btns">
                        <a href="#" class="primary-btn cart-btn">CONTINUE SHOPPING</a>
                        <a href="#" class="primary-btn cart-btn cart-btn-right"><span class="icon_loading"></span>
                            Upadate Cart</a>
                    </div>
                </div> --}}
                <div class="col-lg-6">
                    {{-- <div class="shoping__continue"> --}}
                        {{-- <div class="shoping__cart__btns"> --}}
                            <a href="{{route('showIndex')}}" class="primary-btn">Continue Shopping</a>
                        {{-- </div> --}}
                    {{-- </div> --}}
                </div>
                <div class="col-lg-6">
                    <div class="shoping__checkout mt-1">
                        <h5>Cart Total</h5>
                        <ul>
                            <li class="subtotal-price">Subtotal <span> Rs. {{Session::has('cart')?Session::get('cart')->totalPrice:'0'}}</span></li>
                            <li class="total-price">Total <span>Rs. {{Session::has('cart')?Session::get('cart')->totalPrice:'0'}}</span></li>
                        </ul>
                        <a href="{{ route('shopping.checkout') }}" class="primary-btn">PROCEED TO CHECKOUT</a>
                    </div>
                </div>
            </div>
        </div>

        @endif
        <div class="col-lg-12 text-center cartempty" style="display: @if(Session::has('cart')) none @else block @endif">
            <h2>Shopping Cart is Empty</h2>
            <a href="{{route('showIndex')}}" class="primary-btn">Continue Shopping</a>
            </div>
        </div>
    </section>
    <!-- Shoping Cart Section End -->

    @endsection

    @push('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $('.icon_close').click(function() {
            var product_id = $(this).data('product_id');
            var this_click = $(this);
            var quantity = 1;
            $.ajax({
                type:'GET',
                url:'/remove-product-from-cart/'+product_id,
                // data:{quantity:quantity, product_id:product_id},
                success:function(data){
                    console.log(data);
                    console.log(data.quntity);
                    $('.show-cart-total').html(data.quntity);
                    $('.show-cart-item-total').html(data.itemtotal);
                    $('.subtotal-price').find('span').html("Rs." +data.itemtotal);
                    $('.total-price').find('span').html("Rs." +data.itemtotal);
                    if(data.itemtotal == 0)
                    {
                        $('.cartdata').hide();
                        $('.cartempty').show();
                    }  else{
                        this_click.parents('tr').remove();
                    }
                    toastr.success('Product is removed from cart!',{closeButton: false,positionClass:'toast-top-right'});
                }
                });
        });
    </script>
    @endpush
