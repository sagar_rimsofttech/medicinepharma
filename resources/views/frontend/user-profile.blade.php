
@extends('layouts.frontend.app')
@push('css')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<style>
.ledger-list-view .ledger-list-item, .ledger-patient-details, .ledger-doctor-details {
    padding: 1rem;
    background-color: #fffff;
    margin-bottom: 1.5rem;
    border: 1px solid #fffff;
    border-radius: 6px;
    box-shadow: 0px 3px 9px 0 rgba(0, 0, 0, 0.2), 0 6px 37px 0 rgba(0, 0, 0, 0.19);
}


.card {
    z-index: 0;
    background-color: #ECEFF1;
    padding-bottom: 20px;
    margin-top: 90px;
    margin-bottom: 90px;
    border-radius: 10px
}

.top {
    padding-top: 40px;
    padding-left: 13% !important;
    padding-right: 13% !important
}

#progressbar {
    margin-bottom: 0px;
    overflow: hidden;
    color: #455A64;
    padding-left: 0px;
    margin-top: 0px
}

#progressbar li {
    list-style-type: none;
    font-size: 13px;
    width: 25%;
    float: left;
    position: relative;
    font-weight: 400
}

#progressbar .step0:before {
    font-family: FontAwesome;
    content: "\f10c";
    color: #fff
}

#progressbar li:before {
    width: 26px;
    height: 24px;
    line-height: 23px;
    display: block;
    font-size: 10px;
    background: #C5CAE9;
    border-radius: 50%;
    margin: auto;
    padding: 0px
}

#progressbar li:after {
    content: '';
    width: 100%;
    height: 4px;
    background: #C5CAE9;
    position: absolute;
    left: 0;
    top: 12px;
    z-index: -1
}

#progressbar li:last-child:after {
    border-top-right-radius: 10px;
    border-bottom-right-radius: 10px;
    position: absolute;
    left: -50%
}

#progressbar li:nth-child(2):after,
#progressbar li:nth-child(3):after {
    left: -50%
}

#progressbar li:first-child:after {
    border-top-left-radius: 10px;
    border-bottom-left-radius: 10px;
    position: absolute;
    left: 50%
}

#progressbar li:last-child:after {
    border-top-right-radius: 10px;
    border-bottom-right-radius: 10px
}

#progressbar li:first-child:after {
    border-top-left-radius: 10px;
    border-bottom-left-radius: 10px
}

#progressbar li.active:before,
#progressbar li.active:after {
    background: #7fad39;
}

#progressbar li.active:before {
    font-family: FontAwesome;
    content: "\f00c"
}

.icon {
    width: 26px;
    height: 26px;
    margin-right:3px;
}

.icon-content {
    padding-bottom: 10px
}

@media screen and (max-width: 992px) {
    .icon-content {
        width: 50%
    }
}
/* .right {
    border-right: 1px solid #ccc;
} */
.modal-confirm {
	color: #636363;
	width: 400px;
}
.modal-confirm .modal-content {
	padding: 20px;
	border-radius: 5px;
	border: none;
	text-align: center;
	font-size: 14px;
}
.modal-confirm .modal-header {
	border-bottom: none;
	position: relative;
}
.modal-confirm h4 {
	text-align: center;
	font-size: 26px;
	margin: 30px 0 -10px;
}
.modal-confirm .close {
	position: absolute;
	top: -5px;
	right: -2px;
}
.modal-confirm .modal-body {
	color: #999;
}
.modal-confirm .modal-footer {
	border: none;
	text-align: center;
	border-radius: 5px;
	font-size: 13px;
	padding: 10px 15px 25px;
}
.modal-confirm .modal-footer a {
	color: #999;
}
.modal-confirm .icon-box {
	width: 80px;
	height: 80px;
	margin: 0 auto;
	border-radius: 50%;
	z-index: 9;
	text-align: center;
	border: 3px solid #7fad39;
}
.modal-confirm .icon-box i {
	color: #7fad39;
	font-size: 46px;
	display: inline-block;
	margin-top: 13px;
}
.modal-confirm .btn, .modal-confirm .btn:active {
	color: #fff;
	border-radius: 4px;
	background: #60c7c1;
	text-decoration: none;
	transition: all 0.4s;
	line-height: normal;
	min-width: 120px;
	border: none;
	min-height: 40px;
	border-radius: 3px;
	margin: 0 5px;
}
.modal-confirm .btn-secondary {
	background: #c1c1c1;
}
.modal-confirm .btn-secondary:hover, .modal-confirm .btn-secondary:focus {
	background: #a8a8a8;
}
.modal-confirm .btn-danger {
	background: #f15e5e;
}
.modal-confirm .btn-danger:hover, .modal-confirm .btn-danger:focus {
	background: #ee3535;
}
.trigger-btn {
	display: inline-block;
	margin: 100px auto;
}
</style>
@endpush
@section('content')

    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>
    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section  tinted-image" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>My Account</h2>
                        <div class="breadcrumb__option">
                            <a href="./index.html">Home</a>
                            <span>My Account</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->
@php date_default_timezone_set('UTC'); @endphp
    <!-- Shoping Cart Section Begin -->
    <section class="shoping-cart spad">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shoping__cart__table">
                        <div class="row">
                            <div class="col-3">
                              <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link @if(Request::has('tab') && Request::get('tab') == 'myaccount') active @endif" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">My Account</a>
                                <a class="nav-link @if(Request::has('tab') && Request::get('tab') == 'myorder') active @endif" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Orders</a>
                                <a class="nav-link @if(Request::has('tab') && Request::get('tab') == 'changepassword') active @endif" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Change Password</a>
                                {{-- <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Settings</a> --}}
                              </div>
                            </div>

                            <div class="col-8">
                              <div class="tab-content ml-12" id="v-pills-tabContent">
                                <div class="tab-pane fade show @if(Request::has('tab') && Request::get('tab') == 'myaccount') active @endif" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab"><p class="font-weight-bold">My Address</p>
                                    <a href="#" class="text-success text-center" data-toggle="modal" data-target="#addressmodel">+ Add A New Address</a><br>
                                    <div class="d-md-flex">

                                      <div class="overflow-auto p-3 mb-3 mb-md-0 mr-md-3 bg-light" style="max-width: 100%; max-height: 200px;">

                                        <div class="displaycard">
                                        @if(count($useraddresses)>0)
                                        @foreach($useraddresses as $useraddress)
                                        <div class="card mt-3">
                                            <div class="card-body">
                                            {{$useraddress->name??''}}   -  <span clas="mr-4"> {{$useraddress->number??''}} </span>
                                            <p>{{$useraddress->address??''}}, {{$useraddress->locality??''}}, {{$useraddress->landmark??''}} , {{$useraddress->city??''}} , {{$useraddress->state??''}} ,{{$useraddress->pincode??''}} , {{$useraddress->alternatenumber?"Alternate Number :" .$useraddress->alternatenumber:''}}</p>
                                            </div>
                                        </div>
                                         @endforeach
                                         @else
                                         <p>No Address Added</p>
                                         @endif
                                    </div>
                                      </div>
                                    </div>

                                    <hr></div>
                                <div class="tab-pane show @if(Request::has('tab') && Request::get('tab') == 'myorder') active @endif" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                    <p class="font-weight-bold">My Orders</p>
                                    @foreach($orders as $order)
                                    <div class="box">
                                        <div class="box-body">
                                            <div class="ledger-list-item ledger-patient-details">
                                                <div class="row">
                                                    <div class="col-md-12  right ">
                                                        <div class="row">
                                                            @foreach($order->cart->items as $item)
                                                            <div class="col-md-8 ">
                                                                <div>
                                                                    {{-- {{dd($item)}} --}}
                                                                <span class="text-grey">{{$item['item']['name'] }} </span>

                                                                </div>
                                                                <small class="">Price: Rs {{$item['item']['price']}}</small>  |
                                                                <small class="ml-1">Qyantity: {{$item['qty']}}</small>
                                                                <hr>

                                                            </div>
                                                            <div class="col-md-4">
                                                                <div>
                                                                <span class="align-right">Rs: {{$item['price']}}</span>
                                                                </div>
                                                                <small class="">Order Date:{{$order->order_date?date("D, M, j, Y | h:i A",strtotime($order->order_date)):''}} </small>
                                                                <hr>

                                                            </div>
                                                            @endforeach
                                                        </div>
                                                    </div>

                                                    {{-- <div class="col-md-6  right ">
                                                        <div class="row text-center">
                                                            <div class="col-md-12 ">
                                                                <div>
                                                                    <span class="text-grey">Client Name : </span>
                                                                    <label class="text-default">New Era Pvt Ltd</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                            <a href="http://127.0.0.1:8002/admin/client" class="text-primary"><i class="fas fa-eye"></i></a>  |
                                                                <a href="http://127.0.0.1:8002/admin/client/edit/1" class="text-primary"><i class="fas fa-edit"></i></a>
                                                            </div>
                                                        </div>
                                                    </div> --}}
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        Total Price : Rs{{$order->cart->totalPrice}}

                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="row d-flex justify-content-between px-3">
                                                            <div class="d-flex">
                                                                <h5>ORDER : <span class="text-success">{{$order->order_code}}</span></h5>
                                                            </div>
                                                            <div class="d-flex flex-column text-sm-right">
                                                                <p class="mb-0">Expected Arrival : <span>{{$order->order_date?date("l jS Y",strtotime($order->order_date)):''}}</span></p>
                                                                {{-- <p>USPS <span class="font-weight-bold">234094567242423422898</span></p> --}}
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="col-12">
                                                                <ul id="progressbar" class="text-center">
                                                                    <li class="@if($order->order_status >= 1 ) active @endif step0"></li>
                                                                    <li class="@if($order->order_status >= 2 ) active @endif step0"></li>
                                                                    <li class="@if($order->order_status >= 3) active @endif step0"></li>
                                                                    <li class="@if($order->order_status ==4) active @endif step0"></li>
                                                                </ul>
                                                            </div>
                                                            <div class="row justify-content-between ">
                                                                <div class="d-flex ml-4"> <img class="icon" src="{{asset('frontend/icons/orderplaced.png')}}">
                                                                        <p class="showicontext">Processed</p>
                                                                </div>
                                                                <div class="d-flex "> <img class="icon" src="{{asset('frontend/icons/order_shipped.png')}}">
                                                                    <p class="showicontext">Shipped</p>
                                                                </div>
                                                                <div class="d-flex"> <img class="icon" src="{{asset('frontend/icons/order_onroute.png')}}">
                                                                    <p class="showicontext">En Route</p>
                                                                </div>
                                                                <div class="d-flex mr-3"> <img class="icon" src="{{asset('frontend/icons/order_delivered.png')}}">
                                                                    <p class="showicontext">Delivered</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    {{ $orders->appends(\Illuminate\Support\Facades\Input::all())->links('vendor.pagination.Medicinepharma') }}
                                </div>
                                <div class="tab-pane show @if(Request::has('tab') && Request::get('tab') == 'changepassword') active @endif"  id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                <div class="d-md-flex">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <form id="updatepassword" action="{{route('update.password')}}" method="POST">
                                                {{csrf_field()}}
                                                <div class="form-group row">
                                                  <label for="colFormLabel" class="col-sm-6 col-form-label">Current Password</label>
                                                  <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="old_password" id="old_password" placeholder="Current Password">
                                                  </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabel" class="col-sm-4 col-form-label">New Password</label>
                                                    <div class="col-sm-10">
                                                      <input type="text" class="form-control" name="password" id="password" placeholder="New Password">
                                                    </div>
                                                  </div>
                                                  <div class="form-group row">
                                                    <label for="colFormLabel" class="col-sm-6 col-form-label">Confirm New Password</label>
                                                    <div class="col-sm-10">
                                                      <input type="text" class="form-control" name="confirm_password" id="confirm_password" placeholder="Confirm New Password">
                                                    </div>
                                                  </div>
                                                  <button type="submit" class="btn btn-outline-success">Update Password</button>

                                              </form>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">Settings</div> --}}
                              </div>
                            </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>

        @include('frontend.address_model')


        <!-- Modal HTML -->
        <div id="updatepasswordmodel" class="modal fade">
            <div class="modal-dialog modal-confirm">
                <div class="modal-content">
                    <div class="modal-header flex-column">
                        <div class="icon-box">
                            <i class="material-icons ">done</i>
                        </div>
                        <h4 class="modal-title w-100">Password Updated Successfully</h4>
                    </div>
                    <div class="modal-body">
                        <p>Please Login to Continue!</p>
                    </div>
                    <div class="modal-footer justify-content-center">
                        <a href="{{route('showIndex')}}" class="btn bg-site-color"><span class="text-white">Login</span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shoping Cart Section End -->

    @endsection

    @push('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script>

         $("#addressSaveForm").submit(function(e){
                e.preventDefault();
                $.validator.addMethod("alphabetsnspace", function(value, element) {
                    return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
                },"Please input Alphabet only!");
                $('#addressSaveForm').validate({
            errorElement: 'span',
            errorClass: 'help-inline text-danger',
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    alphabetsnspace: true
                },
                number: {
                    required: true,
                    digits: true,
                    minlength:10,
                    maxlength:10
                },
                pincode: {
                    required: true,
                    digits: true,
                    minlength: 6,
                    maxlength:6

                },
                locality: {
                    required: true,
                    maxlength: 35,
                    // lettersonly: true

                },

                address: {
                    required: true,

                },
                city: {
                    required: true,
                    alphabetsnspace: true
                },
                state: {
                    required: true,
                },
                landmark: {
                    required: true,

                },
                alternatenumber: {
                    required: false,
                    digits:true,
                    minlength:10,
                    maxlength:10
                },
            },
        });
                var name = $("#addressname").val();
                var number = $("input[name=number]").val();
                var pincode = $("input[name=pincode]").val();
                var locality = $("input[name=locality]").val();
                var address = $("#addressaddress").val();
                var city = $("input[name=city]").val();
                var state = $("#addressstate").val();
                var landmark = $("input[name=landmark]").val();
                var alternatenumber = $("input[name=alternatenumber]").val();
                $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }

                });
                $.ajax({
                    type:'POST',
                    url:"/store/address",
                    data:{name:name, number:number, pincode:pincode, locality:locality, address:address, city:city, state:state, landmark:landmark, alternatenumber:alternatenumber},
                    dataType:"json",
                    success: function (data) {
                        toastr.success('Address stored Sucessfully!',
                            {
                                closeButton: false,
                                allowHtml: true,
                                positionClass:'toast-top-right',
                            });
                        $('.displaycard').prepend("<div class='card mt-3'><div class='card-body'>"+data.data.name+" - <span class='ml-4'> "+data.data.number+" </span> <p>"+data.data.address+" ,"+data.data.locality+","+data.data.landmark+", "+data.data.city+","+data.data.state+","+data.data.pincode+", Alternate number: "+data.data.alternatenumber+"</p></div></div>");
                        $('#addressmodel').modal('hide');

                    }
                });
            });

            $("#updatepassword").submit(function(e){
                e.preventDefault();
                var old_password = $("#old_password").val();
                var password = $("#password").val();
                var confirm_password = $("input[name=confirm_password]").val();
                $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }

                });
                $.ajax({
                    type:'POST',
                    url:"/update-password",
                    data:{old_password:old_password, password:password, confirm_password:confirm_password},
                    dataType:"json",
                    success: function (data) {
                        console.log(data.message);
                        $("#updatepasswordmodel").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $("#updatepasswordmodel").modal('show');
                        toastr.success(data.message,
                            {
                                closeButton: false,
                                allowHtml: true,
                                positionClass:'toast-top-right',
                            });

                    },error: function (jqXHR, exception) {
                        console.log(jqXHR.status );
                        console.log(jqXHR.responseJSON.message );
                        if(jqXHR.responseJSON.errors){
                        $.each(jqXHR.responseJSON.errors, function(key, val) {
                            toastr.error(''+val+'','Error',{
                            closeButton:true,
                            progressBar:true,
                            });
                        });
                        return false;
                    } else if (jqXHR.status == 400){
                        toastr.error(jqXHR.responseJSON.message,
                            {
                                closeButton: false,
                                allowHtml: true,
                                positionClass:'toast-top-right',
                            });
                }
                    }
                });
            });

    </script>

    <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
    @endpush
