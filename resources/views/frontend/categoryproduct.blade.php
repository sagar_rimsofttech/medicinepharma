@extends('layouts.frontend.app')
@section('content')

    <!-- Breadcrumb Section Begin -->
    <?php $category = \App\Traits\CategoryTrait::getCategory($category_id); ?>
    <section class="breadcrumb-section  tinted-image" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                    <h2> {{!empty($category)?$category->name:'Category'}}</h2>
                        <div class="breadcrumb__option">
                            <a href="./index.html">Home</a>
                            <span>Shop</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Product Section Begin -->
    <section class="product spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-5">
                    <div class="sidebar custom_filter">
                        <div class="sidebar__item">
                            <h4>Categories</h4>
                            <ul>
                                @foreach($categories as $category)
                                <li><a href="{{route('category.product',$category->id)}}">{{$category->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <form class="formsubmit" action="{{route('category.product',$category_id)}}" method="GET">
                            {!! csrf_field() !!}
                        <div class="sidebar__item">
                            <h4>Price</h4>
                            <div class="price-range-wrap">
                                <div class="price-range ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"
                                    data-min="10" data-max="10000">
                                    <div class="ui-slider-range ui-corner-all ui-widget-header"></div>
                                    <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                                    <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                                </div>

                                <div class="range-slider">
                                    <div class="price-input">
                                  Rs :  <input type="text" id="minamount" name="minprice" value="{{$minprice}}">
                                  Rs :  <input type="text" id="maxamount" name="maxprice" value="{{$maxprice}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="sidebar__item sidebar__item__color--option">
                            <h4>Colors</h4>
                            <div class="sidebar__item__color sidebar__item__color--white">
                                <label for="white">
                                    White
                                    <input type="radio" id="white">
                                </label>
                            </div>
                            <div class="sidebar__item__color sidebar__item__color--gray">
                                <label for="gray">
                                    Gray
                                    <input type="radio" id="gray">
                                </label>
                            </div>
                            <div class="sidebar__item__color sidebar__item__color--red">
                                <label for="red">
                                    Red
                                    <input type="radio" id="red">
                                </label>
                            </div>
                            <div class="sidebar__item__color sidebar__item__color--black">
                                <label for="black">
                                    Black
                                    <input type="radio" id="black">
                                </label>
                            </div>
                            <div class="sidebar__item__color sidebar__item__color--blue">
                                <label for="blue">
                                    Blue
                                    <input type="radio" id="blue">
                                </label>
                            </div>
                            <div class="sidebar__item__color sidebar__item__color--green">
                                <label for="green">
                                    Green
                                    <input type="radio" id="green">
                                </label>
                            </div>
                        </div> --}}
                        <div class="sidebar__item">
                            <h4>Popular</h4>
                            <div class="sidebar__item__size">
                                <label for="large">
                                    Popular Selling
                                    <input type="radio" class="popular" id="large" name="popular" value="0">
                                </label>
                            </div>
                            <div class="sidebar__item__size">
                                <label for="medium">
                                    Daily Need
                                    <input type="radio" class="popular" id="medium" name="popular" value="1">
                                </label>
                            </div>
                            <div class="sidebar__item__size">
                                <label for="small">
                                    Special Need
                                    <input type="radio" class="popular" id="small" name="popular" value="2">
                                </label>
                            </div>
                            {{-- <div class="sidebar__item__size">
                                <label for="tiny">
                                    Tiny
                                    <input type="radio" id="tiny">
                                </label>
                            </div> --}}
                        </div>

                        <div class="sidebar__item">
                            <div class="latest-product__text">
                                <h4>Latest Products</h4>
                                <div class="latest-product__slider owl-carousel">
                                    <div class="latest-prdouct__slider__item">
                                        {{-- <h6 class="text-center mb-2">Medicine</h6> --}}
                                        @foreach ($latestproducts as $key=>$product)
                                        @if($key <= 2)
                                        <a href="{{route('product.details',$product->slug)}}" class="latest-product__item">
                                            <div class="latest-product__item__pic">
                                                @php $imagearray = explode(',',$product->image); @endphp
                                                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                                    <ol class="carousel-indicators">
                                                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                                        <li data-target="#myCarousel" data-slide-to="1"></li>
                                                        <li data-target="#myCarousel" data-slide-to="2"></li>
                                                      </ol>
                                                    <div class="carousel-inner">
                                                        @foreach($imagearray as $key=>$prodimage)
                                                        <div class="carousel-item @if($key == 0) active @else '' @endif">
                                                            <img src="{{URL::asset('storage/uploads/product/thumbnails/'.$prodimage)}}" alt="">
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <div class="latest-product__item__text text-center">
                                                    <h6>{{$product->name}}</h6>
                                                        <span>Rs : {{$product->price}}</span>
                                                    </div>
                                            </div>
                                            <div class="latest-product__item__text">
                                            <h6>{{$product->name}}</h6>
                                                <span>Rs : {{$product->price}}</span>
                                            </div>
                                        </a>
                                        @endif
                                        @endforeach
                                    </div>
                                    <div class="latest-prdouct__slider__item">
                                        {{-- <h6 class="text-center mb-2">Medicine</h6> --}}
                                        @foreach ($latestproducts as $key=>$product)
                                        @if($key >= 3)
                                        <a href="#" class="latest-product__item">
                                            <div class="latest-product__item__pic">
                                                @php $imagearray = explode(',',$product->image); @endphp
                                                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                                    <ol class="carousel-indicators">
                                                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                                        <li data-target="#myCarousel" data-slide-to="1"></li>
                                                        <li data-target="#myCarousel" data-slide-to="2"></li>
                                                      </ol>
                                                    <div class="carousel-inner">
                                                        @foreach($imagearray as $key=>$prodimage)
                                                        <div class="carousel-item @if($key == 0) active @else '' @endif">
                                                            <img src="{{URL::asset('storage/uploads/product/thumbnails/'.$prodimage)}}" alt="">
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <div class="latest-product__item__text text-center">
                                                    <h6>{{$product->name}}</h6>
                                                        <span>Rs : {{$product->price}}</span>
                                                    </div>
                                            </div>
                                            <div class="latest-product__item__text">
                                            <h6>{{$product->name}}</h6>
                                                <span>Rs : {{$product->price}}</span>
                                            </div>
                                        </a>
                                        @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-7">
                    {{-- <div class="product__discount">
                        <div class="section-title product__discount__title">
                            <h2>Sale Off</h2>
                        </div>
                        <div class="row">
                            <div class="product__discount__slider owl-carousel">
                                <div class="col-lg-4">
                                    <div class="product__discount__item">
                                        <div class="product__discount__item__pic set-bg"
                                            data-setbg="img/product/discount/pd-1.jpg">
                                            <div class="product__discount__percent">-20%</div>
                                            <ul class="product__item__pic__hover">
                                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                                <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="product__discount__item__text">
                                            <span>Dried Fruit</span>
                                            <h5><a href="#">Raisin’n’nuts</a></h5>
                                            <div class="product__item__price">$30.00 <span>$36.00</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="product__discount__item">
                                        <div class="product__discount__item__pic set-bg"
                                            data-setbg="{{asset('frontend/img/product/discount/pd-2.jpg')}}">
                                            <div class="product__discount__percent">-20%</div>
                                            <ul class="product__item__pic__hover">
                                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                                <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="product__discount__item__text">
                                            <span>Vegetables</span>
                                            <h5><a href="#">Vegetables’package</a></h5>
                                            <div class="product__item__price">$30.00 <span>$36.00</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="product__discount__item">
                                        <div class="product__discount__item__pic set-bg"
                                            data-setbg="img/product/discount/pd-3.jpg">
                                            <div class="product__discount__percent">-20%</div>
                                            <ul class="product__item__pic__hover">
                                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                                <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="product__discount__item__text">
                                            <span>Dried Fruit</span>
                                            <h5><a href="#">Mixed Fruitss</a></h5>
                                            <div class="product__item__price">$30.00 <span>$36.00</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="product__discount__item">
                                        <div class="product__discount__item__pic set-bg"
                                            data-setbg="img/product/discount/pd-4.jpg">
                                            <div class="product__discount__percent">-20%</div>
                                            <ul class="product__item__pic__hover">
                                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                                <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="product__discount__item__text">
                                            <span>Dried Fruit</span>
                                            <h5><a href="#">Raisin’n’nuts</a></h5>
                                            <div class="product__item__price">$30.00 <span>$36.00</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="product__discount__item">
                                        <div class="product__discount__item__pic set-bg"
                                            data-setbg="img/product/discount/pd-5.jpg">
                                            <div class="product__discount__percent">-20%</div>
                                            <ul class="product__item__pic__hover">
                                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                                <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="product__discount__item__text">
                                            <span>Dried Fruit</span>
                                            <h5><a href="#">Raisin’n’nuts</a></h5>
                                            <div class="product__item__price">$30.00 <span>$36.00</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="product__discount__item">
                                        <div class="product__discount__item__pic set-bg"
                                            data-setbg="img/product/discount/pd-6.jpg">
                                            <div class="product__discount__percent">-20%</div>
                                            <ul class="product__item__pic__hover">
                                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                                <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="product__discount__item__text">
                                            <span>Dried Fruit</span>
                                            <h5><a href="#">Raisin’n’nuts</a></h5>
                                            <div class="product__item__price">$30.00 <span>$36.00</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <div class="filter__item">

                        <div class="row">
                            <div class="col-lg-4 col-md-5">
                                <div class="filter__sort">
                                    <span>Sort By</span>
                                    <select name="sortby" id="sortby">
                                        <option value="0">Newest Top</option>
                                        <option value="1" @if($sortBy == 1) selected @endif>Low Price on Top</option>
                                        <option value="2" @if($sortBy == 2) selected @endif>High Price on Top</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="filter__found">
                                    <h6><span>{{count($products)}}</span> Products found</h6>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-3">
                                <div class="filter__option">
                                    <span class="icon_grid-2x2"></span>
                                    <span class="icon_ul"></span>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </form>

                    <div class="row">
                        @foreach($products as $product)

                        <div class="col-md-3 col-sm-6 getdata" >
                            <div class="product-grid4">
                                <div class="product-image4">
                                    @php $imagearray = explode(',',$product->image); @endphp

                                    <a href="{{route('product.details',$product->slug)}}">
                                        @foreach($imagearray as $key=>$prodimage)
                                        @if($key <=1)
                                        <img class="pic-{{$key+1}}" src="{{URL::asset('storage/uploads/product/thumbnails/'.$prodimage)}}">
                                        @endif
                                        @endforeach
                                    </a>
                                    {{-- <div class="carousel-item @if($key == 0) active @else '' @endif">
                                        <img src="{{URL::asset('storage/uploads/product/thumbnails/'.$prodimage)}}" class="d-block w-100" alt="...">
                                    </div> --}}

                                    {{-- <ul class="social">
                                        <li><a href="#" data-tip="Quick View"><i class="fa fa-eye"></i></a></li>
                                        <li><a href="#" data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></a></li>
                                        <li><a href="#" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                                    </ul> --}}
                                    <?php
                                        // will check for latest or old product
                                        $pastday = \Carbon\Carbon::now()->subDays(2)->format('Y-m-d');
                                        $checkdate = $product->created_at->format('Y-m-d');
                                        //
                                        $dis_per = ceil((($product->price - $product->discounted_price) / $product->price) * 100);
                                        ?>
                                    @if($checkdate >= $pastday )<span class="product-new-label">New</span>@endif
                                    <span class="product-discount-label">-{{$dis_per}}%</span>
                                </div>
                                <div class="product-content">
                                    <h3 class="title"><a href="{{route('product.details',$product->slug)}}">{{$product->name}}</a></h3>
                                    <div class="price">Hans
                                        Rs {{$product->discounted_price}}
                                        <span>Rs {{$product->price}}</span>
                                    </div>
                                    <input type="hidden" id="product_id" value="{{$product->id}}">
                                    <a class="add-to-cart add-cart" data-product_id="{{$product->id}}"href="">ADD TO CART</a>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                @php $imagearray = explode(',',$product->image); @endphp
                                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                        <li data-target="#myCarousel" data-slide-to="1"></li>
                                        <li data-target="#myCarousel" data-slide-to="2"></li>
                                      </ol>
                                    <div class="carousel-inner">
                                        @foreach($imagearray as $key=>$prodimage)
                                        <div class="carousel-item @if($key == 0) active @else '' @endif">
                                            <div class="product__item__pic set-bg" data-setbg="{{URL::asset('storage/uploads/product/thumbnails/'.$prodimage)}}">
                                                <ul class="product__item__pic__hover">
                                                    <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                                    <li><a href="{{route('product.details',$product->slug)}}"><i class="fa fa-shopping-cart"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="product__item__text">
                                    <h6><a href="{{route('product.details',$product->slug)}}" class="text-capitalize">{{$product->name}}</a></h6>
                                    <h5><span style="color:#7fad39";>Rs </span>{{$product->price}}</h5>
                                </div>
                            </div>
                        </div> --}}
                        @endforeach
                    </div>
                    {{ $products->render("pagination::bootstrap-4") }}
                    {{-- <div class="product__pagination">
                        {{ $products->render("pagination::bootstrap-4") }}
                        <a href="#">1</a>
                        <a href="#">2</a>
                        <a href="#">3</a>
                        <a href="#"><i class="fa fa-long-arrow-right"></i></a>
                    </div> --}}
                </div>
            </div>
        </div>

    </section>
    <!-- Product Section End -->

    @endsection

    @push('js')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
       $(document).ready(function() {
        $('#sortby').change(function() {
            this.form.submit();
        });
        $(".add-cart").click(function(e){
        e.preventDefault();
        var parenttr = $(this).parents('.getdata');
        var product_id =  $(parenttr).find("a.add-cart").data('product_id');
        var quantity = 1;
        // var product_id = $('#product_id').val();
        // var password = $("input[name=password]").val();
            $.ajax({

            type:'GET',
            url:'/add-to-cart/'+quantity+'/'+product_id,
            // data:{quantity:quantity, product_id:product_id},
            success:function(data){
                $('.show-cart-total').html(data.quntity);
                $('.show-cart-item-total').html( data.itemtotal);
                toastr.success('Product is added to cart!',{closeButton: false,positionClass:'toast-top-right'});
            }
            });
        });
        $('.popular').click(function() {
            this.form.submit();
        });
    // $(".price-range" ).slider({
    //     // options
    //     start: function (event, ui) {
    //         // code
    //     },
    //     slide: function( event, ui ) {
    //         // code
    //     },

    //     change: function(event, ui) {
    //         console.log("1");
    //         $("#priceform").submit();
    //     }
    // });
    var rangeSlider = $(".price-range"),
        minamount = $("#minamount"),
        maxamount = $("#maxamount"),
        minPrice = rangeSlider.data('min'),
        maxPrice = rangeSlider.data('max');
    rangeSlider.slider({
        range: true,
        min: minPrice,
        max: maxPrice,
        values: [minPrice, maxPrice],
        slide: function (event, ui) {
            minamount.val(ui.values[0]);
            maxamount.val(ui.values[1]);
        },
        change: function(event, ui) {
            console.log("1");
            setTimeout(function(){
                $(".formsubmit").submit();
            }, 2000);
            // $(".formsubmit").submit();
        }

    });
    var minprice = '{{$minprice}}';
    var maxprice = '{{$maxprice}}';
    minamount.val(minprice);
    maxamount.val(maxprice);
    });
    </script>
    @endpush
