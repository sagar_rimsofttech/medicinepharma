
@extends('layouts.frontend.app')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@push('css')
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<style>
label {
  width: 100%;
  font-size: 1rem;
}

.card-input-element+.card {
  height: calc(36px + 2*2rem);
  color: var(--success);
  -webkit-box-shadow: none;
  box-shadow: none;
  border: 2px solid transparent;
  border-radius: 4px;
}

.card-input-element+.card:hover {
  cursor: pointer;
}

.card-input-element:checked+.card {
  border: 2px solid var(--success);
  -webkit-transition: border .3s;
  -o-transition: border .3s;
  transition: border .3s;
}

.card-input-element:checked+.card::after {
  content: '\e5ca';
  color: #28a745;
  font-family: 'Material Icons';
  font-size: 24px;
  -webkit-animation-name: fadeInCheckbox;
  animation-name: fadeInCheckbox;
  -webkit-animation-duration: .5s;
  animation-duration: .5s;
  -webkit-animation-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
  animation-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
}

@-webkit-keyframes fadeInCheckbox {
  from {
    opacity: 0;
    -webkit-transform: rotateZ(-20deg);
  }
  to {
    opacity: 1;
    -webkit-transform: rotateZ(0deg);
  }
}

@keyframes fadeInCheckbox {
  from {
    opacity: 0;
    transform: rotateZ(-20deg);
  }
  to {
    opacity: 1;
    transform: rotateZ(0deg);
  }
}

</style>
@endpush
@section('content')
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>
    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section  tinted-image" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>CheckOut</h2>
                        <div class="breadcrumb__option">
                            <a href="./index.html">Home</a>
                            <span>CheckOut</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

     <!-- Checkout Section Begin -->
     <section class="checkout spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h6><span class="icon_tag_alt"></span> Have a coupon? <a href="#">Click here</a> to enter your code
                    </h6>
                </div>
            </div>


            <div class="checkout__form">
                <h4>Billing Details</h4>
                <form action="{{ route('order.store') }}" method="post" id="checkoutForm">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-lg-8 col-md-6">
                            @if(Auth::user())
                                <p class="lead">
                                  Choose your Address:
                                </p>
                                @if(count(Auth::user()->address))
                                @foreach(Auth::user()->address as $useraddress)
                                <label class="appendaddressdata">
                                  <input type="radio" name="address_id" class="card-input-element d-none" id="demo1" value="{{$useraddress->id??''}} ">
                                  <div class="card card-body bg-light d-flex flex-row justify-content-between align-items-center">
                                        <div class="col-lg-4">
                                        {{$useraddress->name??''}}   -  <span > {{$useraddress->number??''}} </span>
                                        </div>
                                    <p>{{$useraddress->address??''}}, {{$useraddress->locality??''}}, {{$useraddress->landmark??''}} , {{$useraddress->city??''}} , {{$useraddress->state??''}} ,{{$useraddress->pincode??''}} , {{$useraddress->alternatenumber?"Alternate Number :" .$useraddress->alternatenumber:''}}</p>
                                  </div>
                                  </label>
                                  @endforeach
                                  @else
                                  <label class="appendaddressdata">
                                    </label>

                                  <a href="#" class="text-success text-center adddrresss" data-toggle="modal" data-target="#addressmodel">+ Add A New Address</a><br>

                                  @endif
                            @else
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Full Name<span>*</span></p>
                                        <input type="text" name="name">
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="checkout__input">
                                <p>Country<span>*</span></p>
                                <input type="text">
                            </div> --}}
                            <div class="checkout__input">
                                <p>Address<span>*</span></p>
                                <input type="text" placeholder="Street Address" class="checkout__input__add" name="address">
                                <input type="text" class="checkout__input__add"  placeholder="Locality (optinal)" name="locality">
                                <input type="text"   placeholder="Landmark like Near Apartment, unite ect (optinal)" name="landmark">
                            </div>
                            <div class="checkout__input">
                                <p>Town/City<span>*</span></p>
                                <input type="text" name="city">
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>State<span>*</span></p>
                                    <select name="state">
                                        <option value="58">Utter Pradesh</option>

                                    </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Postcode / ZIP<span>*</span></p>
                                        <input type="text" name="pincode">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Phone<span>*</span></p>
                                        <input type="text" name="number">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Alternate Number<span>*</span></p>
                                        <input type="text" name="alternatenumber">
                                    </div>
                                </div>
                            </div>
                            <div class="checkout__input__checkbox">
                                <label for="acc">
                                    Create an account?
                                    <input type="checkbox"  checked readonly name="create_account" value="1">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <p>Create an account by entering the information below. If you are a returning customer
                                please login at the top of the page</p>
                            <div class="checkout__input">
                                <p>Email<span>*</span></p>
                                <input type="email" name="email" id="emailcheckout">
                            </div>
                            <div class="checkout__input">
                                <p>Account Password<span>*</span></p>
                                <input type="text" name="password" id="password">
                            </div>
                            <div class="checkout__input">
                                <p>Account Confirm Password<span>*</span></p>
                                <input type="text" name="confirmpassword">
                            </div>
                            {{-- <div class="checkout__input__checkbox">
                                <label for="diff-acc">
                                    Ship to a different address?
                                    <input type="checkbox" id="diff-acc">
                                    <span class="checkmark"></span>
                                </label>
                            </div> --}}
                            @endif
                            <div class="checkout__input">
                                <p>Order notes<span></span></p>
                                <input type="text"
                                    placeholder="Notes about your order, e.g. special notes for delivery." name="delivery_note">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            @if(Session::has('cart'))
                            <div class="checkout__order">
                                <h4>Your Order</h4>
                                <div class="checkout__order__products">Products <span>Total</span></div>
                                <ul>
                                    @foreach($products as $product)
                                    @php $imagearray = explode(',',$product['item']['image']); @endphp
                                        <li>{{$product['item']['name']}}  <b style="color:green">X {{$product['qty']}}</b><span> Rs {{$product['item']['discounted_price']}}</span></li>
                                    @endforeach
                                </ul>
                                <div class="checkout__order__subtotal">Subtotal <span> Rs. {{Session::has('cart')?Session::get('cart')->totalPrice:'0'}}</span></div>
                                <div class="checkout__order__total">Total <span> Rs. {{Session::has('cart')?Session::get('cart')->totalPrice:'0'}}</span></div>
                                {{-- <div class="checkout__input__checkbox">
                                    <label for="acc-or">
                                        Create an account?
                                        <input type="checkbox" id="acc-or">
                                        <span class="checkmark"></span>
                                    </label>
                                </div> --}}
                                <p>We Are currently accepting Payment using paytm</p>
                                <div class="checkout__input__checkbox">
                                    <label for="payment">
                                        Paytm
                                        <input type="radio" id="payment" checked name="payment_type" value="1">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="checkout__input__checkbox">
                                    <label for="cash">
                                        Cash
                                        <input type="radio" id="cash" name="payment_type" value="2">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <button type="submit" class="site-btn">PLACE ORDER</button>
                            </div>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @include('frontend.address_model')
        {{-- @include('frontend.login_form') --}}
    </section>
    <!-- Checkout Section End -->

    @endsection

    @push('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>

    $(document).ready(function(){
        $('#emailcheckout').on('blur', function() {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(!regex.test($(this).val())) {
                toastr.success('Invalid Emial Id!',{closeButton: false,positionClass:'toast-top-right'});
            }else{
                if($(this).val()!=''){
                    var email = $(this).val();
                    email = email;
                    $.ajax
                    ({
                        url: '/user/getExistingUser',
                        method: 'post',
                        data: { email: email, '_token':'{{ csrf_token() }}' },
                        success: function(data)
                        {
                            console.log(data.email);
                            if (data) {
                                $("#emailloginform").val(data.email);
                                toastr.success('Email is allready Registered With Us Please login to continue!',{closeButton: false,positionClass:'toast-top-right'});
                                $("#modalLRForm").modal('show');
                            }
                        }
                    });
                }
            }
        });
        $("#addressSaveForm").submit(function(e){
                e.preventDefault();
                var name = $("#addressname").val();
                var number = $("#addressnumber").val();
                var pincode = $("#addresspincode").val();
                var locality = $("#addresslocality").val();
                var address = $("#addressaddress").val();
                var city = $("#addresscity").val();
                var state = $("#addressstate").val();
                var landmark = $("#addresslandmark").val();
                var alternatenumber = $("#addressalternatenumber").val();
                $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }

                });
                $.ajax({
                    type:'POST',
                    url:"/store/address",
                    data:{name:name, number:number, pincode:pincode, locality:locality, address:address, city:city, state:state, landmark:landmark, alternatenumber:alternatenumber},
                    dataType:"json",
                    success: function (data) {
                        toastr.success('Address stored Sucessfully!',
                            {
                                closeButton: false,
                                allowHtml: true,
                                positionClass:'toast-top-right',
                            });
                        // $('.appendaddressdata').prepend("<div class='card mt-3'><div class='card-body'>"+data.data.name+" - <span class='ml-4'> "+data.data.number+" </span> <p>"+data.data.address+" ,"+data.data.locality+","+data.data.landmark+", "+data.data.city+","+data.data.state+","+data.data.pincode+", Alternate number: "+data.data.alternatenumber+"</p></div></div>");
                        $('.appendaddressdata').prepend("<label class='appendaddressdata'><input type='radio' name='address_id' class='card-input-element d-none' id='demo1' value="+data.data.id+"><div class='card card-body bg-light d-flex flex-row justify-content-between align-items-center'><div class='col-lg-4'>"+data.data.name+"   -  <span > "+data.data.number+" </span></div><p>"+data.data.address+", "+data.data.locality+", "+data.data.landmark+" , "+data.data.city+" , "+data.data.state+" ,"+data.data.pincode+" , 'Alternate Number :' ."+data.data.alternatenumber+"</p></div></label>");
                        $('.adddrresss').hide();
                        $('#addressmodel').modal('hide');

                    }
                });
            });
        // $("#checkoutForm").submit(function(e){
        //     e.preventDefault();
        //     var email = $("#emailcheckout").val();
        //     if(email){
        //     $.ajax({
        //         url: '/user/getExistingUser',
        //         method: 'post',
        //         data: { email: email, '_token':'{{ csrf_token() }}' },
        //         success: function (data) {
        //             if(email === data.email)
        //             {
        //                 $("#emailloginform").val(data.email);
        //                 toastr.success('Email is allready Registered With Us Please login to continue!',{closeButton: false,positionClass:'toast-top-right'});
        //                 $("#modalLRForm").modal('show');

        //             } else {
        //                 e.preventDefault();
        //                 document.getElementById('checkoutForm').submit();
        //             }
        //         }
        //     });
        //     }else {
        //         e.preventDefault();
        //         document.getElementById('checkoutForm').submit();
        //     }
        // });
    });
    </script>
 <script>
    $(document).ready(function () {
        $.validator.addMethod("alphabetsnspace", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    },"Please input Alphabet only!");
        var auth_id = "{{\Auth::id()}}";
        if(!auth_id){
        $('#checkoutForm').validate({
            errorElement: 'span',
            errorClass: 'help-inline text-danger',

            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    alphabetsnspace: true
                },
                number: {
                    required: true,
                    digits: true,
                    minlength:10,
                    maxlength:10
                },
                pincode: {
                    required: true,
                    digits: true,
                    minlength: 6,
                    maxlength:6

                },
                locality: {
                    required: true,
                    maxlength: 35,
                    // alphabetsnspace:true
                    // lettersonly: true

                },
                email: {
                    required: true,
                    email: true

                },
                address: {
                    required: true,

                },
                city: {
                    required: true,
                    alphabetsnspace:true
                },
                state: {
                    required: true,
                },
                landmark: {
                    required: true,

                },
                alternatenumber: {
                    required: false,
                    digits:true,
                    minlength:10,
                    maxlength:10
                },
                create_account: {
                    required: true,
                },
                password: {
                    required: true,
                },
                confirmpassword: {
                    required: true,
                    // equalTo: "#password"
                },
                create_account: {
                    required: true,
                },

            },

        });
        }
        // if(!auth_id){
        $('#addressSaveForm').validate({
            errorElement: 'span',
            errorClass: 'help-inline text-danger',
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    alphabetsnspace:true
                },
                number: {
                    required: true,
                    digits: true,
                    minlength:10,
                    maxlength:10
                },
                pincode: {
                    required: true,
                    digits: true,
                    minlength: 6,
                    maxlength:6

                },
                locality: {
                    required: true,
                    maxlength: 35,
                    // alphabetsnspace:true

                },

                address: {
                    required: true,

                },
                city: {
                    required: true,
                    alphabetsnspace:true
                },
                state: {
                    required: true,
                },
                landmark: {
                    required: true,

                },
                alternatenumber: {
                    required: false,
                    digits:true,
                    minlength:10,
                    maxlength:10
                },
            },
        });
        // }
    });
    </script>
    @endpush
