
<!--Modal: Login / Register Form-->
 <div class="modal fade" id="modalLRForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog cascading-modal" role="document">
    <!--Content-->
    <div class="modal-content">

        <!--Modal cascading tabs-->
        <div class="modal-c-tabs">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs md-tabs tabs-2" role="tablist">
            <li class="nav-item">
            <a class="nav-link active text-red" data-toggle="tab" href="#panel7" role="tab"><i class="fa fa-user mr-1"></i>
                Login</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#panel8" role="tab"><i class="fa fa-user-plus mr-1"></i>
                Register</a>
            </li>
        </ul>

        <!-- Tab panels -->
        <div class="tab-content">
            <!--Panel 7-->
            <div class="tab-pane fade in show active" id="panel7" role="tabpanel">
               <form action ={{route('login')}} method="post">
                {{ csrf_field() }}
            <!--Body-->
            <div class="modal-body mb-1">
                <div class="md-form form-sm mb-2">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope prefix"></i></span>
                    </div>
                    <input type="email" class="form-control" name="email" id="emailloginform" placeholder="email" aria-label="email" aria-describedby="basic-addon1">
                  </div>
                </div>

                <div class="md-form form-sm mb-2">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1"><i class="fa fa-lock prefix"></i></span>
                        </div>
                        <input type="text" class="form-control" name="password" placeholder="password" aria-label="password" aria-describedby="basic-addon1">
                      </div>
                </div>
                <div class="input-group mb-3">
                   <div class="col-lg-6">
                    <button type="submit" class="btn btn-outline-info"><i class="fa fa-sign-in ml-1"></i>
                        {{ trans('adminlte::adminlte.sign_in') }}
                    </button>
                   </div>
                   <div class="col-lg-6">
                    <a href="{{route('login.google',['provide'=>'Google'])}}" class="loginBtn loginBtn--google">Log With Google <i class="fa fa-google" aria-hidden="true"></i></a>
                    {{-- <button class="loginBtn loginBtn--google">
                    Login with Google
                    </button> --}}
                   </div>
                  </div>
                {{-- <div class="text-center mt-2">
                <button class="btn btn-info">Log in <i class="fa fa-sign-in ml-1"></i></button>
                </div> --}}
            </div>
            <!--Footer-->
            <div class="modal-footer">
                <div class="options text-center text-md-right mt-1">
                {{-- <p>Not a member? <a href="#" class="blue-text">Sign Up</a></p> --}}

                <p>Forgot <a href="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}" class="blue-text">Password?</a></p>
                </div>
                <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>
            </div>
            </form>
            </div>
            <!--/.Panel 7-->

            <!--Panel 8-->
            <div class="tab-pane fade" id="panel8" role="tabpanel">

            <!--Body-->
            <div class="modal-body">
                <form action="{{ url(config('adminlte.register_url', 'register')) }}" method="post" id="registeruser">
                    {{ csrf_field() }}

                    <div class="md-form form-sm mb-2">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend has-feedback {{ $errors->has('name') ? 'has-error' : '' }}"">
                              <span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope prefix"></i></span>
                            </div>
                            <input type="text" class="form-control" value="{{ old('name') }}" name="name" placeholder="{{ trans('adminlte::adminlte.full_name') }}" aria-label="email" aria-describedby="basic-addon1" autocomplete="off">
                          </div>
                          @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                        </div>
                <div class="md-form form-sm mb-1">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend has-feedback {{ $errors->has('email') ? 'has-error' : '' }}"">
                          <span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope prefix"></i></span>
                        </div>
                        <input type="email" class="form-control" name="email"  placeholder="email" aria-label="email" aria-describedby="basic-addon1" autocomplete="off">
                      </div>
                      @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    </div>
                    {{-- <br> --}}
                    <div class="md-form form-sm mb-1">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend has-feedback {{ $errors->has('mobile_no') ? 'has-error' : '' }}"">
                              <span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope prefix"></i></span>
                            </div>
                            <input type="text" class="form-control col-12" name="mobile_no"  id="mobile_no" placeholder="Mobile Number" aria-label="mobile_no" aria-describedby="basic-addon1" autocomplete="off">
                            {{-- <span class="text-danger range" style="position:absolute; top:-20px; right:50px;"><a class="text-success requestotp" href="#">Request OTP</a><span></span></span>
                            <span class="text-danger range" style="position:absolute; top:-20px; right:50px;display:none;"><a class="text-success verifyotp" href="#">Verify OTP</a><span></span></span>
                            <input type="text" class="form-control col-4" name="otp" placeholder="Otp" id="otp" aria-label="otp" aria-describedby="basic-addon1" autocomplete="off" disabled> --}}
                          </div>
                                    {{-- <div class="form-group input-group mobile_no_wrap">
                                        <div class="input-group-prepend has-feedback {{ $errors->has('email') ? 'has-error' : '' }}"">
                                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-phone"></i></span>
                                          </div>
                                        <input type="text" class="form-control onlynumbers mobileno" name="mobile_no" id="mobile_no" maxlength="10" placeholder="Mobile No.">
                                        <span class="input-group-addon mobile_no_action">
                                            <button type="button" class="btn text-primary pr-0" id="mobile_no_send_otp">SEND OTP</button>
                                            <span class="get_otp" id="get_otp">
                                                <input class="otp-input onlynumbers" type="text" maxlength="1" size="1">
                                                <input class="otp-input onlynumbers" type="text" maxlength="1" size="1">
                                                <input class="otp-input onlynumbers" type="text" maxlength="1" size="1">
                                                <input class="otp-input onlynumbers" type="text" maxlength="1" size="1">
                                                <button type="button" class="resend_otp fa fa-redo"></button>
                                                <span class="timer"><strong>23</strong><small>Sec</small></span>
                                                <i class="otpok fa fa-check"></i>
                                            </span>
                                        </span>

                                    </div> --}}
                                @if ($errors->has('mobile_no'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile_no') }}</strong>
                                    </span>
                                @endif
                        </div>
                <div class="md-form form-sm mb-2">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend has-feedback {{ $errors->has('password') ? 'has-error' : '' }}"">
                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-lock prefix"></i></span>
                        </div>
                        <input type="password" class="form-control" name="password" placeholder="password" aria-label="password" aria-describedby="basic-addon1" autocomplete="off">
                        </div>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif

                    </div>
                <div class="md-form form-sm mb-2">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}"">
                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-lock prefix"></i></span>
                        </div>
                        <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm password" aria-label="password" aria-describedby="basic-addon1" autocomplete="off">
                        </div>
                        @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                    </div>


                <div class="text-center form-sm mt-2">
                <button class="btn btn-info signup">Sign up <i class="fa fa-sign-in ml-1"></i></button>
                </div>

            </div>
            <!--Footer-->
            <div class="modal-footer">
                <div class="options text-right">
                <p class="pt-1">Already have an account? <a href="#" class="blue-text">Log In</a></p>
                </div>
                <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>
            </div>
            </form>
            </div>
            <!--/.Panel 8-->
        </div>

        </div>
    </div>
    <!--/.Content-->
    </div>
</div>
<!--Modal: Login / Register Form-->
<script>




    </script>
