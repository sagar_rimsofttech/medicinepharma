@extends('adminlte::master')

@section('adminlte_css')
<link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/icheck-bootstrap/icheck-bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/login/style.css') }}">
@yield('css')
@stop

@section('body_class', 'login-page')

@section('body')

    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <?php
                $imagesDir = 'frontend/img/loginpage/';
                $images = glob($imagesDir . '*.{jpg,jpeg,png,gif}', GLOB_BRACE);
                $random_img = basename($images[array_rand($images)]); ?>

				<div class="login100-pic js-tilt" data-tilt>
					<img src="frontend/img/loginpage/{{$random_img}}"  alt="IMG">
                </div>

                {{-- <form action="{{ url(config('adminlte.login_url', 'login')) }}" method="post">
                {{ csrf_field() }} --}}
                <form class="login100-form validate-form" action="{{route('verify') }}" method="post">
                    {{ csrf_field() }}

                    <span class="login100-form-title">
                        Verify OTP
                    </span>

                    @if(Session::has('message'))
                    <div class="alert alert-danger">
                        {{Session::get('message')}}</div>
                        @endif
                    <div class="wrap-input100 validate-input {{ $errors->has('code') ? 'has-error' : '' }}">
                        <input class="input100" type="text" name="code" placeholder="Enter OTP">
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>

                    </div>
                    @if ($errors->has('code'))
                    <span class="help-block">
                        <strong>{{ $errors->first('code') }}</strong>
                    </span>
                    @endif
                    <input type="hidden" value="{{$mobile_no}}" name="mobile">



            <div class="container-login100-form-btn">
                {{-- <button class="login100-form-btn">
							Login
                        </button> --}}
                <button type="submit" class="login100-form-btn">
                    Verify OTP
                </button>
            </div>

            <div class="text-center p-t-12">
                <a href="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}" class="text-center">
                 Request new otp
                </a>


            </div>
        </form>
    </div>
</div>
</div>

@stop

@section('adminlte_js')
@yield('js')
@stop
