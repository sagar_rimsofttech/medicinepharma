 <!-- Modal -->
 <div class="modal fade" id="addressmodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <form class="form-signin form" id="addressSaveForm" action="{{route('saveuser.address')}}" method="POST">
            {{csrf_field()}}
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add New Address</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="form-row">
                            <div class="form-label-group input-group-md mb-3 col-md-6">
                                <input type="text" id="addressname" class="form-control" placeholder="Name" name="name" required autofocus>
                                <label for="addressname">Name</label>
                            </div>
                            <div class="form-label-group input-group-md mb-3 col-md-6">
                                <input type="text" class="form-control" placeholder="Number" name="number" id="addressnumber">
                                <label for="addressnumber">Number</label>
                            </div>
                            <div class="form-label-group input-group-md mb-3 col-md-6">
                                <input type="number" class="form-control" placeholder="Pincode" name="pincode" id="addresspincode">
                                <label for="addresspincode">Pincode</label>
                            </div>
                            <div class="form-label-group input-group-md mb-3 col-md-6">
                                <input type="text" class="form-control" placeholder="Locality" name="locality" id="addresslocality">
                                <label for="addresslocality">Locality</label>
                            </div>
                            <div class="form-group  input-group-md mb-3 col-md-12">
                                <label for="addressaddress">Address</label>
                                <textarea  class="form-control" id="addressaddress" name="address"  ></textarea>
                            </div>
                            <div class="form-label-group input-group-md mb-3 col-md-6">
                                <input type="text" class="form-control" placeholder="City/District/Town" name="city" id="addresscity">
                                <label for="addresscity">City/District/Town</label>
                            </div>
                            <div class="mb-3 col-md-6">
                                <select  id="addressstate" name="state" class="selectpicker" data-live-search="true">
                                    @foreach($states as $state)
                                        <option value="{{$state->state_id}}">{{$state->state_name}}</option>
                                    @endforeach
                                </select>

                            </div>
                            <div class="form-label-group  input-group-md mb-3 col-md-6">
                                <input type="text"  class="form-control" id="addresslandmark" name="landmark" placeholder="landmark(optional)">
                                <label for="addresslandmark">Landmark(optional)</label>
                            </div>
                            <div class="form-label-group input-group-md mb-3 col-md-6">
                                <input type="text" class="form-control" name="alternatenumber" placeholder="Alternate Number" id="addressalternatenumber">
                                <label for="addressalternatenumber">Alternate Number</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-info btn-sm waves-effect" data-dismiss="modal">Close</button>
                    <button type="save" class="btn btn-outline-info btn-sm waves-effect" >+ Save Address</button>
                </div>
            </div>
        </form>
    </div>
</div>

