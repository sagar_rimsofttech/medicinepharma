@extends('layouts.frontend.app')
@section('content')
 <!-- Page Preloder -->
 <div id="preloder">
    <div class="loader"></div>
</div>
<!-- Header Section Begin -->



<!-- Breadcrumb Section Begin -->
<section class="breadcrumb-section  tinted-image" >
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2 class="text-capitalize">{{$product->name}}</h2>
                    <div class="breadcrumb__option">
                        {{-- <a href="">Home</a> --}}
                        {{-- <a href="./index.html">Vegetables</a> --}}
                        {{-- <span>Vegetable’s Package</span> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Section End -->

<!-- Product Details Section Begin -->
<section class="product-details spad">
    <div class="container">
        <div class="row">
            @php $imagearray = explode(',',$product->image); @endphp
            <div class="col-lg-6 col-md-6">
                <div class="product__details__pic">
                    <div class="product__details__pic__item">
                        <img class="product__details__pic__item--large"
                            src="{{URL::asset('storage/uploads/product/normal/'.$imagearray[0])}}" alt="" width="80%">
                    </div>
                    <div class="product__details__pic__slider owl-carousel">
                        @foreach($imagearray as $key=>$prodimage)
                        <img data-imgbigurl="{{URL::asset('storage/uploads/product/normal/'.$prodimage)}}"
                            src="{{URL::asset('storage/uploads/product/normal/'.$prodimage)}}" alt="">
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="product__details__text">
                    <h3 class="text-capitalize">{{$product->name}}</h3>
                    <div class="product__details__rating">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                        <span>(18 reviews)</span>
                    </div>
                    <div class="product__details__price">Rs {{$product->discounted_price}}</div>
                    <p>{{$product->description}}</p>
                    <div class="product__details__quantity">
                        <div class="quantity">
                            <div class="pro-qty">
                                <input type="text" value="1"  max="100" min="1" id="quantity">
                            </div>
                        </div>
                    </div>
                    {{-- <a href="{{route('product.addToCart',$product->id)}}" class="primary-btn add-cart">ADD TO CARD</a> --}}
                <a href="#" class="primary-btn add-to-cart add-cart" data-product_id="{{$product->id}}">ADD TO CARD</a>
                    <a href="#" class="heart-icon"><span class="icon_heart_alt"></span></a>
                    <ul>
                        <li><b>Availability</b> <span>In Stock</span></li>
                        <li><b>Shipping</b> <span>01 day shipping. <samp>Free pickup today</samp></span></li>
                        <li><b>Weight</b> <span>0.5 kg</span></li>
                        <li><b>Share on</b>
                            <div class="share">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="product__details__tab">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab"
                                aria-selected="true">Description</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab"
                                aria-selected="false">Information</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab"
                                aria-selected="false">Reviews <span>(1)</span></a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tabs-1" role="tabpanel">
                            <div class="product__details__tab__desc">
                                <h6>Products Infomation</h6>
                                <p>{{$product->description}}</p>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabs-2" role="tabpanel">
                            <div class="product__details__tab__desc">
                                <h6>Products Infomation</h6>
                                <p>{{$product->description}}</p>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabs-3" role="tabpanel">
                            <div class="product__details__tab__desc">
                                <h6>Products Infomation</h6>
                                <p>{{$product->description}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Product Details Section End -->

<!-- Related Product Section Begin -->
<section class="related-product">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title related__product__title">
                    <h2>Related Product</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <?php $rand = 1;
            if(count($getrelatedproduct) <=12){
                $rand = count($getrelatedproduct);
            } else{
                $rand = 12;
            }
            ?>
            {{-- @foreach ($getrelatedproduct->rand($rand) as $item) --}}
            <div class="container">
                {{-- <div class="row">
                    <div class="col-lg-12">
                        <a href="{{route('category.product',$category->id)}}" class="pull-right">
                               <h5 class="text-color"> <u>View All</u> </h5>
                        </a>
                        <a href="#" class="h3 pull-left">
                                <h3 class="h3 text-color">{{$category->name}}</h3>
                        </a>
                    </div>
                </div> --}}
                <div class="row">
                    @foreach($getrelatedproduct->random($rand) as $key=>$relproduct)
                    @if($key <=11)
                    <div class="col-md-3 col-sm-6 getdata" >
                        <div class="product-grid4">
                            <div class="product-image4">
                                @php $relImagearray = explode(',',$relproduct->image); @endphp

                                <a href="{{route('product.details',$relproduct->slug)}}">
                                    @foreach($relImagearray as $key=>$relprodimage)
                                    @if($key <=1)
                                    <img class="pic-{{$key+1}}" src="{{URL::asset('storage/uploads/product/thumbnails/'.$relprodimage)}}">
                                    @endif
                                    @endforeach
                                </a>
                                {{-- <div class="carousel-item @if($key == 0) active @else '' @endif">
                                    <img src="{{URL::asset('storage/uploads/product/thumbnails/'.$prodimage)}}" class="d-block w-100" alt="...">
                                </div> --}}

                                {{-- <ul class="social">
                                    <li><a href="#" data-tip="Quick View"><i class="fa fa-eye"></i></a></li>
                                    <li><a href="#" data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></a></li>
                                    <li><a href="#" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                                </ul> --}}
                                <?php
                                    // will check for latest or old product
                                    $pastday = \Carbon\Carbon::now()->subDays(2)->format('Y-m-d');
                                    $checkdate = $relproduct->created_at->format('Y-m-d');
                                    //
                                    $dis_per = ceil((($relproduct->price - $relproduct->discounted_price) / $relproduct->price) * 100);
                                    ?>
                                @if($checkdate >= $pastday )<span class="product-new-label">New</span>@endif
                                <span class="product-discount-label">-{{$dis_per}}%</span>
                            </div>
                            <div class="product-content">
                                <h3 class="title"><a href="{{route('product.details',$relproduct->slug)}}">{{$relproduct->name}}</a></h3>
                                <div class="price">
                                    Rs {{$relproduct->discounted_price}}
                                    <span>Rs {{$relproduct->price}}</span>
                                </div>
                                <input type="hidden" id="product_id" value="{{$relproduct->id}}">
                                <a class="add-to-cart add-cart" data-product_id="{{$relproduct->id}}" data-quantity="1" href="">ADD TO CART</a>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
            <hr>
            {{-- @endforeach --}}
        </div>
    </div>
</section>
<!-- Related Product Section End -->
@endsection

@push('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $("#quantity").attr({
        "max" : 10,
        "min" : 2
        });
    });
    $.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

    });
    $(".add-cart").click(function(e){
    e.preventDefault();
    var quantity = 1;
    if($('#quantity').val() > 1 && !$(this).data('quantity') )
    {
        quantity = $('#quantity').val();
    }
    var product_id = $(this).data('product_id');
    console.log(product_id);
    // var product_id = "{{$product->id}}";
    // var password = $("input[name=password]").val();
        $.ajax({

        type:'GET',
        url:'/add-to-cart/'+quantity+'/'+product_id,
        // data:{quantity:quantity, product_id:product_id},
        success:function(data){
            console.log(data);
            console.log(data.quntity);
            $('.show-cart-total').html(data.quntity);
            $('.show-cart-item-total').html(data.itemtotal);
            toastr.success('Product is added to cart!',{closeButton: false,positionClass:'toast-top-right'});
        }
        });
    });
</script>
@endpush
