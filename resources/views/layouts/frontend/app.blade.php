<!DOCTYPE html>
<html lang="en">
<head>

    @stack('css')
    @yield('css')
    @php
    $categories = \App\Traits\CategoryTrait::getCategoryTrait();
    @endphp
    <!-- ========== Meta Tags ========== -->
    <meta charset="UTF-8">
    <meta name="description"  content="Med2MyHome is one of the Best Medicine Delivery in Utter Pradesh. Our Site is specialized in Medicine Delivery of all kind ." />
    <meta name="keywords" content="Best Medicine Delivery in India,Best Medicine Delivery in Utter Pradesh, Best Healthcare  Medicine Delivery In India, Best Hospitals  Medicine Delivery in India, Top Medicine Delivery in India, Best Medicine Delivery Through Prescription" />
    <link rel="canonical" href="http://med2myhome.com" />
    <meta property="og:title" content="Medicine Delivery In Utter Pradesh - Med2Myhome" />
    <meta property="og:description" content="Med2MyHome is one of the Best Medicine Delivery in Utter Pradesh. Our Site is specialized in Medicine Delivery of all kind " />
    <meta property="og:url" content="http://med2myhome.com" />
    <meta name="twitter:title" content="Medicine Delivery In Utter Pradesh - Med2Myhome" />
    <meta name="twitter:description" content="Med2MyHome is one of the Best Medicine Delivery in Utter Pradesh. Our Site is specialized in Medicine Delivery of all kind" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- ========== Title ========== -->
    <title>Med2MyHome</title>
    <!-- ========== Favicon Ico ========== -->
    <!--<link rel="icon" href="fav.ico">-->
    <!-- ========== STYLESHEETS ========== -->
   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
     <!-- Google Font -->
     <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">
   <link href="{{asset('frontend/css/bootstrap.min.css')}}" rel="stylesheet">
   <!-- Fonts Icon CSS -->
   <link href="{{asset('frontend/css/font-awesome.min.css')}}" rel="stylesheet">
   <!-- Css Styles -->
   <link rel="stylesheet" href="{{asset('frontend/css/bootstrap.min.css')}}" type="text/css">
   <link rel="stylesheet" href="{{asset('frontend/css/font-awesome.min.css')}}" type="text/css">
   <link rel="stylesheet" href="{{asset('frontend/css/elegant-icons.css')}}" type="text/css">
   <link rel="stylesheet" href="{{asset('frontend/css/nice-select.css')}}" type="text/css">
   <link rel="stylesheet" href="{{asset('frontend/css/jquery-ui.min.css')}}" type="text/css">
   <link rel="stylesheet" href="{{asset('frontend/css/owl.carousel.min.css')}}" type="text/css">
   <link rel="stylesheet" href="{{asset('frontend/css/slicknav.min.css')}}" type="text/css">
   <link rel="stylesheet" href="{{asset('frontend/css/style.css')}}" type="text/css">
   <link rel="stylesheet" href="{{asset('frontend/css/floating-labels.css')}}" type="text/css">
   <link rel="stylesheet" href="{{asset('frontend/css/product_list.min.css')}}" type="text/css">
   <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
   {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css"> --}}
   <link rel="stylesheet" href="{{asset('frontend/socialmedia_float/css/socialshare.css')}}">
<link rel="stylesheet" href="{{asset('frontend/socialmedia_float/icons/all.css')}}">
</head>
<style>
    .tinted-image {
  background:
    /* top, transparent red, faked with gradient */
    linear-gradient(
      rgba(127, 173, 57, 0.57),
      rgba(127, 173, 57, 0.57)
    ),
    /* bottom, image */
    url({{asset('frontend/img/medicinebanner1.jpg')}});
    }
    .text-color{
    color:#66c6a7;
    }
    a:hover {
        color: #66c6a7;
    }
    a {
        color: #57b946;
    }

    label {
  width: 100%;
  font-size: 1rem;
}

.card-input-element+.card {
  height: calc(36px + 2*2rem);
  color: var(--success);
  -webkit-box-shadow: none;
  box-shadow: none;
  border: 2px solid transparent;
  border-radius: 4px;
}

.card-input-element+.card:hover {
  cursor: pointer;
}

.card-input-element:checked+.card {
  border: 2px solid var(--success);
  -webkit-transition: border .3s;
  -o-transition: border .3s;
  transition: border .3s;
}

.card-input-element:checked+.card::after {
  content: '\e5ca';
  color: #28a745;
  font-family: 'Material Icons';
  font-size: 24px;
  -webkit-animation-name: fadeInCheckbox;
  animation-name: fadeInCheckbox;
  -webkit-animation-duration: .5s;
  animation-duration: .5s;
  -webkit-animation-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
  animation-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
}

@-webkit-keyframes fadeInCheckbox {
  from {
    opacity: 0;
    -webkit-transform: rotateZ(-20deg);
  }
  to {
    opacity: 1;
    -webkit-transform: rotateZ(0deg);
  }
}

@keyframes fadeInCheckbox {
  from {
    opacity: 0;
    transform: rotateZ(-20deg);
  }
  to {
    opacity: 1;
    transform: rotateZ(0deg);
  }
}
.dorp-down-custom{
position: absolute;
    /* top: 100%; */
    width: 96%;
    z-index: 9;
    /* z-index: 1000; */
    display: block;
    /* float: left; */
    min-width: 10rem;
    /* padding: .5rem 0; */
    margin: .125rem 0 0;
    font-size: 1rem;
    color: #212529;
    text-align: left;
    list-style: none;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid rgba(0,0,0,.15);
    border-radius: .25rem;
}

.input-group>.input-group-append:last-child>.btn:not(:last-child):not(.dropdown-toggle), .input-group>.input-group-append:last-child>.input-group-text:not(:last-child), .input-group>.input-group-append:not(:last-child)>.btn, .input-group>.input-group-append:not(:last-child)>.input-group-text, .input-group>.input-group-prepend>.btn, .input-group>.input-group-prepend>.input-group-text {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    background: #7fad39;
}

/* Shared */
.loginBtn {
  box-sizing: border-box;
  position: relative;
  /* width: 13em;  - apply for fixed size */
  margin: 0.2em;
  padding: 0 15px 0 46px;
  border: none;
  text-align: left;
  line-height: 34px;
  white-space: nowrap;
  border-radius: 0.2em;
  font-size: 16px;
  color: #FFF;
}
.loginBtn:before {
  content: "";
  box-sizing: border-box;
  position: absolute;
  top: 0;
  left: 0;
  width: 34px;
  height: 100%;
}
.loginBtn:focus {
  outline: none;
}
.loginBtn:active {
  box-shadow: inset 0 0 0 32px rgba(0,0,0,0.1);
}


/* Facebook */
.loginBtn--facebook {
  background-color: #4C69BA;
  background-image: linear-gradient(#4C69BA, #3B55A0);
  /*font-family: "Helvetica neue", Helvetica Neue, Helvetica, Arial, sans-serif;*/
  text-shadow: 0 -1px 0 #354C8C;
}
.loginBtn--facebook:before {
  border-right: #364e92 1px solid;
  background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_facebook.png') 6px 6px no-repeat;
}
.loginBtn--facebook:hover,
.loginBtn--facebook:focus {
  background-color: #5B7BD5;
  background-image: linear-gradient(#5B7BD5, #4864B1);
}


/* Google */
.loginBtn--google {
  /*font-family: "Roboto", Roboto, arial, sans-serif;*/
  background: #DD4B39;
}
.loginBtn--google:before {
  border-right: #BB3F30 1px solid;
  background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_google.png') 6px 6px no-repeat;
}
.loginBtn--google:hover,
.loginBtn--google:focus {
  background: #E74B37;
}
.bg-site-color{
   background-color: #7fad39!important;

}
.bg-site-text-color{
   color: #7fad39!important;

}
/* sticky bar  */
.socialsticky {
    position: fixed;
    z-index: 999;
    width: 60px;
    margin-top: 150px;
    transition: all 0.3s linear;
    box-shadow: 2px 2px 8px 0px rgba(0, 0, 0, .4)
}

.socialsticky ul {
    margin-top: 0;
    margin-bottom: 0rem;
}

.socialsticky li {
    height: 45px;
    position: relative
}

.socialsticky li a {
    color: #fff !important;
    display: block;
    height: 100%;
    width: 100%;
    line-height: 45px;
    padding-left: 25%;
    /* border-bottom: 1px solid rgba(0, 0, 0, 0.4); */
    transition: all .3s linear;
    text-decoration: none !important
}

.socialsticky li:nth-child(1) a {
    background:#28a745;
    /* color:red; */
}

.socialsticky li:nth-child(2) a {
    background: #28a745;
}

.socialsticky li:nth-child(3) a {
    background: #E1306C
}

.socialsticky li:nth-child(4) a {
    background: #2867B2
}

.socialsticky li:nth-child(5) a {
    background: #333
}

.socialsticky li:nth-child(6) a {
    background: #ff0000
}

.socialsticky li a i {
    position: absolute;
    top: 14px;
    left: 24px;
    font-size: 15px
}

.socialsticky ul li a span {
    display: none;
    font-weight: bold;
    letter-spacing: 1px;
    /* text-transform: uppercase */
}

.socialsticky a:hover {
    z-index: 1;
    width: 230px;
    /* border-bottom: 1px solid rgba(0, 0, 0, .5); */
    /* box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3) */
}

.socialsticky ul li:hover a span {
    padding-left: 15%;
    display: block;
    font-size: 15px
}
    </style>
     <style>
        .mobile_no_wrap .form-control{border-right: none;}
        .mobile_no_action .get_otp {display: none; position: relative; height: 30px;}
        .form-group.input-group .input-group-addon.mobile_no_action {background: #fff; padding: 0px 10px; position: relative;}
        .form-group.input-group .input-group-addon.mobile_no_action .btn {padding-top: 0.25rem; padding-bottom: 0.25rem; background: none;}
        .form-group.input-group .input-group-addon.mobile_no_action .btn:focus{border: none; outline: none;}
        .mobile_no_action .get_otp .resend_otp {display: none; border: none; background: none; color: #d24350; position: relative;  top: 5px;}
        .mobile_no_action .get_otp .otp-input {text-align: center; width: 24px; height: 28px; border: none; outline: none;
; padding: 3px; margin: 0px; border-bottom: solid 0.1rem #ccc !important; position: relative; top:0px}
    .mobile_no_action .get_otp .timer {width: 28px; height: 28px; border-radius: 50%; background: #f4f4f4; display: inline-block;
    line-height: 8px; display: none; font-size: 12px; text-align: center; position: relative; top:-3px;}
    .mobile_no_action .get_otp .timer strong {color: #d24350; display: block; position: relative; top: 6px;}
    .mobile_no_action .get_otp .timer small {display: block; position: relative; top: 10px; font-size: 70%;}

    </style>
<body>
 <!-- Page Preloder -->
 {{-- <div id="preloder">
    <div class="loader"></div>
</div> --}}
 <div class="socialsticky">
    <ul>
        <li><a href="#" class="" type="button"  data-toggle="modal" data-target="#prescriptionupload"><i class="fas fa-clipboard-prescription"><img class="pull-left" src="https://med2myhome.com/frontend/img/prescription.png" width="60%" alt=""></i><span  class="ml-2">upload prescription</span></a></li>
    </ul>
</div>
<!-- Humberger Begin -->
<div class="humberger__menu__overlay"></div>
<div class="humberger__menu__wrapper">
    <div class="humberger__menu__logo">
        {{-- <a href="#"><img src="{{asset('frontend/img/logo.png')}}" alt=""></a> --}}
        <a href="{{route('showIndex')}}"><span style="font-size:26px;"><h2 class="bg-site-text-color">med2myhome</h2></span></a>
    </div>
    <div class="humberger__menu__cart">
        <ul>
            {{-- <li><a href="#"><i class="fa fa-heart"></i> <span>1</span></a></li> --}}
            <li><a href="{{route('product.shoppingCart')}}"><i class="fa fa-shopping-bag"></i> <span class="show-cart-total">{{Session::has('cart')?Session::get('cart')->totalQty:'0'}}</span></a></li>
        </ul>
        <div class="header__cart__price">item: <span class="text-color">Rs </span><span class="show-cart-item-total"> {{Session::has('cart')?Session::get('cart')->totalPrice:'0'}}</span></div>

    </div>
    <div class="humberger__menu__widget">
        <div class="header__top__right__language">
            <img src="{{asset('img/language.png')}}" alt="">
            <div>English</div>
            <span class="arrow_carrot-down"></span>
            <ul>
                {{-- <li><a href="#">Spanis</a></li> --}}
                <li><a href="#">English</a></li>
            </ul>
        </div>
        <div class="header__top__right__auth">
            @if (Route::has('login'))
            @auth

               <div class="header__top__right__language">
                <img src="img/language.png" alt="">
               <span class="text-capitalize"> Hi, {{Auth::user()->name}} </span>
                <span class="arrow_carrot-down"></span>
                <ul>
                    <li><a href="{{route('user.profile')}}">My Account</a></li>
                    <li><a href="#">
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </a></li>
                </ul>
            </div>
            </li>
                {{-- <a href="{{ url('/home') }}">Home</a> --}}
            @else
            <div class="header__top__right__auth">
                {{-- <a href="{{route('login')}}" data-toggle="modal" data-target="#modalLRForm"><i class="fa fa-user"></i> Login / Sign Up</a> --}}
                <a href="{{route('login')}}"><i class="fa fa-user"></i> Login / Sign Up</a>
            </div>
{{--
                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                @endif --}}
            @endauth
    @endif
        </div>
    </div>
    <nav class="humberger__menu__nav mobile-menu">
        <ul>
            <li class="{{Request::is('/')?'active':''}}"><a href="{{route('showIndex')}}'">Home</a></li>
            <li class="{{Request::has('tab') && Request::get('tab') == 'myaccount'?'active':''}}"><a href="{{route('user.profile',['tab'=>'myaccount'])}}">My Account</a></li>
            <li class="{{Request::has('tab') && Request::get('tab') == 'myorder'?'active':''}}"><a href="{{route('user.profile',['tab'=>'myorder'])}}">My Order</a></li>
            <li class="{{Request::has('tab') && Request::get('tab') == 'changepassword'?'active':''}}"><a href="{{route('user.profile',['tab'=>'changepassword'])}}">Change Password</a></li>
            <li><a href="#" type="button"  data-toggle="modal" data-target="#prescriptionupload">Upload Prescription</a></li>
            {{-- <li><a href="#">Pages</a>
                <ul class="header__menu__dropdown">
                    <li><a href="./shop-details.html">Shop Details</a></li>
                    <li><a href="./shoping-cart.html">Shoping Cart</a></li>
                    <li><a href="./checkout.html">Check Out</a></li>
                    <li><a href="./blog-details.html">Blog Details</a></li>
                </ul>
            </li> --}}
            {{-- <li><a href="./blog.html">Blog</a></li> --}}
            {{-- <li><a href="./contact.html">Contact</a></li> --}}
        </ul>
    </nav>
    <div id="mobile-menu-wrap"></div>
    <div class="header__top__right__social">
        <a href="#"><i class="fa fa-facebook"></i></a>
        <a href="#"><i class="fa fa-twitter"></i></a>
        <a href="#"><i class="fa fa-linkedin"></i></a>
        <a href="#"><i class="fa fa-pinterest-p"></i></a>
    </div>
    <div class="humberger__menu__contact">
        <ul>
            <li><i class="fa fa-envelope"></i> support@med2myhome.com</li>
            <li>Free Shipping for all Order of Rs 99</li>
        </ul>
    </div>
</div>
<!-- Humberger End -->

<!-- Header Section Begin -->
<header class="header">
    <div class="header__top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="header__top__left">
                        <ul>
                            <li><i class="fa fa-envelope"></i> support@med2myhome.com</li>
                            <li>Free Shipping for all Order of Rs 99</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="header__top__right">
                        <div class="header__top__right__social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                        </div>
                        <div class="header__top__right__language">
                            <img src="img/language.png" alt="">
                            <div>English</div>
                            <span class="arrow_carrot-down"></span>
                            <ul>
                                {{-- <li><a href="#">Spanis</a></li> --}}
                                <li><a href="#">English</a></li>
                            </ul>
                        </div>
                        @if (Route::has('login'))
                            @auth

                               <div class="header__top__right__language">
                                <img src="img/language.png" alt="">
                               <span class="text-capitalize"> Hi, {{Auth::user()->name}} </span>
                                <span class="arrow_carrot-down"></span>
                                <ul>
                                    <li><a href="{{route('user.profile')}}">My Account</a></li>
                                    <li><a href="#">
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div></a></li>
                                </ul>
                            </div>
                            </li>
                                {{-- <a href="{{ url('/home') }}">Home</a> --}}
                            @else
                            <div class="header__top__right__auth">
                                {{-- <a href="#" data-toggle="modal" data-target="#modalLRForm"><i class="fa fa-user"></i> Login / Sign Up</a> --}}
                                <a href="{{route('login')}}"><i class="fa fa-user"></i> Login / Sign Up</a>
                            </div>
{{--
                                @if (Route::has('register'))
                                    <a href="{{ route('register') }}">Register</a>
                                @endif --}}
                            @endauth
                    @endif
                        {{-- <div class="header__top__right__auth">
                            <a href="#" data-toggle="modal" data-target="#modalLRForm"><i class="fa fa-user"></i> Login</a>
                        </div> --}}

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('frontend.login_form')
    <!--Modal: Login / Register Form-->


    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="header__logo">
                    <a href="{{route('showIndex')}}"><span style="font-size:26px;" class=""><b><h2 class="bg-site-text-color">med2myhome</h2></b></span></a>
                </div>
            </div>
            <div class="col-lg-7">
                <nav class="header__menu">
                    <ul>
                        <li class="{{Request::is('/')?'active':''}}"><a href="{{route('showIndex')}}'">Home</a></li>
                        <li class="{{Request::has('tab') && Request::get('tab') == 'myaccount'?'active':''}}"><a href="{{route('user.profile',['tab'=>'myaccount'])}}">My Account</a></li>
                        <li class="{{Request::has('tab') && Request::get('tab') == 'myorder'?'active':''}}"><a href="{{route('user.profile',['tab'=>'myorder'])}}">My Order</a></li>
                        <li class="{{Request::has('tab') && Request::get('tab') == 'changepassword'?'active':''}}"><a href="{{route('user.profile',['tab'=>'changepassword'])}}">Change Password</a></li>
                        {{-- <li><a href="#">Pages</a>
                            <ul class="header__menu__dropdown">
                                <li><a href="./shop-details.html">Shop Details</a></li>
                                <li><a href="./shoping-cart.html">Shoping Cart</a></li>
                                <li><a href="./checkout.html">Check Out</a></li>
                                <li><a href="./blog-details.html">Blog Details</a></li>
                            </ul>
                        </li> --}}
                        {{-- <li><a href="./blog.html">Blog</a></li> --}}
                        {{-- <li><a href="./contact.html">Contact</a></li> --}}
                    </ul>
                </nav>
            </div>
            <div class="col-lg-2">
                <div class="header__cart">
                    <ul>
                        {{-- <li><a href="#"><i class="fa fa-heart"></i> <span>1</span></a></li> --}}
                        <li><a href="{{route('product.shoppingCart')}}"><i class="fa fa-shopping-bag"></i> <span class="show-cart-total">{{Session::has('cart')?Session::get('cart')->totalQty:'0'}}</span></a></li>
                    </ul>
                    <div class="header__cart__price">item: <span class="text-color">Rs </span><span class="show-cart-item-total"> {{Session::has('cart')?Session::get('cart')->totalPrice:'0'}}</span></div>
                </div>
            </div>
        </div>
        <div class="humberger__open">
            <i class="fa fa-bars"></i>
        </div>
    </div>
</header>
<!-- Header Section End -->

{{-- <div class="social-share social-share-sticky" data-social-share="OPTIONS HERE">
    <div class="btn-group dropright">
      <button class="btn btn-outline-success dropdown-toggle" type="button"  data-toggle="modal" data-target="#prescriptionupload">

        <span class="text-uppercase" aria-hidden="true">
          <span>UPLOAD</span>
          <span>PRESCRIPTION <i class="fa fa-upload"></i></span>
        </span>
      </button>
    </div>
  </div> --}}
@include('frontend.prescription-modal')
<!-- Hero Section Begin -->


<section class="hero hero-normal">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="hero__categories">
                    <div class="hero__categories__all">
                        <i class="fa fa-bars"></i>
                        <span>All Categories</span>
                    </div>
                    @php $style = "style=display:block;"; @endphp
                    <ul class="showcategoryclass" {{Request::is('/')?$style:''}}>
                        @foreach($categories as $category)
                        <li><a href="{{route('category.product',$category->id)}}" >{{$category->name}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="row">
                <div class="col-lg-8">
                    <form action="#">
                <div class="hero__search">
                    <div class="md-form form-sm mb-2">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend text-success">
                                <span class="input-group-text text-white" ><i class="fa fa-search" aria-hidden="true"></i></span>
                            </div>
                            <input type="search" name="product_name_search" id="product_name_search" class="form-control input-lg" placeholder="Search what You looking for?">
                            </div>
                        </div>
                <div id="productList">
                </div>
                {{ csrf_field() }}
                </div>
            </form>

            </div>

            <div class="col-lg-4">
                <div class="hero__search__phone">
                    <div class="hero__search__phone__icon hidecontent">
                        <ul class="social" style=" list-style:none;">
                            <li><a href="" class="fa fa-phone"></a></li>
                        </ul>
                    </div>
                    <div class="hero__search__phone__text hidecontent">
                        <h5>+91 9569486224</h5>
                        <span>support 24/7 time</span>
                    </div>
                    <div class="showcontent">
                        {{-- <div class="card text-white" style="background-color: #7fad39;height:4em;">
                            <div class="card-body text-center">
                                <a href="#" type="button"  data-toggle="modal" data-target="#prescriptionupload">
                                <img class="pull-left ml-3" src="{{asset('frontend/img/prescription.png')}}" width="10%" alt="">  <span class="mr-5">Upload Prescription</span></a>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>

            <div>
            </div>
        </div>
    </div>
</section>
<!-- Hero Section End -->
    @yield('content')


 <!-- Footer Section Begin -->
 <footer class="footer spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="footer__about">
                    <div class="footer__about__logo">
                        <a href="./index.html"><img src="img/logo.png" alt=""></a>
                    </div>
                    <ul>
                        <li><b>Vishwanath trending company</b></li>
                        <li><b>Address</b>: H.n.153/A, Western bazar, Galla Mandi, Mughalsarai, Chandauli</li>
                        <li><b>Phone</b>: +91 9569486224</li>
                        <li><b>Email</b>: support@med2myhome.com</li>
                        <li><b>Pin </b>: 232101</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-1">
                <div class="footer__widget">
                    <h6>Useful Links</h6>
                    <ul>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">About Our Shop</a></li>
                        <li><a href="#">Secure Shopping</a></li>
                        <li><a href="#">Delivery infomation</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Our Sitemap</a></li>
                    </ul>
                    <ul>
                        <li><a href="#">Who We Are</a></li>
                        <li><a href="#">Our Services</a></li>
                        <li><a href="#">Projects</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Innovation</a></li>
                        <li><a href="#">Testimonials</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="footer__widget">
                    <h6>Join Our Newsletter Now</h6>
                    <p>Get E-mail updates about our latest shop and special offers.</p>
                    <form action="#">
                        <input type="text" placeholder="Enter your mail">
                        <button type="submit" class="site-btn">Subscribe</button>
                    </form>
                    <div class="footer__widget__social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-pinterest"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="footer__copyright">
                    <div class="footer__copyright__text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                    <div class="footer__copyright__payment"><img src="img/payment-item.png" alt=""></div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Section End -->
@stack('js')
@yield('js')
    <script src="{{asset('frontend/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('frontend/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('frontend/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('frontend/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('frontend/js/mixitup.min.js')}}"></script>
    <script src="{{asset('frontend/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('frontend/js/main.js')}}"></script>
    <script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> --}}
    {!! Toastr::message() !!}
    <script>
    @if($errors->any())
        @foreach($errors->all() as $error)
            toastr.error('{{ $error }}','Error',{
                closeButton:true,
                progressBar:true,
            });
        @endforeach
    @endif
    @if(session()->has('message'))
        toastr.success('{{ session()->get('message') }}','Success',{
                closeButton:true,
                progressBar:true,
            });
    @endif
    </script>
    <script type="text/javascript">
        function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if ( (charCode > 31 && charCode < 48) || charCode > 57) {
                    return false;
                }
                return true;
            }
        </script>
        <script>
            $(document).ready(function(){
                $("#registeruser").submit(function(event){
                    event.preventDefault();
                    alert(1);
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type:'POST',
                        url:"{{ url(config('adminlte.register_url', 'register')) }}",
                        data:$("#registeruser").serialize(),
                        dataType:"json",
                        success: function (data) {
                            console.log(1);
                            $("#registeruser").hide();

                        },error: function (jqXHR, exception) {
                            console.log(jqXHR.status );
                            // console.log(jqXHR.responseJSON.message );
                            if(jqXHR.responseJSON.errors){
                            $.each(jqXHR.responseJSON.errors, function(key, val) {
                                toastr.error(''+val+'','Error',{
                                closeButton:true,
                                progressBar:true,
                                });
                            });
                            return false;
                        } else if (jqXHR.status == 400){
                            toastr.error(jqXHR.responseJSON.message,
                                {
                                    closeButton: false,
                                    allowHtml: true,
                                    positionClass:'toast-top-right',
                                });
                            }
                        }
                    });
                });
            $('#product_name_search').on('input', function(e) {
                    var query = $(this).val();
                    if('' == query) {
                        $('#productList').hide();
                    }
                    if(query != '' && query.length >= 3)
                    {
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                    url:"{{ route('search.fetch') }}",
                    method:"POST",
                    data:{query:query, _token:_token},
                    success:function(data){
                    $('#productList').fadeIn();
                    $('#productList').html(data);
                    }
                    });

                    }
                    $('#productList').hide();
                });

                $(document).on('click', '.click-product', function(){
                    $('#product_name_search').val($(this).text());
                    $('#productList').fadeOut();
                });

            });
            </script>
              <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
              <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

   <script>
    $(document).ready(function () {
        $.validator.addMethod("alphabetsnspace", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
        },"Please input Alphabet only!");
        var auth_id = "{{\Auth::id()}}";
        if(!auth_id){
        $('#prescriptionForm').validate({
            errorElement: 'span',
            errorClass: 'help-inline text-danger',
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    alphabetsnspace: true
                },
                number: {
                    required: true,
                    digits: true,
                    minlength:10,
                    maxlength:10
                },
                pincode: {
                    required: true,
                    digits: true,
                    minlength: 6,
                    maxlength:6

                },
                locality: {
                    required: true,
                    maxlength: 35,
                    // lettersonly: true

                },
                email: {
                    required: true,
                    email: true

                },
                address: {
                    required: true,

                },
                city: {
                    required: true,
                    alphabetsnspace: true
                },
                state: {
                    required: true,
                },
                landmark: {
                    required: true,

                },
                alternatenumber: {
                    required: false,
                    digits:true,
                    minlength:10,
                    maxlength:10
                },
                create_account: {
                    required: true,
                },
                password: {
                    required: true,
                },
                confirmpassword: {
                    required: true,
                },
                create_account: {
                    required: true,
                },
                prescription: {
                    required: true,
                    extension: "jpeg|png|pdf|doc|docx|jpg"
                },
            },

        });
        }
    });
    </script>
<script type="text/javascript">
$(document).ready(function () {
    var url = "{{Request::is('/')}}";
    if(screen.width >= 699 && url == 1){
       $('.showcategoryclass').show();
    } else if(screen.width >= 699 && url != 1){
        $('.showcategoryclass').hide();
    } else{
        $('.showcategoryclass').hide();
    }
});
    </script>
    <script>
        $(document).ready(function () {
            $(".requestotp").click(function(e){
                var mobile_no = $('#mobile_no').val();
                if(mobile_no.length != 0)
                {
                    $.ajax({
                    url:"/admin/requestotp/"+mobile_no,
                    dataType:"json",
                    success: function (data) {
                        console.log("adasd"+data);

                    }
                });
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
                $(document).on('keypress', '.onlynumbers', function(e){
                    if (e.which != 32 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 8) {
                    return false;
                    }
                });



                // $('.mobileno').keyup(function() {
                //     var cardValue = $(this).val().split(" - ").join("");
                //     if (cardValue.length > 0) {
                //         cardValue = cardValue.match(new RegExp('.{1,4}', 'g')).join(" - ");
                //     }
                //     $(this).val(cardValue);
                // });



                $(document).on('click', '#mobile_no_send_otp', function () {
                        if($("#mobile_no").val() != ''){
                        $(this).hide();
                        $("#get_otp, .timer").css('display','inline-block');

                        $(".otp-input").keyup(function (e) {
                            var $this = $(this);
                            if ($this.val().length == $this.attr("maxlength")) {
                            $this.next(":input").focus();
                        }
                        });

                        $(".otp-input:last").keyup(function (e) {
                        $(".otpok").css('display', 'inline-block');
                        $(".timer").hide();
                        });
                        }
                })

        })
        $(function () {
          $(document).on('keypress', '.onlynumbers', function(e){
    if (e.which != 32 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 8) {
    return false;
    }
});
});
    </script>
    <script>
        $("#fileUpload").change(function () {
        filePreview(this);
    });
    function filePreview(input) {
        $('#preview').html('');
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview').html('<embed src="'+e.target.result+'" width="450" height="200">');
            }

            reader.readAsDataURL(input.files[0]);
        }

    }
        </script>
</body>
</html>
