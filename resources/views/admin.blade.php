@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <p>You are logged in as Admin!</p>

    <div class="container-fluid">
        <button type="button" class="btn btn-secondary" data-toggle="popover" data-placement="top" title="Popup title" data-content="Popup content">Trigger</button>
    </div>
@stop
