<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->biginteger('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->biginteger('brand_id')->unsigned();
            $table->foreign('brand_id')->references('id')->on('brands');
            // $table->biginteger('subcategory_id')->unsigned();
            // $table->foreign('subcategory_id')->references('id')->on('sub_categories');
            $table->integer('price');
            $table->integer('discounted_price');
            $table->integer('quantity');
            $table->boolean('is_active')->default(false);
            $table->boolean('is_hot_product')->default(false);
            $table->boolean('is_special_product')->default(false);
            $table->boolean('is_dailyneed_product')->default(false);
            $table->string('image');
            $table->string('slug');
            $table->string('description')->nullable();
            $table->biginteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users');
            $table->biginteger('updated_by')->unsigned()->nullable();
            $table->foreign('updated_by')->references('id')->on('users');
            $table->biginteger('deleted_by')->unsigned()->nullable();
            $table->foreign('deleted_by')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
