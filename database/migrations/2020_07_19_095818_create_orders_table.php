<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_code');
            $table->string('cart');
            // $table->integer('quantity');
            $table->dateTime('order_date');
            $table->dateTime('delivery_date');
            $table->integer('amount');
            $table->integer('payment_method')->default(1);
            $table->integer('payment_status');
            $table->string('tarnsaction_id');
            $table->integer('order_status')->default(0);
            $table->integer('user_id');
            $table->integer('user_delivery_address');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
