<?php

namespace App\MedicinePharma\Transformers\Api;

use DB;

class CategoryTransformer extends BaseTransformer
{

    public function getCategoryData($category = null)
    {
        return [
            'id' => $category->id,
            'name' => $category->name,
            'description' => $category->description,
            'category_image' => (strlen($category->image) === 0) ? '' : $this->imageUrl($category->image),
            'created_date' => $category->created_at->format('d/m/Y'),
        ];
    }


}
