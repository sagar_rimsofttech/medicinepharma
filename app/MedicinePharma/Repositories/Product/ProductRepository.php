<?php

namespace App\MedicinePharma\Repositories\Product;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\SubCategory;
use Illuminate\Support\Facades\DB;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;

class ProductRepository implements ProductInterface
{
    /**
     * @var Product
     */
    private $subcategory;
    private $product;
    private $brand;
    /**
     * CategoryRepository constructor.
     * @param SubCategory $role
     */
    public function __construct(SubCategory $subcategory,Product $product,Brand $brand){
        $this->subcategory = $subcategory;
        $this->product = $product;
        $this->brand = $brand;
    }

    public function getAll($keyword = null)
    {
        $product = $this->product->latest()->get();
        return $product;
    }

    public function store($request)
    {
        $images = $request->file('image');
        $slug = str_slug($request->name);
        if (isset($images))
        {
            $imagenamesave =[];
            foreach($images as $image)
            {
//            make unique name for image
            $currentDate = Carbon::now()->toDateString();
            $imagename = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
//            check subcategory dir is exists
            if (!Storage::disk('public')->exists('/uploads/product/thumbnails'))
            {
                Storage::disk('public')->makeDirectory('/uploads/product/thumbnails');
            }
            if (!Storage::disk('public')->exists('/uploads/product/normal'))
            {
                Storage::disk('public')->makeDirectory('/uploads/product/normal');
            }
//            resize image for product and upload
            $productImageThumb = Image::make($image)->resize(500,500)->stream();
            Storage::disk('public')->put('uploads/product/thumbnails/'.$imagename,$productImageThumb);
            $productImageNormal = Image::make($image)->stream();
            Storage::disk('public')->put('uploads/product/normal/'.$imagename,$productImageNormal);
            $imagenamesave[] = $imagename;
        }
        } else {
            $imagenamesave[] = "";
        }
        $product = $this->product;
        $product->name = $request->name;
        $product->category_id = $request->category_id;
        $product->brand_id = $request->brand_id;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->discounted_price = $request->discountedprice;
        $product->quantity = $request->quantity;
        $product->slug = $slug;
        if ($request->has('is_active')) {
            $product->is_active = !empty($request->get('is_active')) ? $request->get('is_active') : 0;
        }
        if ($request->has('is_hot_product')) {
            $product->is_hot_product = !empty($request->get('is_hot_product')) ? $request->get('is_hot_product') : 0;
        }
        if ($request->has('is_special_product')) {
            $product->is_special_product = !empty($request->get('is_special_product')) ? $request->get('is_special_product') : 0;
        }
        if ($request->has('is_dailyneed_product')) {
            $product->is_dailyneed_product = !empty($request->get('is_dailyneed_product')) ? $request->get('is_dailyneed_product') : 0;
        }
        $product->image = implode(',',$imagenamesave);
        $product->save();
        return $product;

    }

    public function find($id)
    {
        $product = $this->product::findOrFail($id);
        $categories_name = Category::where('id',$product->category_id)->select('name')->first();
        $brands_name = Brand::where('id',$product->brand_id)->select('name')->first();
        // $subcategories_name = SubCategory::where('id',$product->subcategory_id)->select('name')->first();
        $product->category_name = $categories_name->name;
        $product->brand_name = $brands_name->name;
        // $product->subcategory_name = $subcategories_name->name;
        return $product;
    }

    public function update($request, $id)
    {
        $images = $request->file('image');
        $slug = str_slug($request->name);
        $product = $this->product->findOrFail($id);
        if (isset($images))
        {
            foreach(explode(",",$product->image) as $image)
            {
                if (Storage::disk('public')->exists('/uploads/product/thumbnails'))
                    {
                        $productImage = URL::asset("storage/uploads/product/thumbnails/".$image);
                        File::delete($productImage);
                    }
                if (Storage::disk('public')->exists('/uploads/product/normal'))
                    {
                        $productImage = URL::asset("storage/uploads/product/normal/".$image);
                        File::delete($productImage);
                    }
            }
            foreach($images as $image)

            {
//            make unique name for image
            $currentDate = Carbon::now()->toDateString();
            $imagename = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
//            check product dir is exists
            if (!Storage::disk('public')->exists('/uploads/product/thumbnails'))
            {
                Storage::disk('public')->makeDirectory('/uploads/product/thumbnails');
            }
            if (!Storage::disk('public')->exists('/uploads/product/normal'))
            {
                Storage::disk('public')->makeDirectory('/uploads/product/normal');
            }
//            resize image for product and upload
            $productImageThumb = Image::make($image)->resize(500,500)->stream();
            Storage::disk('public')->put('uploads/product/thumbnails/'.$imagename,$productImageThumb);
            $productImageNormal = Image::make($image)->stream();
            Storage::disk('public')->put('uploads/product/normal/'.$imagename,$productImageNormal);
            $imagenamesave[] = $imagename;
        }
        } else {
            $imagenamesave[] = $product->image;
        }

        $product->name = $request->name;
        $product->category_id = $request->category_id;
        // $product->subcategory_id = $request->subcategory_id;
        $product->description = $request->description;
        $product->price = $request->price??0;
        $product->quantity = $request->quantity??0;
        $product->discounted_price = $request->discountedprice;
        $product->brand_id = $request->brand_id;
        $product->is_active = $request->is_active;
        $product->is_hot_product = $request->is_hot_product??0;
        $product->is_special_product = $request->is_special_product??0;
        $product->is_dailyneed_product = $request->is_dailyneed_product??0;
        $product->slug = $slug;
        if ($request->has('is_active')) {
            $product->is_active = !empty($request->get('is_active')) ? $request->get('is_active') : 0;
        }
        if ($request->has('is_hot_product')) {
            $product->is_hot_product = !empty($request->get('is_hot_product')) ? $request->get('is_hot_product') : 0;
        }
        if ($request->has('is_special_product')) {
            $product->is_special_product = !empty($request->get('is_special_product')) ? $request->get('is_special_product') : 0;
        }
        if ($request->has('is_dailyneed_product')) {
            $product->is_dailyneed_product = !empty($request->get('is_dailyneed_product')) ? $request->get('is_dailyneed_product') : 0;
        }
        $product->image = implode(',',$imagenamesave);
        $product->save();
        return $product;
    }

    public function delete($id)
    {
        $product = $this->product->findorFail($id);

        $product->delete();
        return $product;
    }
    public function getProductdetails($request=null,$id)
    {
        $query = $this->product::query();
        if(!empty($id))
        {
            $query= $query->where('category_id',$id);
        }
        if (isset($request) && $request->has('sortby')){
            if($request->sortby == 1){
                $query= $query->orderBy('discounted_price','asc');
            }
            if($request->sortby == 2){
                $query= $query->orderBy('discounted_price','desc');
            }
        }
        if( isset($request) && $request->has('minprice') && $request->has('maxprice'))
        {
            $query = $query->whereBetween('discounted_price', [$request->minprice, $request->maxprice]);
        }
        if( isset($request) && $request->has('popular'))
        {
            if($request->popular == 0){
                $query = $query->where('is_hot_product',1);
            }
            if($request->popular == 1)
            {
                $query = $query->where('is_dailyneed_product',1);
            }
            if($request->popular == 2)
            {
                $query = $query->where('is_special_product',1);
            }
        }
        $query = $query->latest()->paginate(15);
         return $query;
    }

    public function getLatestProduct()
    {
        $product = $this->product->latest()->take(6)->get();
        return $product;
    }

}
