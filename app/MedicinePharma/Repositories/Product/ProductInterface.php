<?php

namespace App\MedicinePharma\Repositories\Product;

interface ProductInterface
{
    public function getAll();
    public function store($request);
    public function find($id);
    public function update($request, $id);
    public function delete($id);
    public function getProductdetails($request,$id);
    public function getLatestProduct();
    
}
