<?php

namespace App\MedicinePharma\Repositories\Role;

// use App\Ledger\Repositories\User\UserInterface;
use App\Role;
use Illuminate\Support\Facades\DB;
use App\MedicinePharma\Repositories\Permission\PermissionInterface;

class RoleRepository implements RoleInterface
{
    /**
     * @var Role
     */
    private $role;
    private $permission;
    private $user;
    /**
     * RoleRepository constructor.
     * @param Role $role
     */
    public function __construct(Role $role, PermissionInterface $permission)
    {
        $this->role = $role;
        $this->permission = $permission;
        // $this->user = $user;
    }

    public function getAll($keyword = null)
    {
        return $this->role->where(function($query) use ($keyword){

            if(!empty($keyword)){
                $query->where(function($q) use ($keyword)
                {
                    $q->where('name', 'LIKE', '%'. $keyword . '%');
                    $q->orWhere('display_name', 'LIKE', '%'. $keyword . '%');
                    $q->orWhere('description', 'LIKE', '%'. $keyword .'%');
                });
            }
        })
            ->orderBy('id')
            ->paginate(10);
    }

    public function find($id)
    {
        return $this->role->with('permissions')->findOrFail($id);
    }

    public function create($request)
    {
        $role = $this->role;
        $role->name = $request->get('name');
        $role->display_name = $request->get('display_name');
        $role->description = $request->get('description');
        $role->save();
        $role->permissions()->attach($request->get('permissions'));
        return $role;
    }

    public function update($id, $request)
    {
        $role = $this->role->find($id);

        $role->name = $request->get('name');

        $role->display_name = $request->get('display_name');

        $role->description = $request->get('description');

        $role->save();

        $role->permissions()->sync($request->get('permissions'));

        return $role;
    }

    public function destoryrole($id)
    {
        $data = $this->role::findOrFail($id);
        $data->delete();
        return $data;
    }

    public function getRolePermissionIds($role)
    {
        $rolePermissionIds = [];

        foreach ($role->permissions as $permission) {
            $rolePermissionIds[] = $permission->id;
        }

        return $rolePermissionIds;
    }

    public function getList()
    {
        return $this->role->pluck('display_name', 'id');
    }

    public function getCount()
    {
        return $this->role->count();
    }

    public function getRoleIdByName($name)
    {
        return $this->role->where('name',$name)->pluck('id')->first();
    }


}
