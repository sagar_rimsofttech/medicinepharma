<?php

namespace App\MedicinePharma\Repositories\User;

use App\Admin;
use App\Notifications\MailMeeting;
use App\Notifications\RegisterFromPortalAdmin;
use App\MedicinePharma\Repositories\User\UserInterface;
use App\User;
use Illuminate\Support\Facades\DB;
use App\MedicinePharma\Repositories\Permission\PermissionInterface;
use App\Models\UserAddress;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserRepository implements UserInterface
{
    /**
     * @var Role
     */
    private $role;
    private $permission;
    private $user;
    /**
     * RoleRepository constructor.
     * @param Role $role
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    // public function getAll($keyword = null)
    // {
    //     return $this->role->where(function($query) use ($keyword){

    //         if(!empty($keyword)){
    //             $query->where(function($q) use ($keyword)
    //             {
    //                 $q->where('name', 'LIKE', '%'. $keyword . '%');
    //                 $q->orWhere('display_name', 'LIKE', '%'. $keyword . '%');
    //                 $q->orWhere('description', 'LIKE', '%'. $keyword .'%');
    //             });
    //         }
    //     })
    //         ->orderBy('id')
    //         ->paginate(10);
    // }

    // public function find($id)
    // {
    //     return $this->role->with('permissions')->findOrFail($id);
    // }
    public function find($id)
    {
        return  $user = $this->user->select('name', 'email', 'id')->find($id);
    }
    public function create($request)
    {
        $password = $request->has('password') ? $request->password : 'password';
        $user = $this->user;
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->mobile_no = $request->get('number') ? $request->get('number') : null;
        $user->password = Hash::make($password);
        $user->save();
        //  $user->roles()->attach($request->get('role'));
        //  $user->notify(new RegisterFromPortalAdmin);
        return $user;
    }

    public function editUser($id)
    {
        $user = $this->user->with(['roles' => function ($query) {
            $query->select('role_id');
        }])->findorFail($id);
        return  $user;
    }

    public function update($id, $request)
    {
        $user = $this->user->find($id);
        $user->name = $request->get('username');
        $user->email = $request->get('email');
        $user->save();
        $user->roles()->sync($request->get('role'));
        return $user;
    }

    public function updatepassword($request)
    {
        $hashedPassword = Auth::user()->password;
        if (Hash::check($request->old_password, $hashedPassword)) {
            if (!Hash::check($request->password, $hashedPassword)) {
                $user = User::find(Auth::user()->id);
                $user->password = Hash::make($request->password);
                $user->save();
                // return $user;
                Auth::logout();
                // Toastr::success('password chnaged successfully');
                // return redirect()->back();
                return $user;
                // abort(400,)
            } else {
                abort(400, 'New password cannot be the same as old password');
                // Toastr::warning('New password cannot be the same as old password');
                // return redirect()->back();
            }
        } else {
            abort(400, 'Current password not match');
            // Toastr::warning('Current password not match');
            // return redirect()->back();
        }
    }

    public function deleteUser($id)
    {
        $user = $this->user->find($id);

        $user->delete();
    }
    public function storeaddress($request, $createUser = null)
    {
        // dd($request->all());
        if (!empty($createUser->id)) {
            $user_id = $createUser->id;
        } else {
            $user_id = Auth::user()->id;
        }
        $saveaddress = new UserAddress();
        $saveaddress->user_id = !empty($user_id) ? $user_id : '';
        $saveaddress->name = $request->name;
        $saveaddress->number = $request->number;
        $saveaddress->pincode = $request->pincode;
        $saveaddress->locality = $request->locality ?? null;
        $saveaddress->address = $request->address;
        $saveaddress->city = $request->city;
        $saveaddress->state_id = $request->state;
        $saveaddress->landmark = $request->landmark ?? null;
        $saveaddress->alternatenumber = $request->alternatenumber ?? null;
        // dd($saveaddress);
        $saveaddress->save();
        return $saveaddress;
    }

    public function checkuser($email)
    {
        $user = User::where('email', $email)->first();
        if (!empty($user)) {
            return $user;
            //    return view('frontend.login_form', compact('user'));
        }
        return null;
    }
}
