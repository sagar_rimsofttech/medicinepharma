<?php

namespace App\MedicinePharma\Repositories\Admin;

interface AdminInterface
{
    // public function getAll();

    public function editUser($id);

    public function create($request);

    public function update($id, $request);
    public function find($id);

    public function deleteUser($id);

    public function updatepassword($request);
    public function storeaddress($request,$storeaddress=null);

    // public function getRolePermissionIds($id);

    // public function getList();

    // public function getCount();

    // public function getRoleIdByName($name);

    // public function getSuperAdminList();

    // public function getAgentRoles($roleName);
}
