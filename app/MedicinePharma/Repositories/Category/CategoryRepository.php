<?php

namespace App\MedicinePharma\Repositories\Category;

use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Support\Facades\Storage;

class CategoryRepository implements CategoryInterface
{
    /**
     * @var Category
     */
    private $category;
    /**
     * CategoryRepository constructor.
     * @param Category $role
     */
    public function __construct(Category $category){
        $this->category = $category;
    }

    public function getAll($keyword = null)
    {
        $category = $this->category::get();
        return $category;
    }

    public function store($request)
    {
        $image = $request->file('image');
        $slug = str_slug($request->name);
        if (isset($image))
        {
//            make unique name for image
            $currentDate = Carbon::now()->toDateString();
            $imagename = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
//            check category dir is exists
            if (!Storage::disk('public')->exists('/uploads/category/thumbnails'))
            {
                Storage::disk('public')->makeDirectory('/uploads/category/thumbnails');
            }
            if (!Storage::disk('public')->exists('/uploads/category/normal'))
            {
                Storage::disk('public')->makeDirectory('/uploads/category/normal');
            }
//            resize image for category and upload
            $categoryImageThumb = Image::make($image)->resize(500,500)->stream();
            Storage::disk('public')->put('uploads/category/thumbnails/'.$imagename,$categoryImageThumb);
            $categoryImageNormal = Image::make($image)->stream();
            Storage::disk('public')->put('uploads/category/normal/'.$imagename,$categoryImageNormal);
        } else {
            $imagename = "default.png";
        }

        $category = $this->category;
        $category->name = $request->name;
        $category->description = $request->description;
        $category->image = $imagename;
        $category->save();
        return $category;

    }

    public function find($id)
    {
        return $category = $this->category::findOrFail($id);
    }

    public function update($request, $id)
    {
        $image = $request->file('image');
        $slug = str_slug($request->name);
        $category = $this->category->findOrFail($id);
        if (isset($image))
        {
//            make unique name for image
            $currentDate = Carbon::now()->toDateString();
            $imagename = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
//            check category dir is exists
            if (!Storage::disk('public')->exists('/uploads/category/thumbnails'))
            {
                Storage::disk('public')->makeDirectory('/uploads/category/thumbnails');
            }
            if (!Storage::disk('public')->exists('/uploads/category/normal'))
            {
                Storage::disk('public')->makeDirectory('/uploads/category/normal');
            }
//            resize image for category and upload
            $categoryImageThumb = Image::make($image)->resize(500,500)->stream();
            Storage::disk('public')->put('uploads/category/thumbnails/'.$imagename,$categoryImageThumb);
            $categoryImageNormal = Image::make($image)->stream();
            Storage::disk('public')->put('uploads/category/normal/'.$imagename,$categoryImageNormal);
        } else {
            $imagename = $category->image;
        }


        $category->name = $request->name;
        $category->description = $request->description;
        $category->image = $imagename;
        $category->save();
        return $category;
    }

    public function delete($id)
    {
        $category = $this->category->findorFail($id);

        $category->delete();
        return $category;
    }

    public function getList()
    {
        return $this->category->pluck('name', 'id');
    }

    public function searchCategory($keyword = null)
    {
        return  $countries_name = $this->category::where('name', 'like', '%'.$keyword.'%')->select('id', 'name')->limit(10)->get();
    }
}
