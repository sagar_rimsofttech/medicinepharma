<?php

namespace App\MedicinePharma\Repositories\Category;

interface CategoryInterface
{
    public function getAll();
    public function store($request);
    public function find($id);
    public function update($request, $id);
    public function delete($id);
    public function getList();
    public function searchCategory($keyword = null);
}
