<?php

namespace App\MedicinePharma\Repositories\SearchData;

interface SearchDataInterface
{
    public function getAll();
}
