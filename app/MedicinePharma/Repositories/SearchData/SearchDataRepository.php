<?php

namespace App\MedicinePharma\Repositories\SearchData;

use App\Models\Brand;
use App\Models\SearchKeyword;
use Illuminate\Support\Facades\DB;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Support\Facades\Storage;

class SearchDataRepository implements SearchDataInterface
{
    /**
     * @var Category
     */
    private $searchdata;
    /**
     * CategoryRepository constructor.
     * @param Category $role
     */
    public function __construct(SearchKeyword $searchdata){
        $this->searchdata = $searchdata;
    }

    public function getAll($keyword = null)
    {
        $searchdata = $this->searchdata::get();
        return $searchdata;
    }


}
