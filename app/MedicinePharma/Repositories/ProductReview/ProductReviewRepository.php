<?php

namespace App\MedicinePharma\Repositories\ProductReview;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductReview;
use App\Models\SubCategory;
use Illuminate\Support\Facades\DB;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;

class ProductReviewRepository implements ProductReviewInterface
{
    /**
     * @var Product
     */
    private $productreview;
    private $product;
    private $brand;
    /**
     * CategoryRepository constructor.
     * @param SubCategory $role
     */
    public function __construct(ProductReview $productreview,Product $product,Brand $brand){
        $this->productreview = $productreview;
        $this->product = $product;
        $this->brand = $brand;
    }

    public function getAll($keyword = null)
    {
        $productreview = $this->productreview->latest()->get();
        return $productreview;
    }

    public function store($request)
    {
      
        $images = $request->file('image');
        $slug = str_slug($request->name);
        if (isset($images))
        {
            $imagenamesave =[];
            foreach($images as $image)
            {
//            make unique name for image
            $currentDate = Carbon::now()->toDateString();
            $imagename = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
//            check subcategory dir is exists
            if (!Storage::disk('public')->exists('/uploads/productreview/thumbnails'))
            {
                Storage::disk('public')->makeDirectory('/uploads/productreview/thumbnails');
            }
            if (!Storage::disk('public')->exists('/uploads/productreview/normal'))
            {
                Storage::disk('public')->makeDirectory('/uploads/productreview/normal');
            }
//            resize image for product and upload
            $productReviewImageThumb = Image::make($image)->resize(500,500)->stream();
            Storage::disk('public')->put('uploads/productreview/thumbnails/'.$imagename,$productReviewImageThumb);
            $productReviewImageNormal = Image::make($image)->stream();
            Storage::disk('public')->put('uploads/productreview/normal/'.$imagename,$productReviewImageNormal);
            $imagenamesave[] = $imagename;
        }
        } else {
            $imagenamesave[] = "";
        }
        $productreview = $this->productreview;
        $productreview->name = $request->name;
        $productreview->product_id = $request->product_id;
        $productreview->customer_id = 0;
        $productreview->slug = $slug;
        if ($request->has('rating')) {
            $productreview->rating = !empty($request->get('rating')) ? $request->get('rating') : 0;
        }
       
        $productreview->image = implode(',',$imagenamesave);
        $productreview->save();
        return $productreview;

    }
    public function find($id)
    {
        $productreview = $this->productreview::findOrFail($id);
        $products_name = Product::where('id',$productreview->product_id)->select('name')->first();
        $productreview->product_name = $products_name->name;
        return $productreview;
    }
   

    public function update($request, $id)
    {
        $images = $request->file('image');
        $slug = str_slug($request->name);
        $productreview = $this->productreview->findOrFail($id);
        if (isset($images))
        {
            foreach(explode(",",$productreview->image) as $image)
            {
                if (Storage::disk('public')->exists('/uploads/productreview/thumbnails'))
                    {
                        $productImage = URL::asset("storage/uploads/productreview/thumbnails/".$image);
                        File::delete($productImage);
                    }
                if (Storage::disk('public')->exists('/uploads/productreview/normal'))
                    {
                        $productImage = URL::asset("storage/uploads/productreview/normal/".$image);
                        File::delete($productImage);
                    }
            }
            foreach($images as $image)

            {
//            make unique name for image
            $currentDate = Carbon::now()->toDateString();
            $imagename = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
//            check product dir is exists
            if (!Storage::disk('public')->exists('/uploads/productreview/thumbnails'))
            {
                Storage::disk('public')->makeDirectory('/uploads/productreview/thumbnails');
            }
            if (!Storage::disk('public')->exists('/uploads/productreview/normal'))
            {
                Storage::disk('public')->makeDirectory('/uploads/productreview/normal');
            }
//            resize image for product and upload
            $productImageThumb = Image::make($image)->resize(500,500)->stream();
            Storage::disk('public')->put('uploads/productreview/thumbnails/'.$imagename,$productImageThumb);
            $productImageNormal = Image::make($image)->stream();
            Storage::disk('public')->put('uploads/productreview/normal/'.$imagename,$productImageNormal);
            $imagenamesave[] = $imagename;
        }
        } else {
            $imagenamesave[] = $productreview->image;
        }
      
        $productreview->name = $request->name;
        $productreview->product_id = $request->product_id;
        $productreview->customer_id = 0;
        $productreview->slug = $slug;
        if ($request->has('rating')) {
            $productreview->rating = !empty($request->get('rating')) ? $request->get('rating') : 0;
        }
       
        $productreview->image = implode(',',$imagenamesave);
        $productreview->save();
        return $productreview;
        
    }

    public function delete($id)
    {
        $productreview = $this->productreview->findorFail($id);
        $productreview->delete();
        return $productreview;
    }
    public function searchProduct($keyword = null)
    {
        return  $product_name = $this->product::where('name', 'like', '%'.$keyword.'%')->select('id', 'name')->limit(10)->get();
    }
}
