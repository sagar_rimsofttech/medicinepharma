<?php

namespace App\MedicinePharma\Repositories\ProductReview;

interface ProductReviewInterface
{
    public function getAll();
    public function store($request);
    public function searchProduct($keyword = null);
    public function update($request, $id);
    public function delete($id);
    public function find($id);
   
}
