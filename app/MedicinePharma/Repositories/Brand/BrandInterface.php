<?php

namespace App\MedicinePharma\Repositories\Brand;

interface BrandInterface
{
    public function getAll();
    public function store($request);
    public function find($id);
    public function update($request, $id);
    public function delete($id);
    public function getList();
    public function searchBrand($keyword = null);
}
