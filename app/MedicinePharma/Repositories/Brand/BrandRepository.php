<?php

namespace App\MedicinePharma\Repositories\Brand;

use App\Models\Brand;
use Illuminate\Support\Facades\DB;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Support\Facades\Storage;

class BrandRepository implements BrandInterface
{
    /**
     * @var Category
     */
    private $brand;
    /**
     * CategoryRepository constructor.
     * @param Category $role
     */
    public function __construct(Brand $brand){
        $this->brand = $brand;
    }

    public function getAll($keyword = null)
    {
        $brand = $this->brand::get();
        return $brand;
    }

    public function store($request)
    {
        
        $slug = str_slug($request->name);
        $brand = $this->brand;
        $brand->name = $request->name;
        $brand->description = $request->description;
        $brand->slug = $slug;
        if ($request->has('is_active')) {
            $brand->is_active = !empty($request->get('is_active')) ? $request->get('is_active') : 0;
        }
        $brand->save();
        return $brand;

    }

    public function find($id)
    {
        return $brand = $this->brand::findOrFail($id);
    }

    public function update($request, $id)
    {
       
        $slug = str_slug($request->name);
        $brand = $this->brand->findOrFail($id);
        
        $brand->name = $request->name;
        $brand->description = $request->description;
        $brand->is_active = $request->is_active;
        $brand->save();
        return $brand;       
        ;
    }

    public function delete($id)
    {
        $brand = $this->brand->findorFail($id);

        $brand->delete();
        return $brand;
    }

    public function getList()
    {
        return $this->brand->pluck('name', 'id');
    }

    public function searchBrand($keyword = null)
    {
        return  $brand_name = $this->brand::where('name', 'like', '%'.$keyword.'%')->select('id', 'name')->limit(10)->get();
    }
}
