<?php

namespace App\MedicinePharma\Repositories\Prescription;

interface PrescriptionInterface
{
    public function getAll();
    public function searchProduct($keyword = null);
}
