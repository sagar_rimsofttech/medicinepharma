<?php

namespace App\MedicinePharma\Repositories\Prescription;

use App\Models\Order;
use App\Models\Prescription;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class PrescriptionRepository implements PrescriptionInterface
{
    /**
     * @var Product
     */
    private $prescription;
    private $order;
    private $brand;
    /**
     * CategoryRepository constructor.
     * @param SubCategory $role
     */
    public function __construct(Prescription $prescription,Order $order){
        $this->prescription = $prescription;
        $this->order = $order;
    }

    public function getAll()
    {
        $getprescription = $this->prescription::whereNull('order_id')->latest()->get();
        return $getprescription;
    }

    public function searchProduct($keyword = null)
    {
        return  $product_name =Product::where('name', 'like', '%'.$keyword.'%')->select('id', 'name')->limit(20)->get();
    }

}
