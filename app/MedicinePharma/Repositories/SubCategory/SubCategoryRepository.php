<?php

namespace App\MedicinePharma\Repositories\SubCategory;

use App\Models\SubCategory;
use Illuminate\Support\Facades\DB;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Support\Facades\Storage;

class SubCategoryRepository implements SubCategoryInterface
{
    /**
     * @var SubCategory
     */
    private $subcategory;
    /**
     * CategoryRepository constructor.
     * @param SubCategory $role
     */
    public function __construct(SubCategory $subcategory){
        $this->subcategory = $subcategory;
    }

    public function getAll($keyword = null)
    {
        $subcategory = $this->subcategory::get();
        return $subcategory;
    }

    public function store($request)
    {
        $image = $request->file('image');
        $slug = str_slug($request->name);
        if (isset($image))
        {
//            make unique name for image
            $currentDate = Carbon::now()->toDateString();
            $imagename = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
//            check subcategory dir is exists
            if (!Storage::disk('public')->exists('/uploads/subcategory/thumbnails'))
            {
                Storage::disk('public')->makeDirectory('/uploads/subcategory/thumbnails');
            }
            if (!Storage::disk('public')->exists('/uploads/subcategory/normal'))
            {
                Storage::disk('public')->makeDirectory('/uploads/subcategory/normal');
            }
//            resize image for subcategory and upload
            $subcategoryImageThumb = Image::make($image)->resize(500,500)->stream();
            Storage::disk('public')->put('uploads/subcategory/thumbnails/'.$imagename,$subcategoryImageThumb);
            $subcategoryImageNormal = Image::make($image)->stream();
            Storage::disk('public')->put('uploads/subcategory/normal/'.$imagename,$subcategoryImageNormal);
        } else {
            $imagename = "default.png";
        }

        $subcategory = $this->subcategory;
        $subcategory->name = $request->name;
        $subcategory->category_id = $request->categories;
        $subcategory->description = $request->description;
        $subcategory->image = $imagename;
        $subcategory->save();
        return $subcategory;

    }

    public function find($id)
    {
        return $subcategory = $this->subcategory::findOrFail($id);
    }

    public function update($request, $id)
    {
        $image = $request->file('image');
        $slug = str_slug($request->name);
        $subcategory = $this->subcategory->findOrFail($id);
        if (isset($image))
        {
//            make unique name for image
            $currentDate = Carbon::now()->toDateString();
            $imagename = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
//            check subcategory dir is exists
            if (!Storage::disk('public')->exists('/uploads/subcategory/thumbnails'))
            {
                Storage::disk('public')->makeDirectory('/uploads/subcategory/thumbnails');
            }
            if (!Storage::disk('public')->exists('/uploads/subcategory/normal'))
            {
                Storage::disk('public')->makeDirectory('/uploads/subcategory/normal');
            }
//            resize image for subcategory and upload
            $subcategoryImageThumb = Image::make($image)->resize(500,500)->stream();
            Storage::disk('public')->put('uploads/subcategory/thumbnails/'.$imagename,$subcategoryImageThumb);
            $subcategoryImageNormal = Image::make($image)->stream();
            Storage::disk('public')->put('uploads/subcategory/normal/'.$imagename,$subcategoryImageNormal);
        } else {
            $imagename = $subcategory->image;
        }


        $subcategory->name = $request->name;
        $subcategory->category_id = $request->categories;
        $subcategory->description = $request->description;
        $subcategory->image = $imagename;
        $subcategory->save();
        return $subcategory;
    }

    public function delete($id)
    {
        $subcategory = $this->subcategory->findorFail($id);

        $subcategory->delete();
        return $subcategory;
    }
    public function getList()
    {
        return $this->subcategory->pluck('name', 'id');
    }

    public function searchSubCategory($keyword = null,$request)
    {
        return  $states_name = $this->subcategory::where('category_id',$request->category_id)->where('name', 'like', '%'.$request->q.'%')->select('id', 'name')->get();
    }
}
