<?php

namespace App\MedicinePharma\Repositories\Order;

use App\Models\Brand;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Order;
use App\Models\Prescription;
use App\Models\Product;
use App\Models\SubCategory;
use App\Notifications\OrderNotification;
use App\User;
use Illuminate\Support\Facades\DB;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class OrderRepository implements OrderInterface
{
    /**
     * @var Product
     */
    private $subcategory;
    private $product;
    private $brand;
    private $order;
    /**
     * CategoryRepository constructor.
     * @param SubCategory $role
     */
    public function __construct(SubCategory $subcategory, Product $product, Brand $brand, Order $order)
    {
        $this->subcategory = $subcategory;
        $this->product = $product;
        $this->brand = $brand;
        $this->order = $order;
    }

    public function getAll($keyword = null)
    {
        $order = $this->order::latest()->get();
        return $order;
    }

    public function storeOrder($request, $createUser = null, $saveaddress = null)
    {
        if ($request->address_id) {
            $address = $request->address_id;
        } else {
            $address = $saveaddress->id;
        }
        if (!empty($createUser->id)) {
            $user_id =  $createUser->id;
        } else {
            $user_id = Auth::user()->id;
        }

        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $order = new Order();
        $order->cart = serialize($cart);
        $order->user_id = !empty($user_id) ? $user_id : '000';
        $order->order_date = now();
        $order->amount = $cart->totalPrice;
        $order->user_delivery_address = !empty($address) ? $address : null;
        $order->delivery_note = !empty($request->delivery_note) ? $request->delivery_note : null;
        $order->payment_method = !empty($request->payment_type) ? $request->payment_type : 2;
        $order->save();
        $user = User::find($user_id);
        // $via = 'sms';
        // $user->notify(new OrderNotification('sms'));
        // $user->notify(new OrderNotification('mail'));
        return $order;
    }

    public function storePrescription($request, $createUser = null, $saveaddress = null)
    {

        if (!empty(Auth::user())) {
            $user_id =  Auth::user()->id;
            $mobile_no = Auth::user()->mobile_no;
        } else {
            $user_id = null;
            $mobile_no = $request->number;
        }
        $data = new Prescription();
        if ($files = $request->file('prescription')) {
            $files = $request->file('prescription');
            $currentDate = Carbon::now()->toDateString();
            $name = $currentDate . '-' . uniqid() . '.' . $files->getClientOriginalExtension();
            // $name=$files->getClientOriginalName();
            if (!Storage::disk('public')->exists('/uploads/prescription')) {
                Storage::disk('public')->makeDirectory('/uploads/prescription');
            }
            // $file->move($path,$fileName);
            $files->move('storage/uploads/prescription', $name);
            $data->file_name = $name;
        }
        $data->user_id = $user_id ?? null;
        $data->address_id = null;
        $data->order_notes = !empty($request->delivery_note) ? $request->delivery_note : null;
        $data->mobile_no = !empty($mobile_no) ? $mobile_no : null;
        $data->prescription_date = now();
        $data->order_id = null;
        $data->save();
        return $data;
    }

    public function find($id)
    {
        $getorder = $this->order->find($id);
        return $getorder;
    }
}
