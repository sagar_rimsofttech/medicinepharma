<?php

namespace App\MedicinePharma\Repositories\Order;

interface OrderInterface
{
    public function storeOrder($request,$craeteUser=null,$saveaddress=null);
    public function storePrescription($request,$craeteUser=null,$saveaddress=null);
    public function getAll();
    public function find($id);
}
