<?php

namespace App\MedicinePharma\Helpers;

use Intervention\Image\ImageManager;

class Image
{
    private $image;

    public function __construct(ImageManager $image)
    {
        $this->image = $image;
    }

    public function resizeImage($file, $width, $height)
    {
        $image = $this->image->make($file)->resize($width, $height);

        return $image;
    }
}
