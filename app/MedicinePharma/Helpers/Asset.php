<?php

namespace App\MedicinePharma\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;
use App\MedicinePharma\Helpers\Image;


class Asset
{
    private $image;

    public function __construct(Image $image)
    {
        $this->image = $image;
    }


    public static function getImagePath($imagePath, $imageDimensions = [])
    {

        $baseUrl = config('constants.upload_url');

        if(!empty($imagePath) && !str_contains($imagePath, 'http')) {
            $pathInfo = pathinfo($imagePath);
            $path = $pathInfo['dirname'] . '/' . $pathInfo['filename'];

            if(!empty($imageDimensions) && is_array($imageDimensions)){

                @$fullPath = $baseUrl . $path . '-' . $imageDimensions['width'] . 'x' . $imageDimensions['height'] . '.' . $pathInfo['extension'];
            }else{
                @$fullPath = $baseUrl . $path. '.' . $pathInfo['extension'];
            }

            $imagePath = $fullPath;
        }
        return $imagePath;
    }


}
