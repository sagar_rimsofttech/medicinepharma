<?php

namespace App\Providers;

use App\MedicinePharma\Repositories\Brand\BrandInterface;
use App\MedicinePharma\Repositories\Brand\BrandRepository;
use Illuminate\Support\ServiceProvider;
use App\MedicinePharma\Repositories\Role\RoleRepository;
use App\MedicinePharma\Repositories\Role\RoleInterface;
use App\MedicinePharma\Repositories\Permission\PermissionInterface;
use App\MedicinePharma\Repositories\Permission\PermissionRepository;
use App\MedicinePharma\Repositories\User\UserInterface;
use App\MedicinePharma\Repositories\User\UserRepository;
use App\MedicinePharma\Repositories\Category\CategoryInterface;
use App\MedicinePharma\Repositories\Category\CategoryRepository;
use App\MedicinePharma\Repositories\Category\CategotyRepository;
use App\MedicinePharma\Repositories\SubCategory\SubCategoryRepository;
use App\MedicinePharma\Repositories\SubCategory\SubCategoryInterface;
use App\MedicinePharma\Repositories\Product\ProductRepository;
use App\MedicinePharma\Repositories\Product\ProductInterface;
use App\MedicinePharma\Repositories\ProductReview\ProductReviewInterface;
use App\MedicinePharma\Repositories\ProductReview\ProductReviewRepository;
use App\MedicinePharma\Repositories\Order\OrderInterface;
use App\MedicinePharma\Repositories\Order\OrderRepository;
use App\MedicinePharma\Repositories\Admin\AdminInterface;
use App\MedicinePharma\Repositories\Admin\AdminRepository;
use App\MedicinePharma\Repositories\Prescription\PrescriptionInterface;
use App\MedicinePharma\Repositories\Prescription\PrescriptionRepository;
use App\MedicinePharma\Repositories\SearchData\SearchDataInterface;
use App\MedicinePharma\Repositories\SearchData\SearchDataRepository;

/*********************************** */


class RepositoryServiceProvider extends ServiceProvider
{
    protected $defer = true;

    public function boot()
    {
        //
    }

    public function register()
    {
        $this->app->bind(RoleInterface::class, RoleRepository::class);
        $this->app->bind(PermissionInterface::class, PermissionRepository::class);
        $this->app->bind(UserInterface::class, UserRepository::class);
        $this->app->bind(CategoryInterface::class, CategoryRepository::class);
        $this->app->bind(SubCategoryInterface::class, SubCategoryRepository::class);
        $this->app->bind(ProductInterface::class, ProductRepository::class);
        $this->app->bind(BrandInterface::class, BrandRepository::class);
        $this->app->bind(ProductReviewInterface::class, ProductReviewRepository::class);
        $this->app->bind(OrderInterface::class, OrderRepository::class);
        $this->app->bind(AdminInterface::class, AdminRepository::class);
        $this->app->bind(PrescriptionInterface::class, PrescriptionRepository::class);
        $this->app->bind(SearchDataInterface::class, SearchDataRepository::class);
    }

    public function provides()
    {
        return [
            RoleInterface::class,
            PermissionInterface::class,
            UserInterface::class,
            CategoryInterface::class,
            SubCategoryInterface::class,
            ProductInterface::class,
            BrandInterface::class,
            ProductReviewInterface::class,
            OrderInterface::class,
            AdminInterface::class,
            PrescriptionInterface::class,
            SearchDataInterface::class,

        ];
    }
}
