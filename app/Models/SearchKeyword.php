<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class SearchKeyword extends Model
{
    protected $table = 'search_data';

    public static function boot()
    {
        parent::boot();
    }
    public function getCreateInfoAttribute()
    {
       return $createInfo = "".' ' .$this->created_at->diffForHumans().' ';
    }
    public function getUpdateInfoAttribute()
    {
       return $updateInfo = "".' ' .$this->updated_at->diffForHumans().' ';
    }

    public function getUserDataAttribute()
    {
        return $userdata = "".' ' .User::where('id', $this->user_id)->pluck('name')->first().' ';
    }
}
