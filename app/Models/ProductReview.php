<?php

namespace App\Models;

use App\Admin;
use App\User;
use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }
    public function getCreateInfoAttribute()
    {
       return $createInfo = "".' ' .$this->created_at->diffForHumans().' '.'By'.' '.Admin::where('id', $this->created_by)->pluck('name')->first();
    }
    public function getUpdateInfoAttribute()
    {
       return $updateInfo = "".' ' .$this->updated_at->diffForHumans().' '.'By'.' '.Admin::where('id', $this->updated_by)->pluck('name')->first();
    }
}
