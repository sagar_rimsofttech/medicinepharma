<?php

namespace App\Models;

use App\Admin;
use App\MedicinePharma\Helpers\Asset;
use App\Traits\CategoryTrait;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class Category extends BaseModel
{

    use CategoryTrait;
    protected $table = 'categories';

    public static function boot()
    {
        parent::boot();
    }

    public function getCreateInfoAttribute()
    {
       return $createInfo = "".' ' .$this->created_at->diffForHumans().' '.'By'.' '.Admin::where('id', $this->created_by)->pluck('name')->first();
    }
    public function getUpdateInfoAttribute()
    {
       return $updateInfo = "".' ' .$this->updated_at->diffForHumans().' '.'By'.' '.Admin::where('id', $this->updated_by)->pluck('name')->first();
    }
    public function getImageAttribute($value)
    {
        return URL::asset('storage/uploads/').'/category/thumbnails/'.$value;
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product','category_id','id');
    }

}
