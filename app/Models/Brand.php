<?php

namespace App\Models;

use App\Admin;
use App\Traits\BrandTrait;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use BrandTrait;
    protected $table = 'brands';

    public static function boot()
    {
        parent::boot();
    }

    public function getCreateInfoAttribute()
    {
       return $createInfo = "".' ' .$this->created_at->diffForHumans().' '.'By'.' '.Admin::where('id', $this->created_by)->pluck('name')->first();
    }
    public function getUpdateInfoAttribute()
    {
       return $updateInfo = "".' ' .$this->updated_at->diffForHumans().' '.'By'.' '.Admin::where('id', $this->updated_by)->pluck('name')->first();
    }
}
