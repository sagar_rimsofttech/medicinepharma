<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function useraddress()
    {
        return $this->belongsTo(UserAddress::class,'user_delivery_address','id');
    }

    public function getAddressAttribute()
    {
        $address_show = '';
        if(!empty($this->useraddress))
        {
            $address_name = !empty($this->useraddress->name)?$this->useraddress->name:'';
            $address_number = !empty($this->useraddress->number)?$this->useraddress->number:'';
            $address_alternatenumber = !empty($this->useraddress->alternatenumber)?$this->useraddress->alternatenumber:'';
            $address_address = !empty($this->useraddress->address)?$this->useraddress->address:'';
            $address_locality = !empty($this->useraddress->locality)?$this->useraddress->locality:'';
            $address_pincode = !empty($this->useraddress->pincode)?$this->useraddress->pincode:'';
            $address_city = !empty($this->useraddress->city)?$this->useraddress->city:'';
            $address_landmark = !empty($this->useraddress->landmark)?$this->useraddress->landmark:'';



        $address_show = '<p>'.$address_name.' | '.$address_number.'( '.$address_alternatenumber.' )'.'</p><br><p>'.$address_address.','.$address_locality.','.$address_pincode.$address_city.','.$address_landmark.'</p>';
        }
        return $address_show;
    }

}
