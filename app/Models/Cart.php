<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart
{
    public $items = null;
    public $totalQty = 0;
    public $totalPrice = 0;

    public function __construct($oldCart)
    {
        if ($oldCart) {
            $this->items = $oldCart->items;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;
        }
    }

    public function add($item, $quantity, $id)
    {
        // $storedItem = ['qty' => $quantity, 'price' => $item->price, 'item' => $item];
        // if($this->items)
        // {
        //     if(array_key_exists($id, $this->items))
        //     {
        //         $storedItem = $this->items[$id];
        //     }
        // }
        // $storedItem['qty'] = $quantity;
        // $storedItem['price'] = $item->price * $storedItem['qty'];
        // $this->items[$id] = $storedItem;
        // $this->totalQty+=$quantity;
        // $this->totalPrice += $item->price;

        $storedItem = ['qty' => 0, 'price' => $item->discounted_price, 'item' => $item];
        if ($this->items) {
            if (array_key_exists($id, $this->items)) {
                $storedItem = $this->items[$id];
            }
        }
        $storedItem['qty'] += $quantity;
        $storedItem['price'] = $item->discounted_price * $storedItem['qty'];
        $this->items[$id] = $storedItem;
        $this->totalQty += $quantity;
        $this->totalPrice += $item->discounted_price * $quantity;
    }

    public function reduceByOne($id)
    {
        $this->items[$id]['qty']--;
        $this->items[$id]['price'] -= $this->items[$id]['item']['discounted_price'];
        $this->totalQty--;
        $this->totalPrice -= $this->items[$id]['item']['discounted_price'];

        if ($this->items[$id]['qty'] <= 0) {
            unset($this->items[$id]);
        }
    }

    public function removeProductFromCart($id)
    {
        if (array_key_exists($id, $this->items)) {
            $storedItem =   $this->items[$id];
            $this->totalQty -= $storedItem['qty'];
            $this->totalPrice -= $storedItem['price'];
            unset($this->items[$id]);
        }
    }
}
