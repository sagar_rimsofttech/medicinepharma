<?php

namespace App\Models;

use App\Admin;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{

    public function getCreateInfoAttribute()
    {
       return $createInfo = "".' ' .$this->created_at->diffForHumans().' '.'By'.' '.Admin::where('id', $this->created_by)->pluck('name')->first();
    }
    public function getUpdateInfoAttribute()
    {
       return $updateInfo = "".' ' .$this->updated_at->diffForHumans().' '.'By'.' '.Admin::where('id', $this->updated_by)->pluck('name')->first();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
