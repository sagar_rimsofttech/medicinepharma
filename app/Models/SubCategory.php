<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends BaseModel
{
    public static function boot()
    {
        parent::boot();
    }

    public function getCreateInfoAttribute()
    {
       return $createInfo = "".' ' .$this->created_at->diffForHumans().' '.'By'.' '.User::where('id', $this->created_by)->pluck('name')->first();
    }
    public function getUpdateInfoAttribute()
    {
       return $updateInfo = "".' ' .$this->updated_at->diffForHumans().' '.'By'.' '.User::where('id', $this->updated_by)->pluck('name')->first();
    }

    public function category()
    {
        return $this->belongsTo(Category::class,'category_id','id');
    }
}
