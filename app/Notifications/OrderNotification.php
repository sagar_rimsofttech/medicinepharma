<?php

namespace App\Notifications;

use Bitfumes\KarixNotificationChannel\KarixChannel;
use Bitfumes\KarixNotificationChannel\KarixMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OrderNotification extends Notification
{
    use Queueable;
    public $via;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($via)
    {
        $this->via = $via;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->via == 'sms' ? [KarixChannel::class] : ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toKarix($notifiable)
    {
        $mobile_no = '+91' . $notifiable->mobile_no;
        $order_details = $notifiable->orders()->latest()->first();
        $cart = unserialize($order_details->cart);
        foreach ($cart->items as $product) {
            $product = $product;
        }
        $order_product_name = str_limit($product['item']['name'], 6);
        $count = count($cart->items);
        return KarixMessage::create()
            ->from('Med2MyHome')
            ->to($mobile_no)
            ->content('Your Order (' . $order_details->order_code . ') placed sucessfully ' . $order_product_name . '' . '&' . $count . 'is placed with us, visit my order to view Order details');
    }

    public function toMail($notifiable)
    {
        $order =  $notifiable->orders()->latest()->first();
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!')
            ->view('admin.emails/orders/orderConfirmation', compact('notifiable', 'order'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
