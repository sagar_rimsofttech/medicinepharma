<?php

namespace App\Traits;

use App\Models\SubCategory;

trait SubCategoryTrait {

    // private $category;

    // public function __construct(Category $category){
    //     $this->category = $category;
    // }
    // public function restoreCategoryTrait($id)
    // {

    //     $restore = Category::withTrashed()->find($id->id)->restore();
    //     if(count($restore) > 0)
    //     {

    //     }
    // }
    public function checkfordeletedsubcategory($name)
    {
        $restore =  SubCategory::withTrashed()->where('name','=',$name)->whereNotNull('deleted_at')->first();
        if(count($restore)>0){
            return $restore;
        } else {
            return '{"name":"No data"}';
        }
    }
    public function restoredeletedsubcategory($name)
    {
        $restore = SubCategory::withTrashed()->where('name','=',$name)->restore();
        return $restore;
    }

}
