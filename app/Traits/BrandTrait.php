<?php

namespace App\Traits;

use App\Models\Brand;


trait BrandTrait {

    // private $category;

    // public function __construct(Category $category){
    //     $this->category = $category;
    // }
    // public function restoreCategoryTrait($id)
    // {

    //     $restore = Category::withTrashed()->find($id->id)->restore();
    //     if(count($restore) > 0)
    //     {

    //     }
    // }
    public function checkfordeletedbrand($name)
    {
        $restore =  Brand::where('name','=',$name)->whereNotNull('deleted_at')->first();
        if(!empty($restore) && count($restore)>0){
            return $restore;
        } else {
            return '{"name":"No data"}';
        }
    }
    public function restoredeletedbrand($name)
    {
        $restore = Brand::withTrashed()->where('name','=',$name)->restore();
        return $restore;
    }

}
