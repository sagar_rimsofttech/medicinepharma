<?php

namespace App\Traits;

use App\Models\ProductReview;


trait ProductReviewTrait {

    
    public function checkfordeletedproductreview($name)
    {
        $restore =  ProductReview::where('name','=',$name)->whereNotNull('deleted_at')->first();
        if(!empty($restore) && count($restore)>0){
            return $restore;
        } else {
            return '{"name":"No data"}';
        }
    }
    public function restoredeletedproductreview($name)
    {
        $restore = ProductReview::withTrashed()->where('name','=',$name)->restore();
        return $restore;
    }

}
