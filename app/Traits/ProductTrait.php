<?php

namespace App\Traits;

use App\Models\Product;

trait ProductTrait {

    private $product;
    public function __construct(Product $product)
    {
       $this->product = $product;
    }
    public function getProductUsingSlug($slug)
    {
        return Product::where('slug',$slug)->first();
    }
    public function getRelatedProduct($slug)
    {
        $related =  Product::where('slug',$slug)->first();
        //->where('id','!=',$related->id)
        $categoryproduct = Product::where('category_id',$related->category_id)->get();
        return $categoryproduct;
    }

}
