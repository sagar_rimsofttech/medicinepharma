<?php

namespace App\Traits;

use App\Models\Category;

trait CategoryTrait
{

    // private $category;

    // public function __construct(Category $category){
    //     $this->category = $category;
    // }
    // public function restoreCategoryTrait($id)
    // {

    //     $restore = Category::withTrashed()->find($id->id)->restore();
    //     if(count($restore) > 0)
    //     {

    //     }
    // }
    public function checkfordeletedcategory($name)
    {
        $restore =  Category::withTrashed()->where('name', '=', $name)->whereNotNull('deleted_at')->first();
        if (!empty($restore)) {
            return $restore;
        } else {
            return '{"name":"No data"}';
        }
    }
    public function restoredeletedcategory($name)
    {
        $restore = Category::withTrashed()->where('name', '=', $name)->restore();
        return $restore;
    }

    public static function getCategoryTrait()
    {
        return $categories = Category::latest()->get();
    }

    public static function getCategory($category_id)
    {
        return $caregory = Category::findorFail($category_id);
    }
}
