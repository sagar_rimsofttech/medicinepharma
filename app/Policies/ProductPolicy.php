<?php

namespace App\Policies;

// use App\Models\Product;

use App\Admin;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Zizaco\Entrust\EntrustFacade as Entrust;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $category
     * @return bool
     */
    public function index(Admin $user)
    {
        // dd(Entrust::hasRole(['admin','owner']));
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('list-product')) ? true : false;
    }

    /**
     * @param Category $category
     * @return bool
     */
    public function create(Admin $user)
    {
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('create-product')) ? true : false;
    }
    public function edit(Admin $user)
    {
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('edit-product')) ? true : false;
    }
    /**
     * @param Category $category
     * @return bool
     */
    public function update(Admin $user)
    {
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('update-product')) ? true : false;
    }

    /**
     * @param Category $category
     * @return bool
     */
    public function show(Admin $user)
    {
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('show-product')) ? true : false;
    }

    /**
     * @param Category $user
     * @return bool
     */
    public function delete(Admin $user)
    {
        return (Entrust::hasRole(['owner']) && Entrust::can('delete-product')) ? true : false;
    }
}
