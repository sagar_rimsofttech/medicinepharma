<?php

namespace App\Policies;

use App\Admin;
use App\Models\Category;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Zizaco\Entrust\EntrustFacade as Entrust;

class CategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $category
     * @return bool
     */
    public function index(Admin $admin)
    {
        // dd(Entrust::hasRole(['admin','owner']));
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('list-category')) ? true : false;
    }

    /**
     * @param Category $category
     * @return bool
     */
    public function create(Admin $admin)
    {
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('create-category')) ? true : false;
    }
    public function edit(User $admin)
    {
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('edit-category')) ? true : false;
    }
    /**
     * @param Category $category
     * @return bool
     */
    public function update(Admin $admin)
    {
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('update-category')) ? true : false;
    }

    /**
     * @param Category $category
     * @return bool
     */
    public function show(Admin $admin)
    {
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('show-category')) ? true : false;
    }

    /**
     * @param Category $user
     * @return bool
     */
    public function delete(Admin $admin)
    {
        return (Entrust::hasRole(['owner']) && Entrust::can('delete-category')) ? true : false;
    }
}
