<?php

namespace App\Policies;

// use App\Models\Product;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Zizaco\Entrust\EntrustFacade as Entrust;

class ProductReviewPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $category
     * @return bool
     */
    public function index(User $user)
    {
        // dd(Entrust::hasRole(['admin','owner']));
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('list-productreview')) ? true : false;
    }

    /**
     * @param Category $category
     * @return bool
     */
    public function create(User $user)
    {
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('create-productreview')) ? true : false;
    }
    public function edit(User $user)
    {
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('edit-productreview')) ? true : false;
    }
    /**
     * @param Category $category
     * @return bool
     */
    public function update(User $user)
    {
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('update-productreview')) ? true : false;
    }

    /**
     * @param Category $category
     * @return bool
     */
    public function show(User $user)
    {
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('show-productreview')) ? true : false;
    }

    /**
     * @param Category $user
     * @return bool
     */
    public function delete(User $user)
    {
        return (Entrust::hasRole(['owner']) && Entrust::can('delete-productreview')) ? true : false;
    }
}
