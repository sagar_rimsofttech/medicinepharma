<?php

namespace App\Policies;

use App\Models\SubCategory;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Zizaco\Entrust\EntrustFacade as Entrust;

class SubCategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $category
     * @return bool
     */
    public function index(User $user)
    {
        // dd(Entrust::hasRole(['admin','owner']));
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('list-subcategory')) ? true : false;
    }

    /**
     * @param SubCategory $subcategory
     * @return bool
     */
    public function create(User $user)
    {
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('create-subcategory')) ? true : false;
    }
    public function edit(User $user)
    {
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('edit-subcategory')) ? true : false;
    }
    /**
     * @param SubCategory $subcategory
     * @return bool
     */
    public function update(User $user)
    {
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('update-subcategory')) ? true : false;
    }

    /**
     * @param SubCategory $subcategory
     * @return bool
     */
    public function show(User $user)
    {
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('show-subcategory')) ? true : false;
    }

    /**
     * @param SubCategory $subcategory
     * @return bool
     */
    public function delete(User $user)
    {
        return (Entrust::hasRole(['owner']) && Entrust::can('delete-subcategory')) ? true : false;
    }
}
