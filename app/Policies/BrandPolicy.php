<?php

namespace App\Policies;

use App\Models\Brand;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Zizaco\Entrust\EntrustFacade as Entrust;

class BrandPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $category
     * @return bool
     */
    public function index(Brand $brand)
    {
        // dd(Entrust::hasRole(['admin','owner']));
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('list-brand')) ? true : false;
    }

    /**
     * @param Category $category
     * @return bool
     */
    public function create(User $user)
    {
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('create-brand')) ? true : false;
    }
    public function edit(User $user)
    {
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('edit-brand')) ? true : false;
    }
    /**
     * @param Category $category
     * @return bool
     */
    public function update(User $user)
    {
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('update-brand')) ? true : false;
    }

    /**
     * @param Category $category
     * @return bool
     */
    public function show(User $user)
    {
        return (Entrust::hasRole(['admin','owner']) && Entrust::can('show-brand')) ? true : false;
    }

    /**
     * @param Category $user
     * @return bool
     */
    public function delete(User $user)
    {
        return (Entrust::hasRole(['owner']) && Entrust::can('delete-brand')) ? true : false;
    }
}
