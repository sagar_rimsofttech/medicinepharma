<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\VerifyController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
// use Socialite;
// use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    public function showLoginForm()
    {
        return view('auth.login');
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(VerifyController $verify)
    {
        $this->middleware('guest', ['except' => ['logout', 'userLogout']]);
        $this->verifyphone = $verify;
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if ($this->guard()->validate($this->credentials($request))) {
            $user = $this->guard()->getLastAttempted();
            if ($user->active && $this->attemptLogin($request)) {
                return $this->sendLoginResponse($request);
            } else {
                $this->incrementLoginAttempts($request);
                $user->code = $this->verifyphone->sendVerify($user, $user->mobile_no);
                if ($user->save()) {
                    return redirect('/verify-phone?phone=' . $user->mobile_no);
                }
            }
        }

        $this->incrementLoginAttempts($request);
        return $this->sendFailedLoginResponse($request);
    }



    public function userLogout()
    {
        Auth::guard('web')->logout();
        return redirect('/');
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }


    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();
        $authuser = $this->findOrCreateUser($user, $provider);
        if ($authuser->active) {
            Auth::login($authuser, true);
        } else {
            // $this->incrementLoginAttempts($provider);
            if (empty($authuser->mobile_no)) {
                return redirect('/update-phone?email=' . $authuser->email);
            }
            $authuser->code = $this->verifyphone->sendVerify($authuser, $authuser->mobile_no);
            if ($authuser->save()) {
                return redirect('/verify-phone?phone=' . $authuser->mobile_no);
            }
        }
        return redirect($this->redirectTo);
    }

    public function findOrCreateUser($user, $provider)
    {
        $authuserproviderid = User::where('provider_id', $user->id)->first();
        if (!empty($authuserproviderid)) {
            return $authuserproviderid;
        } else {
            $authuseremail = User::where('email', $user->email)->first();
            if (!empty($authuseremail)) {
                $authuseremail->provider_id = $user->id;
                $authuseremail->$provider;
                $authuseremail->save();
                return $authuseremail;
            }
            return User::create([
                'name' => $user->name,
                'email' => $user->email,
                'password' => Hash::make('password'),
                'provider' => $provider,
                'provider_id' => $user->id,
                'email_verified_at' => date('Y-m-d h:i:s'),
            ]);
        }
    }
}
