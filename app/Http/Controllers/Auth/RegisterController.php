<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\Http\Controllers\VerifyController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/verify-phone';
    private $verify;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(VerifyController $verify)
    {
        $this->middleware('guest');
        $this->verifyphone = $verify;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    protected function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        return $this->registered($request, $user) ?: redirect('/verify-phone?phone=' . $request->phone);
    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'max:255', 'unique:users'],
            // 'password' => ['required', 'string', 'min:6', 'confirmed'],
            // 'password' => ['required', 'string', 'min:6', 'confirmed'],
            'mobile_no' => ['required', 'regex:/^[6-9][0-9]{9}$/', 'unique:users'],
            'password' => 'required|min:6',
            'password_confirmation' => 'required|required_with:password|same:password'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'mobile_no' => $data['mobile_no']
        ]);
        $mobile_no = $data['mobile_no'];
        $this->verifyphone->sendVerify($user, $mobile_no);
        return $user;

        // return redirect()->route('verify-phone')->with(['phone_number' => $data['phone_number']]);
    }
}
