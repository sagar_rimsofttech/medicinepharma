<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
class AdminLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin', ['expect' => ['logout']]);
    }

    protected function guard(){
        return Auth::guard('admin');
    }

    use AuthenticatesUsers;
    public function showLoginForm()
    {
        return view('auth.admin-login');
    }
    protected $redirectTo = '/admin/dashboard';
    public function login(Request $request)
    {
        $this->validate($request ,[
            'email' =>'required|email',
            'password' => 'required|min:6'
        ]);

        if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember))
        {
            return redirect()->intended(route('admin.dashboard'));
        }

        return redirect()->back()->withInput($request->only('email','remember'));
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/');
    }
}
