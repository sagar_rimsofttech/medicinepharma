<?php

namespace App\Http\Controllers;

use Anand\LaravelPaytmWallet\Facades\PaytmWallet;
use App\Models\Order;
use App\Notifications\OrderNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

// use PaytmWallet;

class PaytmController extends Controller
{
    /**
     * Redirect the user to the Payment Gateway.
     *
     * @return Response
     */
    public function pay($order_id, $totalamount)
    {

        $payment = PaytmWallet::with('receive');

        $payment->prepare([
            'order' => $order_id, // your order id taken from cart
            'user' => 'Cust_id_12', // your user id
            'mobile_number' => 7277407765, // your customer mobile no
            'email' => 'hungerforcode@gmail.com', // your user email address
            'amount' => $totalamount, // amount will be paid in INR.
            'callback_url' => config('constants.paytm_callback'), // callback URL
        ]);

        return $payment->receive();
    }

    /**
     * Obtain the payment information.
     *
     * @return Object
     */
    public function paymentCallback()
    {
        $transaction = PaytmWallet::with('receive');

        $response = $transaction->response(); // To get raw response as array
        //Check out response parameters sent by paytm here -> http://paywithpaytm.com/developer/paytm_api_doc?target=interpreting-response-sent-by-paytm

        if ($transaction->isSuccessful()) {
            Session::forget('cart');
            $update_trnx_id = Order::find($transaction->getOrderId());
            $update_trnx_id->tarnsaction_id = $transaction->getTransactionId();
            $update_trnx_id->payment_status = 1;
            $update_trnx_id->save();
            $user = $update_trnx_id->user;
            $user->notify(new OrderNotification('sms'));
            $user->notify(new OrderNotification('mail'));
            return redirect()->route('order.success', $transaction->getOrderId())->with('Ordered Sucessfully ');
        } else if ($transaction->isFailed()) {
            //Transaction Failed
            $update_trnx_id = Order::find($transaction->getOrderId());
            $update_trnx_id->payment_status = 4;
            $update_trnx_id->tarnsaction_id = $transaction->getTransactionId();
            $update_trnx_id->save();
            return redirect()->route('order.failed', $transaction->getOrderId())->with('Payment Failed ');
        } else if ($transaction->isOpen()) {
            //Transaction Open/Processing
            $update_trnx_id = Order::find($transaction->getOrderId());
            $update_trnx_id->payment_status = 2;
            $update_trnx_id->tarnsaction_id = $transaction->getTransactionId();
            $update_trnx_id->save();
            return redirect()->route('order.open', $transaction->getOrderId())->with('Payemt is still processing');
        }
        $transaction->getResponseMessage(); //Get Response Message If Available
        //get important parameters via public methods
        $transaction->getOrderId(); // Get order id

        $transaction->getTransactionId(); // Get transaction id
    }
}
