<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\UpdatePasswordRequest;
use App\MedicinePharma\Repositories\User\UserInterface;
use App\Models\Order;
use App\Models\State;
use App\Models\UserAddress;
use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Notifications\smsNotification;
// use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Notification;

class UserProfileController extends Controller
{
    private $user;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }
    public function index()
    {
        if (!Auth::user()) {
            Toastr::warning('Please Login To continue');
            return redirect()->back();
        }
        $useraddresses = UserAddress::where('user_id', Auth::user()->id)->latest()->get();
        $states = State::where('state_id', 58)->get();
        $orders = Order::where('user_id', Auth::user()->id)->latest()->paginate(5);
        // $orders = Order::latest()->get();
        $orders->transform(function ($order, $key) {
            $order->cart = unserialize($order->cart);
            return $order;
        });
        return view('frontend.user-profile', compact('useraddresses', 'states', 'orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeaddress(Request $request)
    {
        try {
            $storeaddress = $this->user->storeaddress($request);
            return response()->json(['message' => 'Address Saved Successfully inserted', 'data' => $storeaddress], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    public function updatepassword(UpdatePasswordRequest $request)
    {
        try {
            $updatepassword = $this->user->updatepassword($request);
            return response()->json(['message' => "Password Updated successfully"], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getExistingUser(Request $request)
    {
        return $checkuser = $this->user->checkuser($request->get('email'));
    }

    public function loginWithOtp(Request $request)
    {
        Log::info($request);
        $user  = User::where([['mobile_no', '=', request('mobile')], ['code', '=', request('otp')]])->first();
        if ($user) {
            Auth::login($user, true);
            User::where('mobile_no', '=', $request->mobile)->update(['code' => null]);
            return redirect()->route('showIndex');
        } else {
            return Redirect()->back();
        }
    }

    public function loginWithOtpView()
    {
        return  view('vendor/adminlte/OtpLogin');
    }

    public function sendOtp(Request $request)
    {

        $otp = random_int(1000, 9999);
        Log::info("otp = " . $otp);
        $message = ("<#> $otp is your OTP . Kindly donot share your OTP with anyone. ");
        $user = User::where('mobile_no', '=', $request->mobile)->first();
        if ($user) {
            $user->code = $otp;
            $user->save();
            Notification::send($user, new smsNotification((int)$request->mobile, $message, $otp));
            return response()->json(['Message' => 'Otp Sent Sucessfully to mobile Number'], 200);
        } else {
            return response()->json(['Message' => 'Mobile Number is not registered With Us'], 400);
        }
        // send otp to mobile no using sms api
    }

    public function checkmobilenumber(Request $request)
    {
        $otp = random_int(1000, 9999);
        $message = ("<#> $otp is your OTP . Kindly donot share your OTP with anyone. ");
        $user = User::where('mobile_no', '=', $request->mobile)->first();
        if (!empty($user) && !empty($user->mobile_no_verified_at)) {
            return response()->json(['Message' => 'Mobile Number is allready registered'], 400);
        }
        $user = User::where('email', '=', $request->email)->first();
        $user->mobile_no =  $request->mobile;
        $user->code = $otp;
        $user->save();
        Notification::send($user, new smsNotification((int)$request->mobile, $message, $otp));
        return response()->json(['Message' => 'Otp Sent Sucessfully to mobile Number'], 200);

        // send otp to mobile no using sms api
    }

    public function updatePhone(Request $request, $mobile_no = null)
    {
        $email =  $request->email;
        return  view('vendor/adminlte/updatemobile', compact('email'));
    }
}
