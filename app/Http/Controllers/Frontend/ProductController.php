<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MedicinePharma\Repositories\Product\ProductInterface;
use App\Models\Cart;
use App\Models\SearchKeyword;
use App\Traits\ProductTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
// use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    use ProductTrait;
    private $product;

    public function __construct(ProductInterface $product)
    {
        $this->product = $product;
    }

    public function productdetails($slug)
    {
        $productid = $this->getProductUsingSlug($slug);
        $getrelatedproduct = $this->getRelatedProduct($slug);
        $product = $this->product->find($productid->id);

        return view('frontend.productdetails', compact('product', 'getrelatedproduct'));
    }

    public function getAddToCart(Request $request, $quantity, $product_id)
    {
        $product = $this->product->find($product_id);
        $oldCart = $request->session()->has('cart') ? $request->session()->get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $quantity, $product->id);
        $request->session()->put('cart', $cart);
        // dd($request->session()->get('cart'));
        $data = [];
        $response = [
            "quntity" =>  $cart->totalQty,
            "itemtotal" => $cart->totalPrice,
            'itemtotalprice' => $cart->items[$product_id]['price'],
        ];
        return $response;
    }


    public function getReduceByOne($id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->reduceByOne($id);

        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }

        $response = [
            "quntity" =>  $cart->totalQty,
            "itemtotal" => $cart->totalPrice,
            'itemtotalprice' => $cart->items[$id]['price'],
        ];
        return $response;
    }

    public function removeProductFromCart($id)
    {
        // $product = $this->product->find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeProductFromCart($id);
        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }
        $response = [
            "quntity" =>  $cart->totalQty,
            "itemtotal" => $cart->totalPrice,
        ];

        return $response;
    }


    public function getCart()
    {
        if (!Session::has('cart')) {
            return view('frontend.shopping-cart');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        return view('frontend.shopping-cart', ['products' => $cart->items, 'totalPrice' => $cart->totalPrice]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    function searchfetch(Request $request)
    {
        if ($request->get('query')) {
            if (!empty($request->get('query'))) {
                if (strlen($request->get('query')) >= 5) {
                    $savesearch = $this->savekeyword($request);
                }
                $query = $request->get('query');
                $data = DB::table('products')
                    ->where('name', 'LIKE', "%{$query}%")
                    ->get();
                $output = '<ul class="dorp-down-custom global-search" style="display:block;">';
                foreach ($data as $row) {
                    $url = "/product/details/$row->slug";
                    $output .= '<li><a href="' . $url . '" class="ml-3 click-product">' . $row->name . '</a></li>';
                    $output .= '<div class="dropdown-divider"></div>';
                }
                $output .= '</ul>';
                echo $output;
            }
        }
    }

    public function savekeyword($request)
    {
        $searchdata = new SearchKeyword();
        $searchdata->search_keyword = $request->get('query') ?? null;
        $searchdata->user_id = Auth::user()->id ?? null;
        $searchdata->created_at = now();
        $searchdata->updated_at = now();
        $searchdata->save();
    }
}
