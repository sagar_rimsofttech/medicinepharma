<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\CustomCurl;
use App\MedicinePharma\Repositories\Category\CategoryInterface;
use App\MedicinePharma\Repositories\Product\ProductInterface;
use \GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $category;
    private $product;
    public function __construct(CategoryInterface $category, ProductInterface $product){
        $this->category = $category;
        $this->product = $product;
    }
    public function index()
    {
        try{
            $categories = $this->category->getAll();
            $products = $this->product->getProductdetails($request=null,$id=null);
            $allproducts = $this->product->getAll();
            return view('frontend.index',compact('categories','products','allproducts'));
          }catch(\Exception $e){
            abort(400, "Somehting went wrong");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categoryproduct(Request $request,$id)
    {
        try{
            // dd($request->all());
            $categories = $this->category->getAll();
            $products = $this->product->getProductdetails($request,$id);
            $latestproducts = $this->product->getLatestProduct();
            $category_id = $id;
            $sortBy = $request->sortby?$request->sortby:'0';
            $minprice = $request->minprice?$request->minprice:'10';
            $maxprice = $request->maxprice?$request->maxprice:'10000';
            return view('frontend.categoryproduct',compact('categories','products','category_id','sortBy','minprice','maxprice','latestproducts'));
          }catch(\Exception $e){
            //   dd($e->getMessage());
            abort(400, "Somehting went wrong");
            }
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
