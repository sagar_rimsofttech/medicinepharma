<?php

namespace App\Http\Controllers\Frontend;

use Anand\LaravelPaytmWallet\Facades\PaytmWallet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PaytmController;
use App\Http\Requests\Prescription\StorePrescriptionRequest;
use App\MedicinePharma\Repositories\Order\OrderInterface;
use App\MedicinePharma\Repositories\User\UserInterface;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Prescription;
use App\Notifications\OrderNotification;
use App\Models\State;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $payment;
    private $order;
    private $orders;
    private $user;

    public function __construct(PaytmController $payment, Order $order, OrderInterface $orders, UserInterface $user)
    {
        $this->payment = $payment;
        $this->order = $order;
        $this->orders = $orders;
        $this->user = $user;
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Session::has('cart')) {
            return redirect()->back();
        }
        if ($request->get('email') && $request->create_account == 1) {
            $checkuser = $this->user->checkuser($request->get('email'));
            if (!empty($checkuser)) {
                return redirect()->back()->withErrors('Email id is allready registered ,Please Login to Proceed');
            }
        }
        if ($request->create_account == 1) {

            $createUser = $this->user->create($request);
            $saveaddress = $this->user->storeaddress($request, $createUser);
            $order = $this->orders->storeOrder($request, $createUser, $saveaddress);
        } else {
            if (Auth::user() && empty($request->address_id)) {
                return redirect()->back()->withErrors('Please Select the address');
            }
            $order = $this->orders->storeOrder($request);
        }
        if ($request->payment_type == 1) {
            $payment = PaytmWallet::with('receive');
            $payment->prepare([
                'order' => $order->id, // your order id taken from cart
                'user' => !empty(Auth::user()->id) ? Auth::user()->id : $createUser->id, // your user id
                'mobile_number' => !empty($order->user->mobile_no) ? $order->user->mobile_no : '123', // your customer mobile no
                'email' => $order->user->email, // your user email address
                'amount' => $order->amount, // amount will be paid in INR.
                'callback_url' => 'http://127.0.0.1:8000/payment/status' // callback URL
            ]);

            return $payment->receive();
        } else {
            $user = $order->user;
            $user->notify(new OrderNotification('sms'));
            $user->notify(new OrderNotification('mail'));
            Session::forget('cart');
            return redirect()->route('order.success', $order->id)->with('Ordered Sucessfully ');
        }
        // $makepayment = $this->payment->pay($order->id, $cart->totalPrice);
        // return view('frontend.shopping-cart');

    }

    // public function storePrescription(StorePrescriptionRequest $request)
    // {
    //     try {
    //         if ($request->get('email') && $request->create_account == 1) {
    //             $checkuser = $this->user->checkuser($request->get('email'));
    //             if (!empty($checkuser)) {
    //                 return redirect()->back()->withErrors('Please Login to Proceed');
    //             }
    //         }
    //         if ($request->create_account == 1) {
    //             $createUser = $this->user->create($request);
    //             $saveaddress = $this->user->storeaddress($request, $createUser);
    //             $order = $this->orders->storePrescription($request, $createUser, $saveaddress);
    //         } else {
    //             $order = $this->orders->storePrescription($request);
    //         }
    //         return redirect()->back()->with('message', 'Prescription Uploaded Successfully , We will contact with you shortly ');
    //     } catch (\Exception $e) {
    //         logger($e->getMessage());
    //         return response()->json(['message' => 'Something Went wrong'], 400);
    //     }
    // }
    public function storePrescription(StorePrescriptionRequest $request)
    {
        try {

            $order = $this->orders->storePrescription($request);

            return redirect()->back()->with('message', 'Prescription Uploaded Successfully , We will contact with you shortly ');
        } catch (\Exception $e) {
            dd($e->getMessage());
            return response()->json(['message' => 'Something Went wrong'], 400);
        }
    }
    public function getRecentOrder($order_id)
    {
        $order =  $this->order->find($order_id);
        return view('frontend.order-summary', compact('order'));
        //    dd(unserialize($order->cart));
    }
    public function getRecentFailedOrder($order_id)
    {
        $order =  $this->order->find($order_id);
        return view('frontend.order-failed', compact('order'));
        //    dd(unserialize($order->cart));
    }
    public function getRecentOpenOrder($order_id)
    {
        $order =  $this->order->find($order_id);
        return view('frontend.order-open', compact('order'));
        //    dd(unserialize($order->cart));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function checkout()
    {
        $states = State::where('state_id', 58)->get();
        if (!Session::has('cart')) {
            return view('frontend.shopping-checkout', compact('states'));
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        return view('frontend.shopping-checkout', ['products' => $cart->items, 'states' => $states, 'totalPrice' => $cart->totalPrice]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
