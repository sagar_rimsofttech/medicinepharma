<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Notifications\smsNotification;
// use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Notification;

class VerifyController extends Controller

{

    public function getVerify(Request $request)
    {
        $mobile_no = $request->phone;
        return view('frontend.verify-mobile', compact('mobile_no'));
    }
    public function sendVerify($user, $mobile_no)
    {
        $otp = random_int(1000, 9999);
        $message = ("<#> $otp is your OTP . Kindly donot share your OTP with anyone. ");
        $mobile_no = $mobile_no;
        $user->code = $otp;
        $user->active = 0;
        $user->save();
        // $this->notify(new smsNotification($mobile_no, $message, $otp));
        Notification::send($user, new smsNotification($mobile_no, $message, $otp));
        return $otp;
    }

    public function postVerify(Request $request)
    {
        // dd($request->all());
        if ($user = User::where('code', $request->code)->where('mobile_no', $request->mobile)->first()) {
            $user->active = 1;
            $user->code = null;
            $user->mobile_no_verified_at = now();
            $user->save();

            return redirect()->route('login')->withMessage('Your acccount is Active Now');
        } else {
            return back()->withMessage('Verification Code is incorrect, please try again');
        }
    }
}
