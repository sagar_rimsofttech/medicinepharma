<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MedicinePharma\Repositories\Category\CategoryInterface;
use App\User;
use Exception;

class CategoryApiController extends BaseController {

    public $apicompany;

    public function __construct(CategoryInterface $category) {
        $this->category = $category;
    }

    public function getCategoryApi(Request $request) {
        try {
                $response = $this->category->getAll();
                foreach($response as $res){
                $action_url =  [
                    'uri' => 'doctors/' . $res->id . '/show',
                ];
            }
            return response()->json(['data'=>$response,'action'=>$action_url,'code'=>200,'message'=>"Success"]);
        } catch (Exception $ex) {
            logger()->error($ex->getMessage());
            // dd($ex->getMessage());
            // abort(400, trans('errors.COMPANY_1'));
            return response()->json(['Error'=>'No Date Found','code'=>400,'message'=>"failed"]);
        }
    }



}
