<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SubCategory\StoreSubCategoryRequest;
use App\Http\Requests\Admin\SubCategory\UpdateSubCategoryRequest;
use App\MedicinePharma\Repositories\Category\CategoryInterface;
use App\MedicinePharma\Repositories\SubCategory\SubCategoryInterface;
use App\Models\Category;
use App\Traits\SubCategoryTrait;
use Brian2694\Toastr\Toastr;
use Exception;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use SubCategoryTrait;
    private $subcategory;
    private $category;
    public function __construct(SubCategoryInterface $subcategory, CategoryInterface $category){
        $this->subcategory = $subcategory;
        $this->category = $category;
    }
    public function index()
    {
        $subcategories = $this->subcategory->getAll();
        return view('admin.subcategory/index',compact('subcategories'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->category->getList();
        return view('admin.subcategory.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSubCategoryRequest $request)
    {
        try {
            $savesubcategory = $this->subcategory->store($request);
            return redirect()->route('subcategory.index');
        }catch (Exception $ex) {
            // dd($ex->getMessage());
            abort(400, "Somehting went wrong");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subcategory = $this->subcategory->find($id);
        $categories = $this->category->getList();
        return view('admin.subcategory/edit',compact('subcategory','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSubCategoryRequest $request, $id)
    {
        $updatesubcategory = $this->subcategory->update($request,$id);
        return redirect()->route('subcategory.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $deletesubcategory = $this->subcategory->delete($id);
        }catch(\Exception $e){
            return redirect()->route('subcategory.index')->with('danger', $e->getMessage());
        }
        return redirect()->route('subcategory.index');
    }

    public function checkfordeleted($keyword=null)
    {
        $checktrait = $this->checkfordeletedsubcategory($keyword);
        return  $checktrait;
    }
    public function restoredeleted($keyword=null)
    {
        $restoretrait = $this->restoredeletedsubcategory($keyword);
        return  $restoretrait;
    }

    public function searchSubCategory($keyword=null,Request $request)
    {
        $subcategories = $this->subcategory->searchSubCategory($keyword,$request);
        $subcategoryArray=[];
        foreach ($subcategories as $subcategorykey=>$category){
            $subcategoryArray[$subcategorykey]['id']=$category->id;
            $subcategoryArray[$subcategorykey]['text']=$category->name;
        }
        return $subcategoryArray;
    }
}
