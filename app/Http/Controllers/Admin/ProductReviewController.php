<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductReview\StoreProductReviewRequest;
use App\MedicinePharma\Repositories\Product\ProductInterface;
use App\MedicinePharma\Repositories\ProductReview\ProductReviewInterface;
use Exception;

class ProductReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    private $product;
    private $productreview;
    
    public function __construct(ProductInterface $product,ProductReviewInterface $productreview){
       $this->product = $product;
       $this->productreview = $productreview;
    }

    public function index()
    {
        $productreviews = $this->productreview->getAll();
        return view('admin.productreview/index',compact('productreviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.productreview.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductReviewRequest $request)
    {
       
        try {
            $saveproductreview = $this->productreview->store($request);
            return redirect()->route('productreview.index');
        }catch (Exception $ex) {
            dd($ex->getMessage());
            abort(400, "Somehting went wrong");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $productreview = $this->productreview->find($id);
        return view('admin.productreview/edit',compact('productreview'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateproductreview = $this->productreview->update($request,$id);
        return redirect()->route('productreview.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $deleteproductreview = $this->productreview->delete($id);
        }catch(\Exception $e){
            return redirect()->route('productreview.index')->with('danger', $e->getMessage());
        }
        return redirect()->route('productreview.index');
    }
    public function checkfordeletedproductreview($keyword=null)
    {
        $checktrait = $this->checkfordeletedproductreview($keyword);
        return  $checktrait;
    }
    public function restoredeletedproductreview($keyword=null)
    {
        $restoretrait = $this->restoredeletedproductreview($keyword);
        return  $restoretrait;
    }
    public function searchProduct($keyword=null)
    {
        $products = $this->productreview->searchProduct($keyword);
        $productArray=[];
        foreach ($products as $productkey=>$product){
            $productArray[$productkey]['id']=$product->id;
            $productArray[$productkey]['text']=$product->name;
        }
        return $productArray;
    }
}
