<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MedicinePharma\Repositories\Role\RoleInterface;
use App\User;
use App\MedicinePharma\Repositories\Admin\AdminInterface as UserUserInterface;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    private $user;
    private $role;

    public function __construct(UserUserInterface $user, RoleInterface $role)
    {
        $this->user = $user;
        $this->role = $role;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Admin::get();
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = $this->role->getList();
        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        DB::beginTransaction();
        try {
            $this->user->create($request);
            DB::commit();
            return redirect()->route('user.index')->with('success', 'User registered successfully');
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            logger($e->getMessage());
            return redirect()->back()->with('error', 'User registration failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $roles = $this->role->getList();
            $user = $this->user->editUser($id);
        } catch (\Exception $e) {
            return redirect()->route('user.index')->with('danger', $e->getMessage());
        }
        return view('admin.users.edit', compact('roles', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $this->user->update($id, $request);
            DB::commit();
            return redirect()->route('user.index')->with('success', 'User registered successfully');
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            logger($e->getMessage());
            return redirect()->back()->with('error', 'User registration failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            $delete_user = $this->user->deleteUser($id);
        } catch (\Exception $e) {
            return redirect()->route('user.index')->with('danger', $e->getMessage());
        }
        Toastr::Warning('User Successfully Deleted :)', 'Success');
        return redirect()->route('user.index');
    }
}
