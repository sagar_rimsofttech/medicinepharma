<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\StoreCategoryRequest;
use App\Http\Requests\Admin\Category\UpdateCategoryRequest;
use App\MedicinePharma\Repositories\Category\CategoryInterface;
use App\Traits\CategoryTrait;
use Brian2694\Toastr\Toastr;
use Exception;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use CategoryTrait;
    private $category;
    public function __construct(CategoryInterface $category){
        $this->category = $category;
    }
    public function index()
    {
        $categories = $this->category->getAll();
        return view('admin.category/index',compact('categories'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategoryRequest $request)
    {
        try {
            $savecategory = $this->category->store($request);
            return redirect()->route('category.index');
        }catch (Exception $ex) {
            dd($ex->getMessage());
            abort(400, "Somehting went wrong");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->category->find($id);
        return view('admin.category/edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, $id)
    {
        $updatecategory = $this->category->update($request,$id);
        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $deletecategory = $this->category->delete($id);
        }catch(\Exception $e){
            return redirect()->route('category.index')->with('danger', $e->getMessage());
        }
        return redirect()->route('category.index');
    }



    public function checkfordeleted($keyword=null)
    {
        $checktrait = $this->checkfordeletedcategory($keyword);
        return  $checktrait;
    }
    public function restoredeleted($keyword=null)
    {
        $restoretrait = $this->restoredeletedcategory($keyword);
        return  $restoretrait;
    }

    public function searchCategory($keyword=null)
    {
        $categories = $this->category->searchCategory($keyword);
        $categoryArray=[];
        foreach ($categories as $categorykey=>$category){
            $categoryArray[$categorykey]['id']=$category->id;
            $categoryArray[$categorykey]['text']=$category->name;
        }
        return $categoryArray;
    }
}
