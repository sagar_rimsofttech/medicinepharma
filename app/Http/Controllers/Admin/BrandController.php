<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MedicinePharma\Repositories\Brand\BrandInterface;
use App\Traits\BrandTrait;
use Brian2694\Toastr\Toastr;
use Exception;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use BrandTrait;
    private $brand;
    public function __construct(BrandInterface $brand){
        $this->brand = $brand;
    }
    public function index()
    {
        $brands = $this->brand->getAll();
        return view('admin.brand/index',compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $savebrand = $this->brand->store($request);
            return redirect()->route('brand.index');
        }catch (Exception $ex) {
            dd($ex->getMessage());
            abort(400, "Something went wrong");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = $this->brand->find($id);
        return view('admin.brand/edit',compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updatebrand = $this->brand->update($request,$id);
        return redirect()->route('brand.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $deletebrand = $this->brand->delete($id);
        }catch(\Exception $e){
            return redirect()->route('brand.index')->with('danger', $e->getMessage());
        }
        return redirect()->route('brand.index');
    }

    public function checkfordeleted($keyword=null)
    {
        $checktrait = $this->checkfordeletedbrand($keyword);
        return  $checktrait;
    }
    public function restoredeleted($keyword=null)
    {
        $restoretrait = $this->restoredeletedbrand($keyword);
        return  $restoretrait;
    }
    public function searchBrand($keyword=null)
    {
        $brands = $this->brand->searchBrand($keyword);
        $brandArray=[];
        foreach ($brands as $brandkey=>$brand){
            $brandArray[$brandkey]['id']=$brand->id;
            $brandArray[$brandkey]['text']=$brand->name;
        }
        return $brandArray;
    }
}
