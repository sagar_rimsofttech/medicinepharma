<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MedicinePharma\Repositories\Order\OrderInterface;
use App\Traits\BrandTrait;
use Brian2694\Toastr\Toastr;
use Exception;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $order;
    public function __construct(OrderInterface $order){
        $this->order = $order;
    }
    public function index()
    {
        $orders = $this->order->getAll();
        return view('admin.order/index',compact('orders'));
    }

    public function getOrder(Request $request)
    {
        $order_item = $this->order->find($request->id);
        $orderitem = [];
        $orderitem[] = unserialize($order_item->cart);
        return $orderitem;
    }


}
