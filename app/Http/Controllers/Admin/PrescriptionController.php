<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MedicinePharma\Repositories\Order\OrderInterface;
use App\MedicinePharma\Repositories\Prescription\PrescriptionInterface;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Prescription;
use App\Models\Product;
use Brian2694\Toastr\Toastr;
use Exception;
use Illuminate\Support\Facades\Session;

class PrescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $prescription;
    private $order;
    public function __construct(PrescriptionInterface $prescription,OrderInterface $order){
        $this->prescription = $prescription;
        $this->order = $order;
    }
    public function index()
    {
        $prescriptions = $this->prescription->getAll();
        return view('admin.prescription/index',compact('prescriptions'));

    }
    public function searchProduct($keyword=null)
    {
        $products = $this->prescription->searchProduct($keyword);
        $productArray=[];
        foreach ($products as $productkey=>$product){
            $productArray[$productkey]['id']=$product->id;
            $productArray[$productkey]['text']=$product->name;
        }
        return $productArray;
    }
    public function addprescriptionorder(Request $request)
    {
        Session::forget('cart');
        foreach($request->product_name as $key =>$value){
        $product = Product::find($value);
        $oldCart = $request->session()->has('cart') ? $request->session()->get('cart') :null;
        $cart = new Cart($oldCart);
        $cart->add($product,$request->quantity[$key], $product->id);
        $request->session()->put('cart', $cart);
        // dd($request->session()->get('cart'));
        $data= [];
        $response[] = [
            "quntity" =>  $cart->totalQty,
            "itemtotal" => $cart->totalPrice,
            'itemtotalprice' => $cart->items[$product->id]['price'],
        ];


    }
    $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $order = new Order();
        $order->cart = serialize($cart);
        $order->user_id = !empty($request->hidden_user_id)?$request->hidden_user_id:'000';
        $order->order_date = now();
        $order->amount = $cart->totalPrice;
        $order->user_delivery_address = !empty($address)?$address:null;
        $order->payment_method = !empty($request->payment_type)?$request->payment_type:2;
        $order->order_from = 2;//prescription order
        $order->prescription_id = !empty($request->hidden_id)?$request->hidden_id:null;
        $order->user_delivery_address = !empty($request->hidden_address_id)?$request->hidden_address_id:null;
        $order->delivery_note = !empty($request->hidden_order_notes)?$request->hidden_order_notes:null;
        $order->save();
        $prescription = Prescription::find($order->prescription_id);
        $prescription->order_id = $order->id;
        $prescription->save();
        Session::forget('cart');
        return redirect()->back()->with('success', 'Order Placed successfully');
    }

}
