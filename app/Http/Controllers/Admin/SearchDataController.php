<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MedicinePharma\Repositories\Brand\BrandInterface;
use App\MedicinePharma\Repositories\SearchData\SearchDataInterface;
use App\Traits\BrandTrait;
use Brian2694\Toastr\Toastr;
use Exception;

class SearchDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $searchdata;
    public function __construct(SearchDataInterface $searchdata){
        $this->searchdata = $searchdata;
    }
    public function index()
    {
        $searchdatas = $this->searchdata->getAll();
        return view('admin.searchdata/index',compact('searchdatas'));
    }
}
