<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Product\StoreProductRequest;
use App\Http\Requests\Admin\Product\UpdateProductRequest;
use App\MedicinePharma\Repositories\Brand\BrandInterface;
use App\MedicinePharma\Repositories\Category\CategoryInterface;
use App\MedicinePharma\Repositories\Product\ProductInterface;
use App\MedicinePharma\Repositories\SubCategory\SubCategoryInterface;
use App\Models\Category;
use App\Traits\SubCategoryTrait;
use Brian2694\Toastr\Toastr;
use Exception;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use SubCategoryTrait;
    private $subcategory;
    private $category;
    private $product;
    private $brand;
    public function __construct(SubCategoryInterface $subcategory, CategoryInterface $category,ProductInterface $product,BrandInterface $brand){
        $this->subcategory = $subcategory;
        $this->category = $category;
        $this->product = $product;
        $this->brand = $brand;
    }
    public function index()
    {
        $products = $this->product->getAll();
        return view('admin.product/index',compact('products'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        try {
            $saveproduct = $this->product->store($request);
            return redirect()->route('product.index');
        }catch (Exception $ex) {
            dd($ex->getMessage());
            abort(400, "Somehting went wrong");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->product->find($id);
        return view('admin.product.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = $this->product->find($id);
        $categories = $this->category->getList();
        $subcategories = $this->subcategory->getList();
        return view('admin.product/edit',compact('product','categories','subcategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest  $request, $id)
    {
        $updateproduct = $this->product->update($request,$id);
        return redirect()->route('product.index');
    }

    public function getProduct($id)
    {
        $getproduct = $this->product->find($id);
        return $getproduct;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $deleteproduct = $this->product->delete($id);
        }catch(\Exception $e){
            return redirect()->route('product.index')->with('danger', $e->getMessage());
        }
        return redirect()->route('product.index');
    }

    public function checkfordeleted($keyword=null)
    {
        $checktrait = $this->checkfordeletedproduct($keyword);
        return  $checktrait;
    }
    public function restoredeleted($keyword=null)
    {
        $restoretrait = $this->restoredeletedproduct($keyword);
        return  $restoretrait;
    }
}
