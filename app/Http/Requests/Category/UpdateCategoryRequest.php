<?php

namespace App\Http\Requests\Admin\Category;

use App\Models\Category;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return policy(Category::class)->update($this->user());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd($this->request->get("name"));
        return [
                'name' => 'required|min:3|max:35|regex:/^([a-zA-Z\' ])*$/|unique:categories,name,'.$this->request->get("hidden_id"),
                'description' => 'required|min:3',
                'image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
            ];
    }

    public function forbiddenResponse()
    {
        return response()->view('errors.403');
    }
}
