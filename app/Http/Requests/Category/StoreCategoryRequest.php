<?php

namespace App\Http\Requests\Admin\Category;

use App\Models\Category;
use App\Traits\CategoryTrait;
use Illuminate\Foundation\Http\FormRequest;
use phpDocumentor\Reflection\Types\Null_;

class StoreCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return policy(Category::class)->create($this->user());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
                'name' => 'required|min:3|unique:categories,name',
                'description' => 'required|min:3',
                'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ];

    }

    public function forbiddenResponse()
    {
        return response()->view('errors.403');
    }
}
