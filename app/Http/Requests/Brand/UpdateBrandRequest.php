<?php

namespace App\Http\Requests\Admin\Brand;

use App\Models\Brand;
use Illuminate\Foundation\Http\FormRequest;

class UpdateBrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return policy(Brand::class)->update($this->user());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd($this->request->get("name"));
        return [
                'name' => 'required|min:3|max:35|regex:/^([a-zA-Z\' ])*$/|unique:brands,name,'.$this->request->get("hidden_id"),
                'description' => 'required|min:3',
                
            ];
    }

    public function forbiddenResponse()
    {
        return response()->view('errors.403');
    }
}
