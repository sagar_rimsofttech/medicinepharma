<?php

namespace App\Http\Requests\Admin\Brand;

use App\Models\Brand;
use App\Traits\BrandTrait;
use Illuminate\Foundation\Http\FormRequest;
use phpDocumentor\Reflection\Types\Null_;

class StoreBrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return policy(Brand::class)->create($this->user());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
                'name' => 'required|min:3|unique:brands,name',
                'description' => 'required|min:3',
                
            ];

    }

    public function forbiddenResponse()
    {
        return response()->view('errors.403');
    }
}
