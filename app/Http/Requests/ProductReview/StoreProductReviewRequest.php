<?php

namespace App\Http\Requests\ProductReview;

use App\Models\ProductReview;
use Illuminate\Foundation\Http\FormRequest;
use phpDocumentor\Reflection\Types\Null_;

class StoreProductReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
        // return policy(ProductReview::class)->create($this->user());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
                'name' => 'required|min:3',
                'product_id' => 'required|exists:products,id',
                'rating' => 'required|integer',
                'image.*' => 'required|mimes:jpeg,png,gif |max:4096',
                'image' => 'max:5',
            ];
    }
    public function messages()
    {
        return [
            'image.max' => "Image can't be more than 5.",
            
        ];

    }

    public function forbiddenResponse()
    {
        return response()->view('errors.403');
    }
}
