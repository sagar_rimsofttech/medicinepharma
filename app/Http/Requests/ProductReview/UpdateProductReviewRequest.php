<?php

namespace App\Http\Requests\Admin\ProductReview;

use App\Models\ProductReview;
use Illuminate\Foundation\Http\FormRequest;

class UpdateProductReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return policy(ProductReview::class)->update($this->user());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|unique_with:products,name,category_id,brand_id,'.(int)$this->request->get('hidden_id'),
                'category_id' => 'required|exists:categories,id',
                // 'subcategory_id' => 'required|exists:sub_categories,id',
                'description' => 'required|min:3',
                'brand_id' => 'required|exists:brands,id',
                // 'subcategory_id' => 'required|exists:sub_categories,id',
                'price' => 'required|integer',
                'discountedprice' => 'required|integer|min:0|lt:price',
                'quantity' => 'required|integer',
                'image.*' => 'mimes:jpeg,png,gif |max:4096',
                'image' => 'max:5',
            ];
    }

    public function messages()
    {
        return [
            'image.max' => "Image can't be more than 5.",
            'nameunique_with' => 'Combination of name and category allredy exists',
            'discountedprice.lt' => "Discounted Price Should be Less than Price.",
        ];

    }

    public function forbiddenResponse()
    {
        return response()->view('errors.403');
    }
}
