<?php

namespace App\Http\Requests\Prescription;

use App\Models\Prescription;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use phpDocumentor\Reflection\Types\Null_;

class StorePrescriptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        // return policy(Category::class)->create($this->user());
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ["nullable", Rule::requiredIf(!empty($this->request->get('create_account'))), "min:3", "max:255", "regex:/^([a-zA-Z\' ])*$/"],
            'number' => ['nullable', Rule::requiredIf(!empty($this->request->get('create_account'))), "regex:/^[1-9]{1,1}?[0-9]{9,9}$/"],
            'pincode' => ['nullable', Rule::requiredIf(!empty($this->request->get('create_account'))), "regex:/^[0-9]{6}$/"],
            'locality' => ['nullable', Rule::requiredIf(!empty($this->request->get('create_account'))), "regex:/^[a-zA-Z0-9_\-]*$/"],
            'email' => ['nullable', Rule::requiredIf(!empty($this->request->get('create_account'))), "max:255", "email"],
            'address' => ['nullable', Rule::requiredIf(!empty($this->request->get('create_account'))), "regex:/^[a-zA-Z0-9_\-]*$/"],
            'city' => ['nullable', Rule::requiredIf(!empty($this->request->get('create_account'))), "regex:/^([a-zA-Z\' ])*$/"],
            'state' => ['nullable', Rule::requiredIf(!empty($this->request->get('create_account'))), "in:58"],
            'landmark' => ['nullable', Rule::requiredIf(!empty($this->request->get('create_account'))), "max:255", "regex:/^[a-zA-Z0-9_\-]*$/"],
            'alternatenumber' => 'nullable|regex:/^[1-9]{1,1}?[0-9]{9,9}$/',
            'create_account' => ['nullable', Rule::requiredIf(!empty($this->request->get('create_account'))), "in:1"],
            'password' => ['nullable', Rule::requiredIf(!empty($this->request->get('create_account'))), "min:6"],
            'confirmpassword' => ['nullable', Rule::requiredIf(!empty($this->request->get('create_account'))), "required_with:password", "same:password", "min:6"],
            'delivery_note' => 'nullable|max:255',
            'prescription' => 'required|mimes:' . implode(',', config('constants.valid_extensions_file')) . '|max:' . config('constants.max_upload_size_file'),

        ];
    }

    public function forbiddenResponse()
    {
        return response()->view('errors.403');
    }
}
