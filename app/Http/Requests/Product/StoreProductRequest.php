<?php

namespace App\Http\Requests\Admin\Product;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use phpDocumentor\Reflection\Types\Null_;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return policy(Product::class)->create($this->user());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'name' => 'required|min:3|unique:products,name,category_id',
                'category_id' => 'required|exists:categories,id',
                'brand_id' => 'required|exists:brands,id',
                // 'subcategory_id' => 'required|exists:sub_categories,id',
                'price' => 'required|integer',
                'discountedprice' => 'required|integer|min:0|lt:price',
                'quantity' => 'required|integer',
                'is_active' => 'nullable|integer',
                'is_host_product' => 'nullable|integer',
                'is_special_product' => 'nullable|integer',
                'is_dailyneed_product' => 'nullable|integer',
                'description' => 'required|min:3',
                'image.*' => 'required|mimes:jpeg,png,gif |max:4096',
                'image' => 'max:5',
            ];
    }
    public function messages()
    {
        return [
            'image.max' => "Image can't be more than 5.",
            'discountedprice.lt' => "Discounted Price Should be Less than Price.",
        ];

    }

    public function forbiddenResponse()
    {
        return response()->view('errors.403');
    }
}
