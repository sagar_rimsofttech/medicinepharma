<?php

namespace App\Http\Requests\Admin\SubCategory;

use App\Models\SubCategory;
use App\Traits\SubCategoryTrait;
use Illuminate\Foundation\Http\FormRequest;
use phpDocumentor\Reflection\Types\Null_;

class StoreSubCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return policy(SubCategory::class)->create($this->user());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'name' => 'required|min:3|unique:sub_categories,name',
                'categories' => 'required|exists:categories,id',
                'description' => 'required|min:3',
                'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ];

    }

    public function forbiddenResponse()
    {
        return response()->view('errors.403');
    }
}
