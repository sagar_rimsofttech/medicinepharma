<?php

namespace App\Helpers;

class CustomCurl {

    private $httpCode = null;
    private $headers = null;
    public $request_local_ip = null;
    public function post($url, $data, $headers){
        return $this->sendRequest($url, 'post', $data, $headers);
    }

    public function get($url, $data, $headers){
        return $this->sendRequest($url, 'get', $data, $headers);
    }

    private function sendRequest($url, $type, $data, $headers){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if($type == 'post'){
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        else{
            //curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //A given cURL operation should only take
        //120 seconds max.
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        $headers[] = "is_emr_request:true";

        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $output = curl_exec ($ch);

        // dd($headers);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $headers = substr($output, 0, $header_size);
        $headersArray = $this->getHeadersFromCurlResponse($output);
        $body = substr($output, $header_size);

        $this->setStatusCode($headersArray['http_code']);
        $this->setHeaders($headersArray);

        curl_close ($ch);
        return $body;
    }

    public function getAuthorizationCode(){
        return $this->headers['Authorization'];
    }

    private function setStatusCode($statusCode){
        if($statusCode == 'HTTP/1.1 200 OK')
            $this->httpCode = 200;
    }

    public function getStatusCode(){
        return $this->httpCode;
    }

    private function setHeaders($headers){
        $this->headers = $headers;
    }

    public function getHeaders(){
        return $this->headers;
    }

    private function getHeadersFromCurlResponse($response)
    {
        try{

            $headers = array();

            $headerText = explode("\r\n\r\n", $response);
            $headerTemp = array_map(function($arr){return str_replace(' ', '_', $arr);}, $headerText);
            $searchword = 'HTTP/1.1_200_OK';

            $headerKey = array_keys(array_filter($headerTemp, function($var) use ($searchword){
                return strpos($var, $searchword) !== false;
            }))[0];

            foreach (explode("\r\n", $headerText[$headerKey]) as $i => $line)
            if ($i === 0)
            $headers['http_code'] = $line;
            else
            {
                list ($key, $value) = explode(': ', $line);

                $headers[$key] = $value;
            }

            return $headers;
        }
        catch(\Exception $e){
            return ['http_code' => 405];
        }
    }
}
