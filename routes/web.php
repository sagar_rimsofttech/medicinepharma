<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response as FacadesResponse;

Route::view('/', 'welcome');

// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(["prefix" => "admin", "namespace" => "Admin", 'middleware' => 'auth:admin'], function () {

    /********* User Management */
    Route::get('/users', 'UserController@index')->name('user.index');
    Route::get('/users/create', 'UserController@create')->name('user.create');
    Route::get('/users/edit/{reference_id}', 'UserController@edit')->name('user.edit');
    Route::post('/users/destroy/{reference_id}', 'UserController@destroy')->name('user.destroy');
    Route::post('/users/store', 'UserController@store')->name('user.store');
    Route::POST('/users/update/{reference_id}', 'UserController@update')->name('user.update');
    /*********role and permission */
    Route::resource('/roles', 'RoleController');
    Route::post('/roles/destroy/{reference_id}', 'RoleController@destroy')->name('roles.destroy');
    Route::resource('/permissions', 'PermissionController');
    Route::post('/permissions/destroy/{reference_id}', 'PermissionController@destroy')->name('permissions.destroy');
    /*******************user profile */
    Route::post('/user-profile-password-update', 'UserProfileController@updatepassword')->name('userpassword.update');
    Route::resource('/user-profile', 'UserProfileController');

    /************************Catgory*********/
    Route::resource('/category', 'CategoryController');
    Route::POST('/category/update/{reference_id}', 'CategoryController@update')->name('category.update');
    Route::post('/category/destroy/{reference_id}', 'CategoryController@destroy')->name('category.destroy');

    Route::get('/checkfordeleted/{keyword?}', 'CategoryController@checkfordeleted')->name('category.checkfordeleted');
    Route::get('/restoredeleted/{keyword?}', 'CategoryController@restoredeleted')->name('category.restoredeleted');
    Route::get('/search-category/{keyword?}', 'CategoryController@searchCategory')->name('search.category');


    /************************SubCatgory*********/
    Route::resource('/subcategory', 'SubCategoryController');
    Route::POST('/subcategory/update/{reference_id}', 'SubCategoryController@update')->name('subcategory.update');
    Route::post('/subcategory/destroy/{reference_id}', 'SubCategoryController@destroy')->name('subcategory.destroy');

    Route::get('/checkfordeletedsubcategory/{keyword?}', 'SubCategoryController@checkfordeleted')->name('subcategory.checkfordeletedsubcategory');
    Route::get('/restoredeletedsubcategory/{keyword?}', 'SubCategoryController@restoredeleted')->name('subcategory.restoredeletedsubcategory');
    Route::get('/search-subcategory/{keyword?}', 'SubCategoryController@searchSubCategory')->name('search.subcategory');
    /************************Product*********/
    Route::resource('/product', 'ProductController');
    Route::POST('/product/update/{reference_id}', 'ProductController@update')->name('product.update');
    Route::post('/product/destroy/{reference_id}', 'ProductController@destroy')->name('product.destroy');
    Route::get('getproduct/{id}', 'ProductController@getProduct')->name('get.product');


    Route::get('/checkfordeletedproduct/{keyword?}', 'ProductController@checkfordeleted')->name('product.checkfordeletedproduct');
    Route::get('/restoredeletedproduct/{keyword?}', 'ProductController@restoredeleted')->name('product.restoredeletedproduct');

    /************************Brand*********/
    Route::resource('/brand', 'BrandController');
    Route::POST('/brand/update/{reference_id}', 'BrandController@update')->name('brand.update');
    Route::post('/brand/destroy/{reference_id}', 'BrandController@destroy')->name('brand.destroy');

    Route::get('/checkfordeletedbrand/{keyword?}', 'BrandController@checkfordeleted')->name('brand.checkfordeletedbrand');
    Route::get('/restoredeletedbrand/{keyword?}', 'BrandController@restoredeleted')->name('brand.restoredeletedbrand');
    Route::get('/search-brand/{keyword?}', 'BrandController@searchBrand')->name('search.brand');

    /************************ProductReview*********/
    Route::resource('/productreview', 'ProductReviewController');
    Route::POST('/productreview/update/{reference_id}', 'ProductReviewController@update')->name('productreview.update');
    Route::post('/productreview/destroy/{reference_id}', 'ProductReviewController@destroy')->name('productreview.destroy');

    Route::get('/checkfordeletedproductreview/{keyword?}', 'ProductReviewController@checkfordeletedproductreview')->name('productreview.checkfordeletedproductreview');
    Route::get('/restoredeletedproductreview/{keyword?}', 'ProductReviewController@restoredeletedproductreview')->name('productreview.restoredeletedproductreview');
    Route::get('/search-product/{keyword?}', 'ProductReviewController@searchProduct')->name('search.product');
    /************************Order*********/
    Route::resource('/order', 'OrderController');
    Route::get('get/order', 'OrderController@getOrder');
    /************************Prescription*********/
    Route::resource('/prescription', 'PrescriptionController');
    Route::get('search-product/{keyword?}', 'PrescriptionController@searchProduct')->name('prescription.product');
    Route::post('/prescription-order', 'PrescriptionController@addprescriptionorder')->name('prescription.order');
    //  Route::POST('/product/update/{reference_id}', 'ProductController@update')->name('product.update');
    //  Route::post('/product/destroy/{reference_id}','ProductController@destroy')->name('product.destroy');

    //  Route::get('/checkfordeletedproduct/{keyword?}', 'ProductController@checkfordeleted')->name('product.checkfordeletedproduct');
    //  Route::get('/restoredeletedproduct/{keyword?}', 'ProductController@restoredeleted')->name('product.restoredeletedproduct');
    /************************SearchData*********/
    Route::resource('/searchdata', 'SearchDataController');
    //   Route::get('search-product/{keyword?}', 'PrescriptionController@searchProduct')->name('prescription.product');
    //   Route::post('/prescription-order','PrescriptionController@addprescriptionorder')->name('prescription.order');

});
Route::get('/users/logout', 'Auth\LoginController@userLogout')->name('user.logout');
Route::get('/verify', 'VerifyController@getVerigy')->name('getVerify');
Route::post('/verify', 'VerifyController@postVerify')->name('verify');
// Route::get('/admin','AdminController@index');
Route::prefix('admin')->group(function () {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
});
// Route::get('/getemail', function() {
//     if (1==1) {
//         $notifiable =  Auth::user();
//         $order = Auth::user()->orders()->latest()->first();
//         return FacadesResponse::view('admin.emails/orders/orderConfirmation',compact('notifiable','order'));
//     }

// });
Auth::routes(['verify' => true]);
Route::get('/', 'Frontend\IndexController@index')->name('showIndex');
Route::get('/category/{reference_id}/product', 'Frontend\IndexController@categoryproduct')->name('category.product');
Route::get('/product/details/{slug}', 'Frontend\ProductController@productdetails')->name('product.details');
Route::get('/add-to-cart/{quantity}/{product_id}', 'Frontend\ProductController@getAddToCart')->name('product.addToCart');
Route::get('/reduce-by-one/{product_id}', 'Frontend\ProductController@getReduceByOne')->name('product.removeFromCart');
Route::get('/remove-product-from-cart/{product_id}', 'Frontend\ProductController@removeProductFromCart')->name('product.removeProductFromCart');
Route::get('/shopping-cart', 'Frontend\ProductController@getCart')->name('product.shoppingCart');
Route::get('/shopping-checkout', 'Frontend\OrderController@checkout')->name('shopping.checkout');
Route::get('/order-summary/{order_id}', 'Frontend\OrderController@getRecentOrder')->name('order.success');
Route::get('/order-failed/{order_id}', 'Frontend\OrderController@getRecentFailedOrder')->name('order.failed');
Route::get('/order-open/{order_id}', 'Frontend\OrderController@getRecentOpenOrder')->name('order.open');
Route::post('/store/order', 'Frontend\OrderController@store')->name('order.store');
Route::get('/user-profile', 'Frontend\UserProfileController@index')->name('user.profile');
Route::post('/store/address', 'Frontend\UserProfileController@storeaddress')->name('saveuser.address');
Route::post('/user/getExistingUser', 'Frontend\UserProfileController@getExistingUser')->name('check.user');
Route::post('/store/prescription', 'Frontend\OrderController@storePrescription')->name('store.prescription');
Route::post('/productsearch/fetch', 'Frontend\ProductController@searchfetch')->name('search.fetch');
Route::post('/update-password', 'Frontend\UserProfileController@updatepassword')->name('update.password');

Route::get('/payment', 'PaytmController@pay');
Route::post('/payment/status', 'PaytmController@paymentCallback');
Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider')->name('login.google');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/verify-phone', 'VerifyController@getVerify')->name('getverify');
Route::post('/verify-phone', 'VerifyController@postVerify')->name('verify');
Route::get('/update-phone', 'Frontend\UserProfileController@updatePhone')->name('update.phone');
Route::post('/update-phone', 'Frontend\UserProfileController@saveMobilenumber')->name('save.phone');

Route::post('loginWithOtp', 'Frontend\UserProfileController@loginWithOtp')->name('loginWithOtp');
Route::get('/loginWithOtp', 'Frontend\UserProfileController@loginWithOtpView');


Route::post('checkmobilenumber', 'Frontend\UserProfileController@checkmobilenumber');
Route::post('sendOtp', 'Frontend\UserProfileController@sendOtp');
