<?php

// use Illuminate\Support\Facades\URL;

return [

    'upload_path' => env('UPLOAD_PATH') . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR,
    'upload_url' => env('UPLOAD_URL') . '/uploads/',
    'asset_url' => env('UPLOAD_URL') . '/assets/',
    'paytm_callback' => 'https://med2myhome.com/payment/status',
    // 'image_url' => URL::asset('storage/uploads/'),
    'ORDER' => [
        'PAYMENT_STATUS' => [
            0 => 'Pending',
            1 => 'Success',
            3 => 'Canceled',
            2 => 'Open',
            4 => 'Failed',
        ],
        'PAYMENT_TYPE' => [
            1 => 'Paytm',
            2 => 'Cash',
        ],
        'ORDER_STATUS' => [
            1 => 'Processed',
            2 => 'Shipped',
            3 => 'En Route',
            4 => 'Delivered'
        ],
        'ORDER_FROM' => [
            1 => 'Portal',
            2 => 'Prescription'
        ],
    ],
    'Unit' => [],
    'valid_extensions_file' => ['jpg', 'jpeg', 'png', 'pdf', 'docx', 'doc'],
    'valid_extensions_image' => ['jpg', 'jpeg', 'png'],
    'max_upload_size_file' => 5000,
    'max_upload_size_image' => 2000,
];
